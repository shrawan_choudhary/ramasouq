<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('login', 'API\Userapi@login');
Route::post('register', 'API\Userapi@register');
Route::post('getcitylist', 'API\Cityapi@getcitylist');
Route::post('getcatlist', 'API\Categoryapi@getcatlist');
Route::post('getslider', 'API\Sliderapi@getslider');
Route::post('getcoupons', 'API\Couponapi@getcoupons');
Route::post('getrecentcoupons', 'API\Couponapi@getrecentcoupons');
Route::post('getfeaturedcoupons', 'API\Couponapi@getfeaturedcoupons');
Route::post('full_register', 'API\Userapi@full_register');

Route::group(['middleware' => 'auth:api'], function(){
//Route::post('details', 'API\Userapi@details');

Route::post('otp_verify', 'API\Userapi@otp_verify');
Route::post('resend_otp', 'API\Userapi@resend_otp');
Route::post('add_to_cart', 'API\Cartapi@add_to_cart');
Route::post('get_cart_list', 'API\Cartapi@get_cart_list');
Route::post('add_order', 'API\Orderapi@add_order');
});