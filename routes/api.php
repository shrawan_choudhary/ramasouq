<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('login', 'API\Userapi@login');
Route::post('register', 'API\Userapi@register');
Route::post('contact_us', 'API\Userapi@contact_us');
Route::post('forgot_password', 'API\Userapi@forgot_password');
Route::post('reset_password', 'API\Userapi@reset_password');
Route::post('social_login', 'API\Userapi@social_login');
Route::post('social_register', 'API\Userapi@social_register');
Route::post('user_reset_password_otp_veriy', 'API\Userapi@user_reset_password_otp_veriy');
Route::post('getcitylist', 'API\Cityapi@getcitylist');
Route::post('getcatlist', 'API\Categoryapi@getcatlist');
Route::post('getslider', 'API\Sliderapi@getslider');
Route::post('getcoupons', 'API\Couponapi@getcoupons');
Route::post('coupon_details', 'API\Couponapi@coupon_info');
Route::post('getrecentcoupons', 'API\Couponapi@getrecentcoupons');
Route::post('search_coupon', 'API\Couponapi@search_coupon');
Route::post('getfeaturedcoupons', 'API\Couponapi@getfeaturedcoupons');
//Route::post('full_register', 'API\Userapi@full_register');
Route::post('getpage', 'API\Pagesapi@getpage');
Route::post('add_to_inquiry', 'API\Inquiryapi@add_to_inquiry');
Route::post('store_signup', 'API\Storeapi@store_signup');
Route::post('store_login', 'API\Storeapi@store_login');
Route::post('add_store_coupon', 'API\Storeapi@add_store_coupon');
Route::post('get_store_coupon_info', 'API\Storeapi@get_store_coupon_info');
Route::post('get_store_coupons', 'API\Storeapi@get_store_coupons');
Route::post('add_coupon_image', 'API\Storeapi@add_coupon_image');
Route::post('store_redeemed_coupons', 'API\Storeapi@store_redeemed_coupons');
Route::post('register_shop', 'API\Storeapi@register_shop');
Route::post('edit_shop', 'API\Storeapi@edit_shop');
Route::post('store_sales_report', 'API\Storeapi@store_sales_report');
Route::post('store_otp_verify', 'API\Storeapi@store_otp_verify');
Route::post('verify_otp_to_coupon_redeem', 'API\Storeapi@verify_otp_to_coupon_redeem');
Route::post('shop_coupon_redeem', 'API\Storeapi@shop_coupon_redeem');
Route::post('send_otp_to_coupon_redeem', 'API\Storeapi@send_otp_to_coupon_redeem');
Route::post('store_change_password', 'API\Storeapi@store_change_password');
Route::post('store_resend_otp', 'API\Storeapi@store_resend_otp');
Route::post('store_contact_us', 'API\Storeapi@store_contact_us');
Route::post('store_coupon_redeem', 'API\Storeapi@store_coupon_redeem');
Route::post('store_otp_verify_to_coupon_redeem', 'API\Storeapi@store_otp_verify_to_coupon_redeem');
Route::post('get_notifications', 'API\Notificationapi@get_notifications');
Route::post('getNotificationCount', 'API\Notificationapi@getNotificationCount');
Route::post('send_msg', 'API\Chatapi@send_msg');
Route::post('chat_history', 'API\Chatapi@chat_history');
Route::post('getcartCount', 'API\Cartapi@getcartCount');
Route::post('store_forgot_password', 'API\Storeapi@store_forgot_password');
Route::post('store_reset_password', 'API\Storeapi@store_reset_password');
Route::post('store_reset_password_otp_veriy', 'API\Storeapi@store_reset_password_otp_veriy');



Route::group(['middleware' => 'auth:api'], function(){
    // Route::post('details', 'API\Userapi@details');

    Route::post('otp_verify', 'API\Userapi@otp_verify');
    Route::post('resend_otp', 'API\Userapi@resend_otp');
    Route::post('add_to_cart', 'API\Cartapi@add_to_cart');
    Route::post('get_cart_list', 'API\Cartapi@get_cart_list');
    Route::post('add_order', 'API\Orderapi@add_order');
    Route::post('my_coupons', 'API\Orderapi@my_coupons');
    Route::post('my_purchased_coupons', 'API\Orderapi@my_purchased_coupons');
    Route::post('my_used_coupons', 'API\Orderapi@my_used_coupons');
    Route::post('my_expired_coupons', 'API\Orderapi@my_expired_coupons');
    Route::post('redeem_coupon', 'API\Orderapi@redeem_coupon');
    Route::post('edit_profile', 'API\Userapi@edit_profile');
    Route::post('change_password', 'API\Userapi@change_password');
    Route::post('remove_cart_item', 'API\Cartapi@remove_cart_item');
});