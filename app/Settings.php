<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
   
    protected $guarded = [];
    public function getLogoAttribute()
		{
			if (! $this->attributes['logo']) {
				return 'logo.png';
			}
			return $this->attributes['logo'];
		}
		public function getFaviconAttribute()
		{
			if (! $this->attributes['favicon']) {
				return 'favicon_def.ico';
			}
			return $this->attributes['favicon'];
		}
		public function getBtnTitleAttribute()
		{
			if (! $this->attributes['btn_title']) {
				return 'Hot Deals';
			}
			return $this->attributes['btn_title'];
		}
		public function getBtnTitle2Attribute()
		{
			if (! $this->attributes['btn_title2']) {
				return 'Trending Items';
			}
			return $this->attributes['btn_title2'];
		}
		public function getFTitle1Attribute()
		{
			if (! $this->attributes['f_title1']) {
				return 'Widget 1';
			}
			return $this->attributes['f_title1'];
		}
		public function getFTitle2Attribute()
		{
			if (! $this->attributes['f_title2']) {
				return 'Widget 2';
			}
			return $this->attributes['f_title2'];
		}
		public function getFTitle3Attribute()
		{
			if (! $this->attributes['f_title3']) {
				return 'Widget 3';
			}
			return $this->attributes['f_title3'];
		}
		public function getFTitle4Attribute()
		{
			if (! $this->attributes['f_title4']) {
				return 'Newsletter Subscribe';
			}
			return $this->attributes['f_title4'];
		}
}
