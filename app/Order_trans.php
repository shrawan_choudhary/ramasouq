<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_trans extends Model
{
	protected $table = 'order_trans';
	
	protected $fillable = [
  	'user_id','store_id','qty','coupon_id','price','code','discount','order_id','is_redeem','discount_type','discount_upto'
  ];
}
