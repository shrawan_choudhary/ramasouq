<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\Viewable;


class Couponimage extends Model
{
  use Viewable;
  protected $table   = "coupon_image";
  protected $fillable = [
  	'coupon_id','image'];

  // protected $dates = [
  //       'expiry' 
  //   ];
  
  protected $appends = ['image_url']; 

    public function getImageUrlAttribute()
    {
        return $this->image ? url('images/coupon/' . $this->image) : "";
    }
  
}
