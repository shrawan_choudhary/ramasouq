<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
  //   use Viewable;
  protected $table = 'cart';
  protected $fillable = [
  	'user_id','store_id','qty','coupon_id'
  ];
}