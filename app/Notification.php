<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
		protected $table = 'order_trans';
		protected $fillable = [
  	'user_id','store_id','message'
	];
	
}
