<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
    	'title','image','is_active','icon','slug','color_code'
    ];

    public function coupon()
    {
      return $this->hasMany('App\Coupon', 'category_id', 'id');
    }
    public function store()
    {
      return $this->belongsToMany('App\Store');
    }
    public function stores()
    {
      return $this->hasMany('App\Store', 'category_id',  'id');
    }
    
}
