<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    //
	protected $table = 'inquiry';
	
	 protected $fillable = [
  	'name','email','mobile_no','shop_name'
  ];
	
}
