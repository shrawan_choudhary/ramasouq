<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [
    	'mobile','title','image','is_active','is_featured','is_otp_verified','slug','link','address','category_id','opening_time','closing_time','auth_token','password','email','merchant_name','device_type','device_id','fcm_id','otp','coupon_redeem_token','is_terms_and_condition_accepted','city_id'
    ];

    public function coupon()
    {
        return $this->hasMany('App\Coupon');
    }
    public function category()
    {
        return $this->belongsToMany('App\Category');
    }
    public function categories()
    {
        return $this->hasMany('App\Category', 'id', 'category_id');
    }

}
