<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;

class NotificationController extends Controller
{
   
    public function index(request $request){

        $users = User::get();
        

        $data = compact('users'); // Variable to array convert
        return view('admin.send-notification', $data);
    }

    public function sendmail(Request $request)
    {
        
        $email = $request->email;
        // dd($email);
        // print_r($email);
        // $email = implode(',', $email);
        // $input['email']     = $email;
        // $emails = $input['email'];
        // $emails = explode(",", $email[0]);
        // dd($emails);
        $subject = $request->subject;
        $msg     = $request->message;
        // dd()
        foreach ($email as $e) {

        $data = array('name' => "Suncity Techno", 'subject' => $subject, 'msg' => $msg, 'email' => $e);

          Mail::send('email.send-mail', $data, function($message) use ($data) {
             $message->to($data['email'], $data['name'])->subject($data['subject']);
             $message->from('ramasouq@ramasouq.com', 'Ramasouq');
          }); 
        }          
        return redirect()->back()->with('success', 'Message Send Successful.');
    }
    public function mail(request $request){

        $users = User::get();
        

        $data = compact('users'); // Variable to array convert
        return view('admin.mail', $data);
    }
    public function mailsend(Request $request)
    {
        $email = $request->email;
        // dd($email);
        // print_r($email);
        // $email = implode(',', $email);
        // $input['email']     = $email;
        // $emails = $input['email'];
        // $emails = explode(",", $email[0]);
        // dd($emails);
        $subject = $request->subject;
        $msg     = $request->message;
        // dd()
        foreach ($email as $e) {

        $data = array('name' => "Suncity Techno", 'subject' => $subject, 'msg' => $msg, 'email' => $e);

          Mail::send('email.send-mail', $data, function($message) use ($data) {
             $message->to($data['email'], $data['name'])->subject($data['subject']);
             $message->from('ramasouq@ramasouq.com', 'Ramasouq');
          }); 
        }          
        return redirect()->back()->with('success', 'Message Send Successful.');
    }
}