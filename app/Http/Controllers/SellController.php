<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pages;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
	public function index()
    {
       
        $statename = DB::table('states')->get();
        $cityname = DB::table('citiese')->get();

        $title = ucwords(Lang::get('constants.user') . ' ' . Lang::get('constants.register')); 
        
        return view('admin.faq.index')->with('name', $name)->with('page', 'sell')->with('statename', $statename)->with('cityname', $cityname);
    }

}