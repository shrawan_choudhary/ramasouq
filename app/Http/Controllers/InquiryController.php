<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Inquery;
use App\State;
use App\City;

class InquiryController extends Controller {
    
    public function index(Request $request) {

        $inquiry    = Inquery::with(['i_city', 'i_city.cat'])->get();
        $data       = compact('inquiry');
        // dd($inquiry);
        return view('admin.inquiry',$data);

    }
    public function destroy($id)
    {
        $remove = Inquery::where('id',$id)->delete();

        return back()->with('deleted','Inquiry has been deleted');;
    }

    // public function removeMultiple(Request $request)
    // {
    //     $check = $request->check; // input type="checkbox" name="check[]"
    //     City::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

    //     return redirect()->back()->with('success', 'Item(s) removed.');
    // }

     public function bulk_delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'checked' => 'required',
        ]);

        if ($validator->fails()) {

            return back()->with('deleted', 'Please select one of them to delete');
        }

        foreach ($request->checked as $checked) {

            $this->destroy($checked);
            
        }

        return back()->with('deleted', 'Inquiry has been deleted');   
    }
}