<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\City;
use App\State;

class CityController extends Controller
{
   
    // public function index(request $request){

    //     $query = City::latest();

    //     if( !empty( $request->title ) ) {
    //         $query->where('title', 'LIKE', '%'.$request->title.'%');
    //     }


    //     $lists1 = $query->paginate(20);        

    //     $data = compact( 'lists1' ); // Variable to array convert
    //     return view('admin.city.create', $data);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $city = City::with('cat')->get();
        
        return view('admin.city.index', compact('city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    

   
    // public function add()
    // {
    //     //

    //     $states = State::get();
    //     $parentArr = [
    //         ''  => 'Select State'
    //     ];

    //     foreach($states as $c) {

    //         $parentArr[ $c->id ] = $c->state;
    //     }
    //     $data = compact('parentArr');
    //     return view('backend.inc.city.add',$data);
    // }
    public function create()
    {

    	$states = State::get();
        $parentArr = [
            ''  => 'Select State'
        ];

        foreach($states as $c) {

            $parentArr[ $c->id ] = $c->name;
        }
        return view('admin.city.create', compact('parentArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    
    // public function addData(Request $request)
    // {
    //     //
    //     $rules = [
    //         'title'             => 'required',            
    //             ];
    //         // $file = $request->file('image','image_hover');
        
           
    //     $request->validate( $rules );
    //     $obj = new City;
    //     $obj->title             = $request->title;
    //     $obj->slug              = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
    //     $obj->parent            = $request->parent;
        
        
    //     // $obj->image  = $request->$file->getClientOriginalName();
        
    //     $obj->save();

    //     return redirect( url('admin-control/city/') )->with('success', 'Success! New record has been added.');
    // }
    public function store(Request $request)
    {
        //
        $rules = [
            'name'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
        
           
        $request->validate( $rules );
        $obj = new City;
        $obj->name             = $request->name;
        $obj->slug              = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        $obj->parent            = $request->parent;
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return back()->with('added','Faq has been added');;
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function edit(Request $request,$id)
    {
        //
        $edit = City::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        $states = State::get();
        $parentArr = [
            ''  => 'Select State'
        ];

        foreach($states as $c) {
            $parentArr[ $c->id ] = $c->name;
        }
        


        $lists1 = City::latest()->paginate(20);
        //
        

        

        return view('admin.city.edit',compact('lists1','edit','parentArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    
    
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'name'          => 'required',
            
            
        ];
        $request->validate( $rules );
        

        $obj = City::findOrFail( $id );
        $obj->name           = $request->name;
        $obj->slug            = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug);
        $obj->parent          = $request->parent;
        
        $obj->save();
        return redirect('admin/city')->with('updated','City has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     

    public function destroy($id)
    {
        $remove = City::where('id',$id)->delete();

        return back()->with('deleted','Faq has been deleted');;
    }

    // public function removeMultiple(Request $request)
    // {
    //     $check = $request->check; // input type="checkbox" name="check[]"
    //     City::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

    //     return redirect()->back()->with('success', 'Item(s) removed.');
    // }

     public function bulk_delete(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"

        City::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');  
    }

   
}
