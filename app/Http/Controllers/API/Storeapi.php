<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Store; 
use App\User;
use App\Couponimage;
use App\Coupon; 
use App\Category; 
use App\Settings; 
use Image;
use Carbon\Carbon;
use App\Order_trans; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
 
use Validator;



class Storeapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	
    public function store_signup(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            
            'name' 				=> 'required' , 
            'mobile' 			=> 'required' ,
            'email' 			=> 'required' ,
            'password' 			=> 'required',
			'confirm_password' 	=> 'required|same:password',
            'device_type' 		=> 'required',
            'device_id' 		=> 'required',
            'fcm_id' 			=> 'required'
          
		  ]);
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		$setting = Settings::find(1);
		$input = $request->all();
	
	
		$input['merchant_name'] =$input['name']; 
		unset($input['name']);
		
		
		 $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

	$autToken = 	substr(str_shuffle(str_repeat($pool, 16)), 0, 16);
		$input['password'] = bcrypt($input['password']);
		$input['auth_token']=$autToken;
		$otp = rand(100000,999999);
		
// 		$msg    = urlencode("Your one time password in ".$setting->w_title." is ".$otp." ");

//         $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);

//         $sms    = file_get_contents($apiUrl);
        
		$input['otp']=$otp;
		$input['is_otp_verified']=0;
		
		$store =Store::create($input); 
		
		
        $success['storeid'] =  $store->id;
        $success['title'] =  '';
        $success['mobile'] =  $store->mobile;
        $success['merchant_name'] =  $store->merchant_name;
        $success['email'] =  $store->email;
        $success['image'] =  '';
        $success['address'] =  '';
        $success['opening_time'] =  '';
        $success['closing_time'] = '';
        $success['auth_token'] =  $store->auth_token;
        $success['is_active'] =  '';
        $success['otp'] =  $otp;
		
		$data = array();
		$data['status'] = 'success'; 
		$data['data'] = $success; 
		$data['is_shop_registred'] = false; 
		$data['is_otp_verified'] = 0; 
		
		$data['msg'] = 'add Successfully'; 
		return response()->json($data, $this->successStatus); 
		
	}  
	
	    public function store_otp_verify(Request $request) 
    { 
			$validator = Validator::make($request->all(), [ 
            'store_id' => 'required', 	
            'otp' 	 => 'required'   ,  
            'auth_token' 	 => 'required'     
        ]);
		
		if ($validator->fails()) { 
		
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($validator->errors(), 200); 
		
           // return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		$input = $request->all(); 
		
		
		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get()->count(); 
		if($storeCount==1)
		{
		
		$store = Store::findOrFail($input['store_id']);
	//	$user->update($input);	
//	echo $store->otp;
		if ($store->otp ==$input['otp'])
		{
			//$user = User::where($arr);
			$input['is_otp_verified']=1;
			$arrOtp = array('is_otp_verified'=>1);
		//	$arrOtp['is_otp_verified']=1;
		//	$store->update($arrOtp);

			DB::table('stores')
            ->where('id', $input['store_id'])
            ->update(['is_otp_verified' => '1']);

			
			$success = array();
			$success['is_otp_verified'] = 1;
			//$success['otp'] =  $user->otp;
			
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = 'OTP Verified Successfully'; 
			return response()->json($result, $this->successStatus);
			
			//return response()->json(['success'=>$success], $this-> successStatus); 			
		}else
		{
			//return response()->json(['error'=>'Invalid OTP'], 200); 
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid OTP'; 
				return response()->json($result, 200); 
		}
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 
		}
		
		
		}  
	
	
   public function register_shop(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'shop_id' 			=> 'required' , 
            'city_id' 			=> 'required' , 
            'shop_name' 		=> 'required' , 
            'address' 			=> 'required' ,
            'category_id' 		=> 'required' ,
            'opening_time' 		=> 'required' ,
            'closing_time' 		=> 'required' ,
			'image' => 'nullable|image|mimes:jpeg,png,jpg',
			'device_type' 		=> 'required',
            'device_id' 		=> 'required',
            'fcm_id' 			=> 'required',
            'is_terms_and_condition_accepted' 			=> 'required',
        ]);
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		$input = $request->all();
		$shop_name = $input['shop_name'];
		$input['title'] = $shop_name;
		unset($input['shop_name']);
	
	if ($file = $request->file('image')) {
			
			$optimizeImage = Image::make($file);
      $optimizePath = public_path().'/images/store/';
      $name = time().$file->getClientOriginalName();
      $optimizeImage->save($optimizePath.$name, 72);

			$input['image'] = $name;

		}
	
	
	
	$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['shop_id'])
							->get()->count(); 
		
		if($storeCount==1)
		{
	
	
	  $store = Store::findOrFail($input['shop_id']);
	
		if($store!=null)
		{
			$store->update($input);
			unset($store['password']);			
			unset($store['slug']);			
			unset($store['link']);	

			

			$Data  = DB::table('stores')
					->select('stores.id','stores.mobile','stores.title','stores.image','stores.address','stores.category_id','stores.opening_time','stores.closing_time','stores.auth_token','stores.merchant_name','stores.email','stores.device_type','stores.device_id','stores.fcm_id','categories.title as cat_name')	
					->leftjoin('categories','stores.category_id' ,'categories.id')
					->where('stores.id', '=', $input['shop_id'])
				->get();
					
			$success['storeid'] =  $store->id;
			$data = array();
			$data['status'] = 'success'; 
			$data['data'] = $Data[0]; 
			$data['msg'] = 'add Successfully'; 
			return response()->json($data, $this->successStatus); 
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Not Found'; 
				return response()->json($result, 200); 
			
		}
		
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 
		}
		
		
	}  
	
   public function edit_shop(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'shop_id' 			=> 'required' , 
            'opening_time' 		=> 'required' ,
            'closing_time' 		=> 'required' ,
			'image' => 'nullable|image|mimes:jpeg,png,jpg'
        ]);
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		$input = $request->all();
		
	
	if ($file = $request->file('image')) {
			
			$optimizeImage = Image::make($file);
      $optimizePath = public_path().'/images/store/';
      $name = time().$file->getClientOriginalName();
      $optimizeImage->save($optimizePath.$name, 72);

			$input['image'] = $name;

		}
	
	
	
	$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['shop_id'])
							->get()->count(); 
		
		if($storeCount==1)
		{
	
	
	  $store = Store::findOrFail($input['shop_id']);
	
		if($store!=null)
		{
			$store->update($input);
			unset($store['password']);			
			unset($store['slug']);			
			unset($store['link']);	

			

			$Data  = DB::table('stores')
					->select('stores.id','stores.mobile','stores.is_otp_verified','stores.title','stores.image','stores.address','stores.category_id','stores.opening_time','stores.closing_time','stores.auth_token','stores.merchant_name','stores.email','stores.device_type','stores.device_id','stores.fcm_id','categories.title as cat_name')	
					->leftjoin('categories','stores.category_id' ,'categories.id')
					->where('stores.id', '=', $input['shop_id'])
				->get();
					
			$success['storeid'] =  $store->id;
			$data = array();
			$data['status'] = 'success'; 
			$data['data'] = $Data[0]; 
			$data['msg'] = 'add Successfully'; 
			return response()->json($data, $this->successStatus); 
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Not Found'; 
				return response()->json($result, 200); 
			
		}
		
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 
		}
		
		
	}  
	
		public function store_login(Request $request){ 
	
			
		 $validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'password' 	=> 'required', 
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =''; 
					$result['msg'] = 'Invalid Parameters'; 
					return response()->json($result, 200);        
			}
		$input = $request->all();
		
		$storeCount	 = Store::where('mobile','=',$input['mobile'])->count();
		
		if($storeCount==1)
		{
		$storeData =	Store::where('mobile','=',$input['mobile'])->skip(0)->take(1)->get();
			$jsonData =	json_encode($storeData);
			$jsonDecode = json_decode($jsonData,true);
		$current_password = 	$jsonDecode[0]['password'];
		$storeId = 	$jsonDecode[0]['id'];
		$merchant_name = 	$jsonDecode[0]['title'];
		
		if(password_verify($input['password'], $current_password))
		{ 
       // if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password')])){ 
		
		//$store = Auth::store(); 
		//	$store = Store::findOrFail($storeId);
			
           //$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			// $store->update($input1);
			
			$StoreNew =$Data  = DB::table('stores')
		->select('stores.id','stores.title','stores.mobile','stores.is_otp_verified','stores.merchant_name','stores.email','stores.device_type','stores.device_id','stores.fcm_id','stores.image','stores.address','stores.opening_time','stores.closing_time','stores.auth_token','stores.is_featured','stores.is_active','categories.title as category_name')
		->leftjoin('categories','stores.category_id' ,'categories.id')
		->where('stores.id','=',$storeId)->get();
		   
		   
		   if($merchant_name=='')
		   {
				$is_shop_registred ='false';
		   }else
		   {
			   $is_shop_registred ='true';
		   }
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $StoreNew[0];
			$result['is_shop_registred'] = $is_shop_registred; 			
			$result['is_otp_verified'] = $StoreNew[0]->is_otp_verified; 			
			$result['msg'] = ''; 
			
			return response()->json($result, $this->successStatus);	
        } 
        else{ 
  
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Unauthorised'; 
				return response()->json($result, 200); 
		 
        }  
		}
		else{ 
         //   return response()->json(['error'=>'Unauthorised'], 200); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Unauthorised'; 
				return response()->json($result, 200); 
		 
        } 
    }
	
	
	
	
 public function add_store_coupon(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'title' 					=> 'required' , 
            'code' 						=> 'required' , 
			'image' => 'nullable|image|mimes:jpeg,png,jpg',
            'price' 					=> 'required' ,
            'category_id' 				=> 'required' ,
            'discount' 					=> 'required' ,
            'discount_type' 			=> 'required' ,
            'expiry' 					=> 'required',
            'detail' 					=> 'required',
            'is_featured' 				=> 'required',
            'is_exclusive' 				=> 'required',
            'auth_token' 				=> 'required',
            'store_id' 					=> 'required',
            'terms_conditions' 			=> 'required',
            'total_number_of_coupons' 	=> 'required',
            'coupon_user_limit' 	=> 'required',
			
        ]);	
		
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		$input = $request->all();
	
		
		if($input['discount_type']!='FLAT' && $input['discount_type']!='PERCENTAGE')
		{
			$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'invalid discount_type Perameters ! Value must be FLAT or PERCENTAGE'; 
				return response()->json($data, 200); 
		}	
		if($input['discount_type']=='%')
		{
			 $validator = Validator::make($request->all(), [ 
            'discount_upto' 			=> 'required' 
			]);
			
			
			if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
		}	
		}
		
		
		if($input['is_featured']!='1' && $input['is_featured']!='0')
		{
			$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'invalid is_featured Perameters ! Value must be 0 or 1'; 
				return response()->json($data, 200); 
		}
		if($input['is_exclusive']!='1' && $input['is_exclusive']!='0')
		{
			$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'invalid is_exclusive Perameters ! Value must be 0 or 1'; 
				return response()->json($data, 200); 
		}
		
		
		
		if ($file = $request->file('image')) {
			
			$optimizeImage = Image::make($file);
      $optimizePath = public_path().'/images/coupon/';
      $name = time().$file->getClientOriginalName();
      $optimizeImage->save($optimizePath.$name, 72);

			$input['image'] = $name;

		}
		

		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get()->count(); 
		
		if($storeCount==1)
		{
			$storeData =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get();
			$jsonStoreData = json_decode(json_encode($storeData),true);
		//	print_r($jsonStoreData[0]['city_id']);
			 $jsonStoreData[0]['city_id'];
			$input['city_id'] = $jsonStoreData[0]['city_id'];
			$coupon =Coupon::create($input); 
			$success['couponid'] =  $coupon->id;	
			$data = array();
			$data['status'] = 'success'; 
			$data['data'] = $success; 
			$data['msg'] = 'add Successfully'; 
			return response()->json($data, $this->successStatus); 
		}else
		{
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = []; 
				$data['msg'] = 'Autharization Failed'; 
				return response()->json($data, 200); 		
		} 	
}
        
        public function add_coupon_image(Request $request){
            
            $validator = Validator::make($request->all(), [ 
            'auth_token' 			=> 'required',
            'coupon_id'         =>  'required'
        ]);
        
        if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
			$input = $request->all();
			$CouponimageList = Couponimage::where('coupon_id', '=', $input['coupon_id'])->get();
			if(count($CouponimageList)<=10){
		  
    
            if ($file = $request->file('image')) {
			
			$optimizeImage = Image::make($file);
              $optimizePath = public_path().'/images/coupon/';
              $name = time().$file->getClientOriginalName();
              $optimizeImage->save($optimizePath.$name, 72);
        
        			$input['image'] = $name;
        
        		}
			
			$Data = new Couponimage($input);
					
				
	        
		    
	//	$Data  = DB::table('order_trans')
	//					->select('coupons.*','order_trans.qty','order_trans.code','order_trans.is_redeem',
	//						'coupons.image','coupons.expiry','stores.title as store_name')
	//					->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
	//					->leftjoin('stores','order_trans.store_id' ,'stores.id')
	//					->where('order_trans.store_id' ,'=' , $input['store_id'])
	//					->where('is_redeem' ,'=' ,'N')
	//					->get();
		
		
		

		if($Data->save())	
		{
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $Data; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus); 	
			//return response()->json(['success'=>$Data], $this->successStatus); 
		}else
		{
			
			$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = []; 
			$data['msg'] = 'No records Found'; 
			return response()->json($data, 200); 
				//return response()->json(['Failed'=>'Failed']); 
		}
			}else
		{
			
					$data = array();
					$data['status'] = 'failed'; 
					$data['data'] = []; 
					$data['msg'] = 'Autharization Failed'; 
			return response()->json($data, 200); 
			
		
		
	}
		
	    
	}
	
	public function get_store_coupon_info(Request $request)
	{
	    $validator = Validator::make($request->all(), [ 
            'coupon_id' => 'required'           
        ]);
        if ($validator->fails()) { 
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
				
		        return response()->json($data);          
        }
        
        $Data  = DB::table('coupons')
					->select('coupons.*','coupons.title as coupon_name','stores.title as store_name','categories.title as category_name','categories.image as cat_image')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
				->where('coupons.id', '=', $request->coupon_id)
				->first();
		
		$Data->couponimage = DB::table('coupon_image')
                        ->where('coupon_image.coupon_id', '=', $request->coupon_id)
                        ->select('coupon_image.image as couponimage')
                        ->get()->toArray();
                        
                        $Data->couponimage[count($Data->couponimage)]['couponimage'] = $Data->image;
                        
        $result = array();
    	$result['status'] = 'success'; 
    	$result['data'] = $Data; 
    	$result['msg'] = ''; 
    	return response()->json($result); 
	}


		public function get_store_coupons(Request $request)
	{
		
		 $validator = Validator::make($request->all(), [ 
            'auth_token' 			=> 'required',
            'store_id' 			=> 'required'
        ]);	
		
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		$input = $request->all();
		
		
		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get()->count(); 
		
		if($storeCount==1)
		{
		    
		
			$Data  = DB::table('coupons')
					->select('coupons.*','coupons.title as coupon_name','stores.title as store_name','categories.title as category_name','categories.image as cat_image')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
				->where('coupons.store_id', '=', $input['store_id'])
				->orderBy('coupons.created_at', 'DESC')
				
                ->get();
                foreach($Data as $d){
                    // $couponimage = Couponimage::where('coupon_id',$d->id)->get();
                    $couponimage = DB::table('coupon_image')
                                    ->where('coupon_image.coupon_id', '=', $d->id)
                                    ->select('coupon_image.image as couponimage')
                                    ->get();
                    
                    $d->couponimage = $couponimage;
                }
	
	//	$Data  = DB::table('order_trans')
	//					->select('coupons.*','order_trans.qty','order_trans.code','order_trans.is_redeem',
	//						'coupons.image','coupons.expiry','stores.title as store_name')
	//					->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
	//					->leftjoin('stores','order_trans.store_id' ,'stores.id')
	//					->where('order_trans.store_id' ,'=' , $input['store_id'])
	//					->where('is_redeem' ,'=' ,'N')
	//					->get();
		
		
		

		if(count($Data)>0)	
		{
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $Data; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus); 	
			//return response()->json(['success'=>$Data], $this->successStatus); 
		}else
		{
			
			$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = []; 
			$data['msg'] = 'No records Found'; 
			return response()->json($data, 200); 
				//return response()->json(['Failed'=>'Failed']); 
		}
		
		}else
		{
			
					$data = array();
					$data['status'] = 'failed'; 
					$data['data'] = []; 
					$data['msg'] = 'Autharization Failed'; 
			return response()->json($data, 200); 
			
		
		
	}






		}
		
		
			   
	   public function store_redeemed_coupons(Request $request) 
    { 
        // dd($request);
        $validator = Validator::make($request->all(), [ 
           'auth_token' => 'required',
            'store_id' 	=> 'required'             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
          
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 


			//return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get()->count(); 
		
		if($storeCount==1)
		{
		
		$cartCount = 	Order_trans::where('store_id' ,'=' , $input['store_id'])
						->where('is_redeem' ,'=' ,'Y')
						->count();
		
		if($cartCount>0)
		{
			$data  = DB::table('order_trans')
						->select('coupons.*','order_trans.qty','order_trans.code','order_trans.is_redeem',
							'coupons.image','coupons.expiry','stores.title as store_name')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->where('order_trans.store_id' ,'=' , $input['store_id'])
						->where('is_redeem' ,'=' ,'Y')
						->get();
					
					
				$result['status'] = 'success'; 
				$result['data'] =$data; 
				$result['msg'] = ''; 
				return response()->json($result, 200); 		
					
		//	return response()->json(['success'=>$data], $this-> successStatus); 			
		}else
		{
			// return response()->json(['error'=>'No records Found'], 200); 
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 		
		}
		
		

	}else
		{	
					$data = array();
						$data['status'] = 'failed'; 
						$data['data'] = []; 
						$data['msg'] = 'Autharization Failed'; 
					return response()->json($data, 200); 
		}
	}	  




	public function store_sales_report(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
           'auth_token' 	=> 'required',
            'store_id' 		=> 'required',             
            'date_range' 	=> 'required'            
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
          
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 


			//return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		if($input['date_range']=='1Day' || $input['date_range']=='4Months' || $input['date_range']=='12Months')
		{
			
			
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid date_range values of date_range must 1Day or 4Months or 12Months '; 
				return response()->json($result, 200); 
		}
		
		
		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get()->count(); 
							
		
		if($storeCount==1)
		{
		
		if($input['date_range']=='1Day')
		{
		    
			$dateS = $yesterday = Carbon::today();
			$dateE = Carbon::now()->EndOfDay(); 
			
			
		}elseif($input['date_range']=='4Months')
		{
			$dateS = Carbon::now()->startOfMonth()->subMonth(3);
			$dateE = Carbon::now()->startOfMonth(); 
			
		}elseif($input['date_range']=='12Months')
		{
			$dateS = Carbon::now()->startOfMonth()->subMonth(12);
			$dateE = Carbon::now()->startOfMonth(); 
		}
	

//	print_r($date);
		$cartCount = 	Order_trans::where('store_id' ,'=' , $input['store_id'])
					->whereBetween('order_trans.created_at',[$dateS,$dateE])
						->count();
	   	
		if($cartCount>0)
		{
			$data  = DB::table('order_trans')
						->select('coupons.*','order_trans.qty','order_trans.code','order_trans.is_redeem','order_trans.user_id','order_trans.store_id',
							'coupons.image','coupons.expiry','stores.title as store_name','users.name as user_name' , 'users.email as user_email', 'users.email as user_email' , 'users.mobile as user_mobile')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->leftjoin('users','order_trans.user_id' ,'users.id')
						->where('order_trans.store_id' ,'=' , $input['store_id'])
					//	->where('created_at' ,'>=' ,DATEADD(MONTH, -3, GETDATE()))
						->whereBetween('order_trans.created_at',[$dateS,$dateE])
						->get();
					
					
				$result['status'] = 'success'; 
				$result['data'] =$data; 
				$result['msg'] = ''; 
				return response()->json($result, 200); 		
					
		//	return response()->json(['success'=>$data], $this-> successStatus); 			
		}else
		{
			// return response()->json(['error'=>'No records Found'], 200); 
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 		
		}
		
		

	}else
		{	
					$data = array();
						$data['status'] = 'failed'; 
						$data['data'] = []; 
						$data['msg'] = 'Autharization Failed'; 
					return response()->json($data, 200); 
		}
	}	
		
	

	    public function store_coupon_redeem(Request $request) 
    { 
			$validator = Validator::make($request->all(), [ 
            'store_id' => 'required', 	
            'user_id' 	 => 'required'   ,  
            'coupon_id' 	 => 'required'   ,  
            'auth_token' 	 => 'required'     
        ]);
		
		if ($validator->fails()) { 
		
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($validator->errors(), 200); 
		
           // return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		$input = $request->all(); 
		
		
		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get()->count(); 
		if($storeCount==1)
		{
		
		$store = Store::findOrFail($input['store_id']);
	//	$user->update($input);	
//	echo $store->otp;

			$Order_transCount =Order_trans::where('store_id','=',$input['store_id'])
							->where('coupon_id','=',$input['coupon_id'])
							->where('user_id','=',$input['user_id'])
							->get()->count(); 

		if ($Order_transCount==1)
		{
			//$user = User::where($arr);
			$user = User::findOrFail($input['user_id']);
			$input['is_otp_verified']=0;
			$otp = rand(100000,999999);
// 			$msg    = urlencode("Your one time password in ".$setting->w_title." is ".$otp." ");

//             $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);
    
//             $sms    = file_get_contents($apiUrl);
			$input['otp']= $otp;
			
			$user->update($input);			
			$success = array('otp'=>$input['otp'] , 'is_otp_verified'=>0);
			//$success['is_otp_verified'] = 0;
			//$success['otp'] =  $user->otp;
			
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);
			
			//return response()->json(['success'=>$success], $this-> successStatus); 			
		}else
		{
			//return response()->json(['error'=>'Invalid OTP'], 200); 
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Coupon Not Found'; 
				return response()->json($result, 200); 
		}
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 
		}
		
		
		}  
	
	 
// this api is not used in this project 
	 public function send_otp_to_coupon_redeem(Request $request) 
    { 
			$validator = Validator::make($request->all(), [ 
            'store_id' => 'required', 	
            'user_mobile' 	 => 'required'   ,  
            'auth_token' 	 => 'required'     
        ]);
		
		if ($validator->fails()) { 
		
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($validator->errors(), 200); 
		
           // return response()->json(['error'=>$validator->errors()], 200);            
        }
		$setting = Settings::find(1);
		$input = $request->all(); 
		
		
		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get()->count(); 
		if($storeCount==1)
		{
		
	//	$user = User::findOrFail($input['user_id']);
	//	$user->update($input);	
//	echo $store->otp;
		
		$user = User::where('mobile', '=' , $input['user_mobile'])->get();
		
		//print_r($user[0]);
	//	print_r($user);
		if(count($user))
		{
		$jsonEncode = json_encode($user);
		$jsonDecode   = json_decode($jsonEncode,true);	
		$userID = 	$jsonDecode[0]['id'];
			//$arr = array();
			$user = User::findOrFail($userID);
			$otp  = rand(100000,999999);
			
// 			$msg    = urlencode("Your one time password in ".$setting->w_title." is ".$otp." ");

//             $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);
    
//             $sms    = file_get_contents($apiUrl);
			
			$arr['otp'] = $otp ;
			$arr['is_otp_verified'] = 0 ;
			$user->update($arr);
			//		->where($arr);			
			$success = array();
			
			$success['otp'] =  $otp  ; // $user->otp;
			$success['userid'] =  $userID  ; // $user->otp;
			
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = 'OTP Sent to user Successfully'; 
			return response()->json($result, $this->successStatus);
			
			//return response()->json(['success'=>$success], $this-> successStatus); 			
		
		}else{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User not Found'; 
				return response()->json($result, 200); 
			
		}	
		
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 
		}
		
		
		}  
	// note used 		
	 public function verify_otp_to_coupon_redeem(Request $request) 
    { 
			$validator = Validator::make($request->all(), [ 
            'store_id' => 'required', 	
            'user_id' 	 => 'required'   ,  
            'otp' 	 => 'required'   ,  
            'auth_token' 	 => 'required'     
        ]);
		
		if ($validator->fails()) { 
		
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($validator->errors(), 200); 
		
           // return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		$input = $request->all(); 
		
		
		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							->get()->count(); 
		if($storeCount==1)
		{
		
	//	$user = User::findOrFail($input['user_id']);
	//	$user->update($input);	
//	echo $store->otp;
		
		$userCount = User::where('id', '=' , $input['user_id'])
					  ->where('otp', '=' , $input['otp'])->get()->count();
		
		//print_r($user[0]);
	//	print_r($user);
		if($userCount==1)
		{
		$user = User::findOrFail($input['user_id']);
		$arr['is_otp_verified'] = 1 ;
			//$user->update($arr);
			//		->where($arr);	

		$storeData = Store::findOrFail($input['store_id']);	
		$storeArr = array();
		 $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$StoreautToken = 	substr(str_shuffle(str_repeat($pool, 16)), 0, 16);
		$storeArr['coupon_redeem_token'] = $StoreautToken;
		$storeData->update($storeArr);
			
			$success = array();
			
			$success['userid'] =  $input['user_id'] ; // $user->otp;
			
			
			// get Coupon List 
			
			//	$cartCount = Order_trans::where('user_id' ,'=' , $input['user_id'])
			//		 ->count();
		
	//	if($cartCount>0)
	//	{
			$coupon_list  = DB::table('order_trans')
						->select('order_trans.id','order_trans.qty','order_trans.code','order_trans.is_redeem',
							'coupons.image','coupons.expiry','stores.title as store_name')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->where('order_trans.store_id' ,'=' , $input['store_id'])
						->where('order_trans.is_redeem' ,'=' , 'N')
						->get();
					
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['coupon_list'] = $coupon_list; 
			$result['coupon_redeem_token'] = $StoreautToken; 
			$result['msg'] = 'OTP Verified Successfully'; 
			return response()->json($result, $this->successStatus);
			
			//return response()->json(['success'=>$success], $this-> successStatus); 			
		
		}else{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid OTP'; 
				return response()->json($result, 200); 
			
		}	
		
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 
		}
		
		
		}  
	






 public function store_otp_verify_to_coupon_redeem(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 	
			'coupon_id' 					 => 'required', 	
            'user_id' 				 => 'required',   
            'store_id' 				 => 'required',   
            'otp' 					 => 'required',  
            'auth_token' 	 		 => 'required' 
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
           
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 	
		   
		   // return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		
		$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							//->where('coupon_redeem_token','=',$input['coupon_redeem_token'])
							->get()->count(); 
		if($storeCount==1)
		{
		
			$userCount = User::where('id', '=' , $input['user_id'])
					  ->where('otp', '=' , $input['otp'])->get()->count();
		
		//print_r($user[0]);
	//	print_r($user);
		
							
		if($userCount==1)
		{					
							
		
		$date = 	date('Y-m-d');
		$cartCount = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
						->where('order_trans.store_id' ,'=' , $input['store_id'])
						->where('order_trans.coupon_id' ,'=' , $input['coupon_id'])
						->get();
		
			
		if(count($cartCount)>0)
		{
			$jsonEncode =  json_Encode($cartCount);
			$jsonDecode  =  json_decode($jsonEncode,true);
			$order_TransID = $jsonDecode[0]['id'];
			$cartCount_Exp = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
				->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
				->where('order_trans.id' ,'=' , $order_TransID)
				->whereDate('coupons.expiry' ,'<' , $date)
				->count();	
			
			if($cartCount_Exp>0)
			{
				
				 $result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Coupon Code Expired'; 
				return response()->json($result, 200); 	
				
				//return response()->json(['error'=>'Coupon Code Expired']);
			}else{
				
				$cartCount_redeem = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
				
				->where('order_trans.id' ,'=' , $order_TransID)
				->where('order_trans.is_redeem' ,'=' , 'Y')
				->count();	
				
				if($cartCount_redeem>0)
				{
					 $result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Coupon Already Used'; 
				return response()->json($result, 200); 
					//return response()->json(['error'=>'Coupon Already Used']);
				}
				else
				{
					 $coupon_redeem = DB::table('order_trans')
					->where('order_trans.user_id' ,'=' , $input['user_id'])
					->where('order_trans.id' ,'=' , $order_TransID)
					->where('order_trans.is_redeem' ,'=' , 'N')
					->update(['is_redeem'=>'Y']);
					

				$Coupondata  = DB::table('order_trans')
						->select('order_trans.discount','order_trans.discount_type','order_trans.discount_upto')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->get();
					
				$result['status'] = 'success'; 
				$result['data'] =$Coupondata; 
				$result['msg'] = ''; 
				return response()->json($result, 200); 

					
				//	return response()->json(['success'=>$Coupondata], $this-> successStatus); 	
				}
			
				
				
			}
			
				
			 			
		}else
		{ 
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Coupon Not Found'; 
				return response()->json($result, 200); 
			 
		}
		
		


	}
		else
		{ 
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Invalid OTP'; 
				return response()->json($result, 200); 
			 
		}

		}
		else
		{ 
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 
			 
		}
		


	}
	
	public function store_change_password(Request $request)    
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'store_id' => 'required',
            'old_password' => 'required' ,
            'new_password' => 'required',
			'confirm_password' =>'required|same:new_password',
			'auth_token' =>'required'
        ]);
	
		if ($validator->fails()) { 
          ///  return response()->json(['error'=>$validator->errors()], 200);  
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] = $validator->errors(); 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 
          
        }
		
		$input = $request->all();
			$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							//->where('coupon_redeem_token','=',$input['coupon_redeem_token'])
							->get()->count(); 
		if($storeCount==1)
		{
	  
		//$store =  Store::findOrFail($id);
		 $store = Store::find($input['store_id']);
		// print_r($store);
	//  $store = array();
	  
		
		 $current_password = $store['password'];           
		if(password_verify($input['old_password'], $current_password))
		{  
		
		                    
        $obj_store = Store::find($input['store_id']);
        $obj_store->password = bcrypt($input['new_password']);
        $obj_store->save();
		//return response()->json(['success'=>'Password Changed Successfully'], $this-> successStatus); 
		
		$result = array();
				$result['status'] = 'success'; 
				$result['data'] =[]; 
				$result['msg'] = 'Password Changed Successfully'; 
				return response()->json($result, $this-> successStatus); 
		
		
		}else
      {           
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Please enter correct Old password'; 
				return response()->json($result, 200); 
      }
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 
			//return response()->json(['error'=>'Incorrect old Password'], 200); 
		}
	
	}
	



	public function store_resend_otp(Request $request)    
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'store_id' => 'required' ,
            'auth_token' => 'required' 
        ]);
	
		if ($validator->fails()) { 
				//return response()->json(['error'=>$validator->errors()], 200);            
        
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 
		
		
		}
		
		
		$input = $request->all(); 
				$storeCount =Store::where('auth_token','=',$input['auth_token'])
							->where('id','=',$input['store_id'])
							//->where('coupon_redeem_token','=',$input['coupon_redeem_token'])
							->get()->count(); 
		
		   
		if($storeCount>0)
		{
					
			$id = $input['store_id'] ;
			$user = Store::findOrFail($id);
			$otp = rand(100000,999999);
// 			$msg    = urlencode("Your one time password in ".$setting->w_title." is ".$otp." ");

//             $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);

//             $sms    = file_get_contents($apiUrl);
			$input['otp'] =  $otp ;
			$user->update($input);	
			
			$success['otp'] =  $otp;
			
				$result = array();
				$result['status'] = 'success'; 
				$result['data'] =$success; 
				$result['msg'] = 'OTP Send Successfully'; 
				return response()->json($result, 200); 	
			
		}else
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Autharization Failed'; 
				return response()->json($result, 200); 	
		}
	
	}
	

function store_contact_us()
{
	$contact = Settings::findOrFail(1);	
	
	$data = array('phone'=>$contact['store_contact_phone'],'sddress'=>$contact['store_contact_address'],'email'=>$contact['store_contact_email'],);
	
	$result = array();
				$result['status'] = 'success'; 
				$result['data'] =$data; 
				$result['msg'] = ''; 
				return response()->json($result); 	
}



	
	public function store_forgot_password(Request $request){ 
	    $setting = Settings::find(1);
	
			
		 $validator = Validator::make($request->all(), [ 
            'mobile' => 'required',
            
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =''; 
					$result['msg'] = 'Invalid Parameters'; 
					return response()->json($result, 200);        
			}
		
	
			$input = $request->all(); 
		
      //  if(Auth::attempt(['mobile' => $input['mobile']])){ 
	  
	 // $userData = User::where('mobile',$input['mobile'])->get();
	  $count = 	Store::where('mobile','=',$input['mobile'])->get()->count();
		if($count==1)
		{
			 $userData = 	Store::where('mobile','=',$input['mobile'])->get();
	//	$user = Auth::user(); 
				$jsonEncode = 	json_encode($userData);
				$json_decode = json_decode($jsonEncode,true);
			$user1 = Store::findOrFail($json_decode['0']['id']);
			
			$userOTP  = rand(100000,999999);
			
// 			$msg    = urlencode("Your one time password in ".$setting->w_title." is ".$userOTP." ");
            
//             $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);
    
//             $sms    = file_get_contents($apiUrl);
			
			$input1['otp'] = $userOTP;
			$input1['is_otp_verified'] = 0;
			
           // $success['token'] =  $user->createToken('MyApp')-> accessToken; 
			 $user1->update($input1);
			
			
		   
		   $success = array('otp'=>$userOTP,'user_id'=>$json_decode['0']['id'],'is_otp_verified'=>0);
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = 'OTP Sent Successfully'; 
			return response()->json($result, $this->successStatus);	
		   
		   
           // return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
         //   return response()->json(['error'=>'Unauthorised'], 200); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Not Found'; 
				return response()->json($result, 200); 
		 
        } 
    }
	
	
	
	
	public function store_reset_password(Request $request){ 
	    
			
		 $validator = Validator::make($request->all(), [ 
            'store_id' => 'required',
          //  'otp' => 'required',
            'new_password' => 'required'
            
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =['error'=>$validator->errors()]; 
					$result['msg'] = 'Invalid Parameters1'; 
					return response()->json($result, 200);        
			}
		
	$input = $request->all(); 
	
		  $count = 	Store::where('id','=',$input['store_id'])
			->where('is_otp_verified','=','1')				
			->get()->count();
			
        if($count==1){ 
		
		 
			$user1 = Store::findOrFail($input['store_id']);
			
			$input1['password'] = bcrypt(request('new_password'));
			$input1['is_otp_verified'] =1;
			
			 //$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			 $user1->update($input1);
			
			
		   $success = array('store_id'=>$input['store_id'],'is_otp_verified'=>1);
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		   
		   
           // return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
         //   return response()->json(['error'=>'Unauthorised'], 200); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'OTP not Verified'; 
				return response()->json($result, 200); 
		 
        } 
    }
	
	
		public function store_reset_password_otp_veriy(Request $request){ 
	
			
		 $validator = Validator::make($request->all(), [ 
            'store_id' => 'required',
            'otp' => 'required'
           
            
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =['error'=>$validator->errors()]; 
					$result['msg'] = 'Invalid Parameters1'; 
					return response()->json($result, 200);        
			}
		
	$input = $request->all(); 
	
		  $count = 	Store::where('id','=',$input['store_id'])
			->where('otp','=',$input['otp'])				
			->get()->count();
        if($count==1){ 
		
		 
			$user1 = Store::findOrFail($input['store_id']);
		
			$userUp = 	 DB::table('stores')
				 ->where('id','=',$input['store_id'])
				 ->update(['is_otp_verified'=>'1']);
			
		   $success = array('store_id'=>$input['store_id'],'is_otp_verified'=>1);
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		   
		   
           // return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
         //   return response()->json(['error'=>'Unauthorised'], 200); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid OTP'; 
				return response()->json($result, 200); 
		 
        } 
    }
	
	
	
	






}

