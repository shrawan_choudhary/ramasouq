<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Category; 
use Illuminate\Support\Facades\Auth; 
use Validator;



class Categoryapi extends Controller
{
    //
	public $successStatus = 200;
	public function getcatlist()
	{
		$Data =  Category::all('id','slug','title','image','is_active','color_code','created_at','updated_at');
		if(count($Data)>0)	
		{
			unset($Data['icon']); 
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $Data; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		//	return response()->json(['success'=>$Data], $this-> successStatus); 
		}else
		{
			
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 
				//return response()->json(['Failed'=>'Failed'], $this-> successStatus); 
		}
	}
	
}
