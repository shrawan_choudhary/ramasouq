<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Order; 
use App\Cart; 
use App\Order_trans; 
use App\Coupon; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB; 
use Validator;



class Orderapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	
    public function add_order(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		$input = $request->all();
        $Order =Order::create($input); 
        $success['orderid'] =  $Order->id;
		
		$CartList = Cart::where('user_id','=',$input['user_id'])
					->get();	
		//if(is_array($CartList)
		//{
			foreach($CartList as $key=>$val)
			{
			
				$CouponData = Coupon::find($val->coupon_id);		
				//print_r($CouponData);	
				//exit;
				$cartData['coupon_id'] = $val->coupon_id;
				$cartData['store_id'] = $val->store_id;
				$cartData['qty'] = $val->qty;
				$cartData['user_id'] = $val->user_id;
				$cartData['code'] = $CouponData->code;
				$cartData['price'] = $CouponData->price;
				$cartData['discount'] = $CouponData->discount;
				$cartData['order_id'] = $Order->id;
				$order_trans = Order_trans::create($cartData);
				
	//	}
		}
	//	print_r($CartList['items:protected']);
		
		
		return response()->json(['success'=>$success], $this-> successStatus); 	
	}  


		



    public function my_purchased_coupons(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		
		
		$cartCount = Order_trans::where('user_id' ,'=' , $input['user_id'])
					 ->count();
		
		if($cartCount>0)
		{
			$data  = DB::table('order_trans')
						->select('order_trans.qty','order_trans.code','order_trans.is_redeem',
							'coupons.image','coupons.expiry','stores.title as stror_name')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->get();
					
					
			return response()->json(['success'=>$data], $this-> successStatus); 			
		}else
		{
			 return response()->json(['error'=>'No records Found'], 401);    
		}
		
		

	}
	   
	   
	   
	   public function my_used_coupons(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		
		
		$cartCount = 	Order_trans::where('user_id' ,'=' , $input['user_id'])
						->where('is_redeem' ,'=' ,'Y')
						->count();
		
		if($cartCount>0)
		{
			$data  = DB::table('order_trans')
						->select('order_trans.qty','order_trans.code','order_trans.is_redeem',
							'coupons.image','coupons.expiry','stores.title as stror_name')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->where('is_redeem' ,'=' ,'Y')
						->get();
					
					
			return response()->json(['success'=>$data], $this-> successStatus); 			
		}else
		{
			 return response()->json(['error'=>'No records Found'], 401);    
		}
		
		

	}
	  

	  public function my_expired_coupons(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		
		$date = 	date('Y-m-d');
		$cartCount = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')	
						->whereDate('coupons.expiry' ,'<' , $date)
						->count();
		
		if($cartCount>0)
		{
			
			$data  = DB::table('order_trans')
						->select('order_trans.qty','order_trans.code','order_trans.is_redeem',
							'coupons.image','coupons.expiry','stores.title as stror_name')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->whereDate('coupons.expiry' ,'<' , $date)
						->get();
					
					
			return response()->json(['success'=>$data], $this-> successStatus); 			
		}else
		{
			 return response()->json(['error'=>'No records Found'], 401);    
		}
		
		

	}
	
		  public function redeem_coupon(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'coupon_code' => 'required',
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		
		$date = 	date('Y-m-d');
		$cartCount = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
						->where('order_trans.code' ,'=' , $input['coupon_code'])
						->count();
		
		if($cartCount>0)
		{
			$cartCount_Exp = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
				->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
				->where('order_trans.code' ,'=' , $input['coupon_code'])
				->whereDate('coupons.expiry' ,'<' , $date)
				->count();	
			
			if($cartCount_Exp>0)
			{
				return response()->json(['error'=>'Coupon Code Expired']);
			}else{
				
				$cartCount_redeem = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
				
				->where('order_trans.code' ,'=' , $input['coupon_code'])
				->where('order_trans.is_redeem' ,'=' , 'Y')
				->count();	
				
				if($cartCount_redeem>0)
				{
					return response()->json(['error'=>'Coupon Already Used']);
				}else
				{
					 $coupon_redeem = DB::table('order_trans')
					->where('order_trans.user_id' ,'=' , $input['user_id'])
					->where('order_trans.code' ,'=' , $input['coupon_code'])
					->where('order_trans.is_redeem' ,'=' , 'Y')
					->update(['is_redeem'=>'Y']);
					

				$Coupondata  = DB::table('order_trans')
						->select('order_trans.discount')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->get();
					


					
					return response()->json(['success'=>$Coupondata], $this-> successStatus); 	
				}
			
				
				
			}
			
				
			 			
		}else
		{ 
			 return response()->json(['error'=>'Invalid Coupon Code'], 401);  		  
		}
		
		

	}
	
	
}
