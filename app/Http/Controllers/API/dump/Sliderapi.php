<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Slider; 
use Illuminate\Support\Facades\Auth; 
use Validator;



class Sliderapi extends Controller
{
    //
	public $successStatus = 200;
	
	public function getslider()
	{
		$Data = Slider::all();
		if($Data)	
		{
			return response()->json(['success'=>$Data], $this-> successStatus); 
		}else
		{
			return response()->json(['Failed'=>'Failed'], $this-> successStatus); 
		}
	
	
	}
	
}
