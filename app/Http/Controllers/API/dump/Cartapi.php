<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Cart; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB; 
use Validator;



class Cartapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	
    public function add_to_cart(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'coupon_id' => 'required', 
            'store_id' 	=> 'required', 
            'qty' 		=> 'required' ,
            'user_id' 		=> 'required' 
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		$input = $request->all();
	
		 $count =   Cart::where('user_id' , '=' , $input['user_id'])
			  ->where('coupon_id' , '=' ,$input['coupon_id'])
			  ->count();
		
		if($count==0)
		{
        $cart =Cart::create($input); 
      //  $success['token'] =  $Cart->createToken('MyApp')-> accessToken; 
        $success['cartid'] =  $cart->id;
		return response()->json(['success'=>$success], $this-> successStatus); 
		}else
		{
			 return response()->json(['error'=>'Coupon Already Added in Cart'], 401);  
		}
	
	
	
	
	}  


    public function get_cart_list(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
             
        ]);
		
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 
		
		$cartCount = Cart::where('user_id' ,'=' , $input['user_id'])
				->count();
		
		if($cartCount>0)
		{
			$data  = DB::table('cart')
						->select('coupons.title','coupons.expiry','coupons.price','coupons.discount','coupons.detail','coupons.image',
							'cart.qty','stores.title as stror_name')
						->leftjoin('coupons','cart.coupon_id' ,'coupons.id')
						->leftjoin('stores','cart.store_id' ,'stores.id')
						->get();
					
					
			return response()->json(['success'=>$data], $this-> successStatus); 			
		}else
		{
			 return response()->json(['error'=>'No records Found'], 401);    
		}
		}  


	
	
}
