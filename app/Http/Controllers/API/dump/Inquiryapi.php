<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Inquiry; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB; 
use Validator;



class Inquiryapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	
    public function add_to_inquiry(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' 	=> 'required', 
            'mobile_no' 		=> 'required' ,
            'message' 		=> 'required' 
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		$input = $request->all();
	
		
		$inquiry =Inquiry::create($input); 
      //  $success['token'] =  $Cart->createToken('MyApp')-> accessToken; 
        $success['inquiryid'] =  $inquiry->id;
		return response()->json(['success'=>$success], $this-> successStatus); 
		
	}  

	
	
}
