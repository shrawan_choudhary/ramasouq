<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Coupon; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;  
use Validator;



class Couponapi extends Controller
{
    //
	public $successStatus = 200;
	
	public function getcoupons(Request $request)
	{
	
		$validator = Validator::make($request->all(), [ 
            'category_id' => 'required'             
        ]);
	
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 		
		$Data = Coupon::select('*')
                ->where('is_active', '=', 1)
                ->where('category_id', '=', $input['category_id'])
                ->get();

		if($Data)	
		{
			return response()->json(['success'=>$Data], $this->successStatus); 
		}else
		{
			return response()->json(['Failed'=>'Failed']); 
		}
	}
	
	
	public function getrecentcoupons()
	{
	
		$Data  = DB::table('coupons')
		->select('coupons.*','categories.title as category_name','stores.title as stror_name')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
				->where('coupons.is_active', '=', 1)
				->orderBy('coupons.created_at', 'DESC')
				->skip(0)->take(5)
                ->get();
	
	
	
	//	$Data = Coupon::select('*')
    //            ->where('is_active', '=', 1)
	//			->orderBy('created_at', 'DESC')
	//			->skip(0)->take(5)
    //            ->get();

		if($Data)	
		{
			return response()->json(['success'=>$Data], $this->successStatus); 
		}else
		{
			return response()->json(['Failed'=>'Failed']); 
		}
	}
		public function getfeaturedcoupons()
	{
			$Data  = DB::table('coupons')
		->select('coupons.*','stores.title as stror_name,','categories.title as category_name')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
				->where('coupons.is_featured', '=', 1)
				->orderBy('coupons.created_at', 'DESC')
				->skip(0)->take(5)
                ->get();
	
		

		if($Data)	
		{
			return response()->json(['success'=>$Data], $this->successStatus); 
		}else
		{
			return response()->json(['Failed'=>'Failed']); 
		}
	}
	
	public function search_coupon(Request $request)
	{
	
		$validator = Validator::make($request->all(), [ 
            'search_key' => 'required'             
        ]);
	
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 		
		//$Data = Coupon::select('*')
		
		$Data  = DB::table('coupons')
		->select('coupons.*','stores.title as stror_name,','categories.title as category_name')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
		
		
                ->where('coupons.is_active', '=', 1)
                ->where('coupons.title', 'like',  '%' . $input['search_key'] . '%' )
                ->orwhere('coupons.code', 'like',  '%' . $input['search_key'] . '%' )
                ->orwhere('stores.title', 'like',  '%' . $input['search_key'] . '%' )
                ->get();

		if(count($Data)>0)	
		{
			return response()->json(['success'=>$Data], $this->successStatus); 
		}else
		{
			return response()->json(['Failed'=>'No records found']); 
		}
	}
	
}
