<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\City; 
use Illuminate\Support\Facades\Auth; 
use Validator;



class Cityapi extends Controller
{
    //
	public $successStatus = 200;
	
	public function getcitylist()
	{
		//return City::all();
		
		$Data = City::all();
		if(count($Data)>0)	
		{
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $Data; 
			$result['msg'] = ''; 
            return response()->json($result, 200);
		} else {
			
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 
				//return response()->json(['Failed'=>'Failed'], $this-> successStatus); 
		}	
	}
}
