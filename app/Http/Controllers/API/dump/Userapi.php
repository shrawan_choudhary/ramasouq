<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;



class Userapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	public function login(){ 
	
	
		
        if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
			unset($user['otp']);
			unset($user['brief']);
			unset($user['is_admin']);
			unset($user['image']);
			unset($user['website']);
			unset($user['google_id']);
			unset($user['facebook_id']);
			unset($user['dob']);
			unset($user['gender']);
		   $success['userData'] =  $user; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
	
	
	
	
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'device_id' => 'required', 
            'fcm_id' => 'required', 
            'device_type' => 'required', 
           
	//     'userid' => 'required', 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password'
		   
            
        ]);
if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		$input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
		$input['otp'] = rand(100000,999999);
		$rnd = rand(100000,999999);
	//	$input['email'] = "demo@gmail$rnd.com";
		$input['is_otp_verified'] = "0";
	//	$id = $input['id'];
        $user =User::create($input); 
	//	print_R($user);
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['userid'] =  $user->id;
        $success['otp'] =  $user->otp;
        $success['is_otp_verified'] =  $user->is_otp_verified;
		
			unset($user['otp']);
			unset($user['brief']);
			unset($user['is_admin']);
			unset($user['image']);
			unset($user['website']);
			unset($user['google_id']);
			unset($user['facebook_id']);
			unset($user['dob']);
			unset($user['gender']);
		   $success['userData'] =  $user; 
		
		
		return response()->json(['success'=>$success], $this-> successStatus); 
    }  


    public function otp_verify(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'userid' => 'required', 
            'otp' 	 => 'required'     
        ]);
		
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 
		
		$user = User::findOrFail($input['userid']);
		if ($user->otp ==$input['otp'])
		{
			//$user = User::where($arr);
			$input['is_otp_verified']=1;
			$user->update($input);			
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			$success['is_otp_verified'] =  1;
			//$success['otp'] =  $user->otp;
			return response()->json(['success'=>$success], $this-> successStatus); 			
		}else
		{
			return response()->json(['error'=>'Invalid OTP'], 401); 
			
		}
		
		}  


	public function edit_profile(Request $request) 
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'userid' => 'required', 
            'name' => 'required', 
            'email' => 'required|email', 
            
        ]);
	
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 
        $id = $input['userid'] ;
        $user = User::findOrFail($id);
		if($user!=null)
		{
			$user->update($input);	
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			
				unset($user['otp']);
				unset($user['brief']);
				unset($user['is_admin']);
				unset($user['image']);
				unset($user['website']);
				unset($user['google_id']);
				unset($user['facebook_id']);
				unset($user['dob']);
				unset($user['gender']);
			
			$success['userData'] =  $user;
	
			return response()->json(['success'=>$success], $this-> successStatus); 
		}else
		{
			return response()->json(['error'=>'User Id Not Found'], 401); 
		}
	
	}
	
	
	public function resend_otp(Request $request)    
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'userid' => 'required' 
        ]);
	
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 
        $id = $input['userid'] ;
		$otp = rand(100000,999999);
		$input['otp'] =  $otp ;
        $user = User::findOrFail($id);
		if($user!=null)
		{
			$user->update($input);	
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			$success['otp'] =  $otp;
			//$success['otp'] =  $user->otp;
			return response()->json(['success'=>$success], $this-> successStatus); 
		}else
		{
			return response()->json(['error'=>'User Id Not Found'], 401); 
		}
	
	}
	
	
	public function change_password(Request $request)    
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'userid' => 'required',
            'old_password' => 'required' ,
            'new_password' => 'required',
			'confirm_password' =>'required|same:new_password',
        ]);
	
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 
        $id = $input['userid'] ;
	  
	   if(Auth::Check())
	{
		
		 $current_password = Auth::User()->password;           
		if(password_verify($input['old_password'], $current_password))
		{  
		
		 $user_id = Auth::User()->id;                       
        $obj_user = User::find($user_id);
        $obj_user->password = bcrypt($input['new_password']);
        $obj_user->save();
		return response()->json(['success'=>'Password Changed Successfully'], $this-> successStatus); 
		
		}else
      {           
				//$error = array('current-password' => 'Please enter correct current password');
        return response()->json(['error'=>'Please enter correct current password'], 401);
      }
		
		}else
		{
			return response()->json(['error'=>'Incorrect old Password'], 401); 
		}
	
	}
	
	
	
}
