<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Pages; 
use Illuminate\Support\Facades\Auth; 
use Validator;



class Pagesapi extends Controller
{
    //
	public $successStatus = 200;
	
	public function getpage(Request $request)
	{
		
		 $validator = Validator::make($request->all(), [ 
            'page_id' => 'required' 
        ]);
	
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 
		
		$Data = pages::where('id','=',$input['page_id'])
						->get();
		if($Data)	
		{
			return response()->json(['success'=>$Data], $this-> successStatus); 
		}else
		{
			return response()->json(['Failed'=>'Failed'], $this-> successStatus); 
		}
	
	
	}
	
}
