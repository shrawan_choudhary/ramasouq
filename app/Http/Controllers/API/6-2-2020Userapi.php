<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;



class Userapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	public function login(Request $request){ 
	
			
		 $validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'password' 	=> 'required', 
            'fcm_id' 		=> 'required' ,
            'device_id' 		=> 'required' ,
            'device_type' 		=> 'required' 
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =''; 
					$result['msg'] = 'Invalid Parameters'; 
					return response()->json($result, 401);        
			}
		
	
	
		
        if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password')])){ 
		
		$user = Auth::user(); 
			$user1 = User::findOrFail($user['id']);
			
			$input1['fcm_id'] = request('fcm_id');
			$input1['device_id'] = request('device_id');
			$input1['device_type'] = request('device_type');
			
			
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
			 $user1->update($input1);
			
			$userNew = User::where('id','=',$user['id'])->get();
			
            $user = $userNew;
			unset($user['otp']);
			unset($user['brief']);
			unset($user['is_admin']);
			unset($user['image']);
			unset($user['website']);
			unset($user['google_id']);
			unset($user['facebook_id']);
			unset($user['dob']);
			unset($user['gender']);
			unset($user['confirmation_code']);
		   $success['userData'] =  $user; 
		   
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		   
		   
           // return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
         //   return response()->json(['error'=>'Unauthorised'], 401); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Unauthorised'; 
				return response()->json($result, 401); 
		 
        } 
    }
	
	
	
	
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'device_id' => 'required', 
            'fcm_id' => 'required', 
            'device_type' => 'required', 
           
	//     'userid' => 'required', 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password'
		   
            
        ]);
if ($validator->fails()) { 
          
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 401); 

	
			//return response()->json(['error'=>$validator->errors()], 401);            
        }
		$input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
		$input['otp'] = rand(100000,999999);
		$rnd = rand(100000,999999);
	//	$input['email'] = "demo@gmail$rnd.com";
		$input['is_otp_verified'] = "0";
	//	$id = $input['id'];
        $user =User::create($input); 
	//	print_R($user);
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['userid'] =  $user->id;
        $success['otp'] =  $user->otp;
        $success['is_otp_verified'] =  $user->is_otp_verified;
		
			unset($user['otp']);
			unset($user['brief']);
			unset($user['is_admin']);
			unset($user['image']);
			unset($user['website']);
			unset($user['google_id']);
			unset($user['facebook_id']);
			unset($user['dob']);
			unset($user['gender']);
		   $success['userData'] =  $user; 
		
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		
	//	return response()->json(['success'=>$success], $this-> successStatus); 
    }  


    public function otp_verify(Request $request) 
    { //722491
        $validator = Validator::make($request->all(), [ 
            'userid' => 'required', 
            'otp' 	 => 'required'     
        ]);
		
		if ($validator->fails()) { 
		
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($validator->errors(), 401); 
		
           // return response()->json(['error'=>$validator->errors()], 401);            
        }
		
		$input = $request->all(); 
		
		$user = User::findOrFail($input['userid']);
	//	$user->update($input);	
		if ($user->otp ==$input['otp'])
		{
			//$user = User::where($arr);
			$input['is_otp_verified']=1;
			$user->update($input);			
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			$success['is_otp_verified'] =  1;
			//$success['otp'] =  $user->otp;
			
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = 'OTP Verified Successfully'; 
			return response()->json($result, $this->successStatus);
			
			//return response()->json(['success'=>$success], $this-> successStatus); 			
		}else
		{
			return response()->json(['error'=>'Invalid OTP'], 401); 
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid OTP'; 
				return response()->json($result, 401); 
		}
		
		}  


	public function edit_profile(Request $request) 
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'userid' => 'required', 
            'name' => 'required', 
            'email' => 'required|email', 
            
        ]);
	
		if ($validator->fails()) { 
         //   return response()->json(['error'=>$validator->errors()], 401); 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($validator->errors(), 401); 
		 
        }
		
		$input = $request->all(); 
        $id = $input['userid'] ;
        $user = User::findOrFail($id);
		if($user!=null)
		{
			$user->update($input);	
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			
				unset($user['otp']);
				unset($user['brief']);
				unset($user['is_admin']);
				unset($user['image']);
				unset($user['website']);
				unset($user['google_id']);
				unset($user['facebook_id']);
				unset($user['dob']);
				unset($user['gender']);
			
			$success['userData'] =  $user;
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);
			
			
			//return response()->json(['success'=>$success], $this-> successStatus); 
		}else
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Id Not Found'; 
				return response()->json($result, 401); 
		//	return response()->json(['error'=>'User Id Not Found'], 401); 
		}
	
	}
	
	
	public function resend_otp(Request $request)    
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'userid' => 'required' 
        ]);
	
		if ($validator->fails()) { 
				//return response()->json(['error'=>$validator->errors()], 401);            
        
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 401); 
		
		
		}
		
		$input = $request->all(); 
        $id = $input['userid'] ;
		$otp = rand(100000,999999);
		$input['otp'] =  $otp ;
        $user = User::findOrFail($id);
		if($user!=null)
		{
			$user->update($input);	
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			$success['otp'] =  $otp;
			//$success['otp'] =  $user->otp;
			return response()->json(['success'=>$success], $this-> successStatus); 
		}else
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 401); 	
			//return response()->json(['error'=>'User Id Not Found'], 401); 
		}
	
	}
	
	
	public function change_password(Request $request)    
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'userid' => 'required',
            'old_password' => 'required' ,
            'new_password' => 'required',
			'confirm_password' =>'required|same:new_password',
        ]);
	
		if ($validator->fails()) { 
          ///  return response()->json(['error'=>$validator->errors()], 401);  
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 401); 
          
        }
		
		$input = $request->all(); 
        $id = $input['userid'] ;
	  
	   if(Auth::Check())
	{
		
		 $current_password = Auth::User()->password;           
		if(password_verify($input['old_password'], $current_password))
		{  
		
		 $user_id = Auth::User()->id;                       
        $obj_user = User::find($user_id);
        $obj_user->password = bcrypt($input['new_password']);
        $obj_user->save();
		//return response()->json(['success'=>'Password Changed Successfully'], $this-> successStatus); 
		
		$result = array();
				$result['status'] = 'success'; 
				$result['data'] =[]; 
				$result['msg'] = 'Password Changed Successfully'; 
				return response()->json($result, $this-> successStatus); 
		
		
		}else
      {           
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Please enter correct current password'; 
				return response()->json($result, 401); 
      }
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Incorrect old Password'; 
				return response()->json($result, 401); 
			//return response()->json(['error'=>'Incorrect old Password'], 401); 
		}
	
	}
	
	
	
}
