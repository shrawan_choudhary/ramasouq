<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Notification; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB; 
use Validator;



class Notificationapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	
    public function get_notifications(Request $request) 
    { 
       $input = $request->all();
	if(isset($input['Utype']))
	{
		   $validator = Validator::make($request->all(), [ 
				'user_id' => 'required', 
		]);
		
	}else
	{
		
		 $validator = Validator::make($request->all(), [ 
				'store_id' => 'required', 
		]);
		
	}
		
		
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		
	
		if(isset($input['store_id']))
		{
		$DataN  = DB::table('notification')
		->select('notification.id','notification.message','users.name as username','stores.title as storename')
						->leftjoin('users','notification.user_id' ,'users.id')
						->leftjoin('stores','notification.store_id' ,'stores.id')
				->where('notification.store_id', '=', $input['store_id'])
				->orderBy('notification.created_at', 'DESC')
				
                ->get();
		}
		
		if(isset($input['user_id']))
		{
		$DataN  = DB::table('notification')
		->select('notification.id','notification.message','users.name as username','stores.title as storename')
						->leftjoin('users','notification.user_id' ,'users.id')
						->leftjoin('stores','notification.store_id' ,'stores.id')
				->where('notification.user_id', '=', $input['user_id'])
				->orderBy('notification.created_at', 'DESC')
				
                ->get();
		}
		
		
		
		if(count($DataN)>0)
		{
		
      //  $success['token'] =  $Cart->createToken('MyApp')-> accessToken; 
       
		
		$data = array();
		$data['status'] = 'success'; 
		$data['data'] = $DataN; 
		$data['msg'] = ''; 
		return response()->json($data, $this->successStatus); 
		}else
		{
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = []; 
				$data['msg'] = 'No Records Found'; 
				return response()->json($data, 200); 
		}
	}  

	
	public function getNotificationCount(Request $request)
	{
		
		 $input = $request->all();
	if(isset($input['Utype']))
	{
		   $validator = Validator::make($request->all(), [ 
				'user_id' => 'required', 
		]);
		
	}else
	{
		
		 $validator = Validator::make($request->all(), [ 
				'store_id' => 'required', 
		]);
		
	}
		
		
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		
	if(isset($input['store_id']))
		{
		$notification_count  = Notification::where('store_id', '=', $input['store_id'])
	            ->get()->count();
		}
		
		if(isset($input['user_id']))
		{
				$notification_count  = Notification::where('user_id', '=', $input['user_id'])
	            ->get()->count();
		}
		
		
		
			$data = array();
			$DataN = array('notification_count'=>$notification_count);
			$data['status'] = 'success'; 
			$data['data'] = $DataN; 
			$data['msg'] = ''; 
			return response()->json($data, $this->successStatus); 
		
	}
	
	
}
