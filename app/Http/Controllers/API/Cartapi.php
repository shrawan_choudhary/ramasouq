<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Cart; 
use App\User; 
use App\Coupon; 
use App\Order_trans; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB; 
use Validator;



class Cartapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	
    public function add_to_cart(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'coupon_id' => 'required', 
            'store_id' 	=> 'required', 
            'qty' 		=> 'required' ,
            'user_id' 		=> 'required' 
        ]);
        if ($validator->fails()) { 
			$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200);        
        }
		$input = $request->all();
	
	 $CouponData =   Coupon::findOrFail($input['coupon_id']);
			  	
		$userLimitCount = $CouponData['coupon_user_limit'];	
		$totalNoCoupon  = $CouponData['total_number_of_coupons'];
		
		
		$OrderTransCount = 	Order_trans::where('user_id' , '=' , $input['user_id'])
		 ->where('coupon_id' , '=' ,$input['coupon_id'])->sum('qty');
		 $remain_count = 0;
		 if($input['qty']>$totalNoCoupon){
		     $result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'No more coupon left'; 
				return response()->json($result, 200); 
		 }
		 if($input['qty']>$userLimitCount)
		{
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'User Limit Exceed'; 
				return response()->json($result, 200); 
		}
		
		if($OrderTransCount != '0'){
		 
		 if($userLimitCount>=$OrderTransCount){
		     
		     $remain_count = $userLimitCount - $OrderTransCount;
		     
		     if($input['qty']>=$remain_count)
        		{
        				$result['status'] = 'failed'; 
        				$result['data'] =''; 
        				$result['msg'] = 'You have already purchased '. $OrderTransCount .' coupon'; 
        				return response()->json($result, 200); 
        		}
		 } 
		}
		
		
		
		
		
		
		 
	    
		
// 		$FinalUserLimit = $OrderTransCount ;
// 		dd(1>2 && 2>1);
		
// 		if($input['qty']>$FinalUserLimit )
// 		{
// 			    $result['status'] = 'failed'; 
// 			    $result['data'] =''; 
// 			    $result['msg'] = 'User Limit Exceed'; 
// 			    return response()->json($result, 200); 
			
// 		}
// 		dd('d');
		$count =   Cart::where('user_id' , '=' , $input['user_id'])
			  ->where('coupon_id' , '=' ,$input['coupon_id'])
			  ->count();
		
		
		
		
		if($count==0)
		{
			$cart =Cart::create($input); 
			//  $success['token'] =  $Cart->createToken('MyApp')-> accessToken; 
        $success['cartid'] =  $cart->id;
		//	return response()->json(['success'=>$success], $this-> successStatus); 
		
				$result['status'] = 'success'; 
				$result['data'] =$success; 
				$result['msg'] = 'Coupon Added in Cart'; 
				return response()->json($result, 200); 	
		
		
		}else
		{
			
			 $count =   Cart::where('user_id' , '=' , $input['user_id'])
			  ->where('coupon_id' , '=' ,$input['coupon_id'])
			  ->update(['qty'=>$input['qty']]);

			//return response()->json(['error'=>'Coupon Already Added in Cart'], 200);
				$result['status'] = 'success'; 
				$result['data'] =''; 
				$result['msg'] = 'Cart Updated'; 
				return response()->json($result, 200); 			 
		}
	
	
	
	
	}  


    public function get_cart_list(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
             
        ]);
		
		if ($validator->fails()) { 
        //    return response()->json(['error'=>$validator->errors()], 200);            
        	$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200);
		
		}
		
		$input = $request->all(); 
		
		$cartCount = Cart::where('user_id' ,'=' , $input['user_id'])
				->count();
		
		if($cartCount>0)
		{
			$data  = DB::table('cart')
						->select('coupons.*','cart.qty','stores.title as store_name','cart.id as cart_id')
						->leftjoin('coupons','cart.coupon_id' ,'coupons.id')
						->leftjoin('stores','cart.store_id' ,'stores.id')
						->where('cart.user_id' ,'=' , $input['user_id'])
						->get();
					
			$result = array();
				$result['status'] = 'success'; 
				$result['data'] =$data; 
				$result['msg'] = ''; 
				return response()->json($result, 200);		
		}else
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200);
		
		}
		}  

    public function remove_cart_item(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'cart_id' => 'required', 
             
        ]);
		
		if ($validator->fails()) { 
          //  return response()->json(['error'=>$validator->errors()], 200); 
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200);
			
        }
		
		$input = $request->all(); 
		
		$cartCount = Cart::where('id' ,'=' , $input['cart_id'])->delete();
				$result = array();
				$result['status'] = 'success'; 
				$result['data'] =''; 
				$result['msg'] = 'Cart Item Removed'; 
				return response()->json($result, 200);
		
		
}

	public function getcartCount(Request $request)
	{
		 $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
             
        ]);
		
		$input  =  $request->all();
			$cartCount = Cart::where('user_id' ,'=' , $input['user_id'])
				->count();
		$result = array();
	
		$cartCount = array('CartCount'=>$cartCount);
				$result['status'] = 'success'; 
				$result['data'] =$cartCount; 
				$result['msg'] = ''; 
				return response()->json($result, 200);
	}




}