<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Coupon; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;  
use Validator;


class Couponapi extends Controller
{
    //
	public $successStatus = 200;
	
	public function coupon_info(Request $request) {
	    $validator = Validator::make($request->all(), [ 
            'coupon_id' => 'required'           
        ]);
        if ($validator->fails()) { 
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
				
		        return response()->json($data);          
        }
        
        $details  = DB::table('coupons')
						->select('coupons.*','coupons.title as coupon_name','stores.title as store_name,','categories.title as category_name', 'categories.image AS cat_image')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
				->where('coupons.is_active', '=', 1)
				->where('coupons.is_front', '=', 1)
				->where('coupons.id', $request->coupon_id)
                ->first();
                
        $couponimage = DB::table('coupon_image')
                        ->where('coupon_image.coupon_id', '=', $details->id)
                        ->select('coupon_image.image as couponimage')
                        ->get()->toArray();
        
        $couponimage[count($couponimage)]['couponimage'] = $details->image;
        
        $details->couponimage = $couponimage;
                
        $data = array();
		$data['status'] = 'success'; 
		$data['data'] = $details; 
		$data['msg'] = 'Data found.'; 
		
        return response()->json($data);  
	}
	
	public function getcoupons(Request $request)
	{
	
		$validator = Validator::make($request->all(), [ 
            'category_id' => 'required',             
            'city_id' => 'required'             
        ]);
	
		if ($validator->fails()) { 
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
		
        //    return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		$input = $request->all(); 	

	
			$Data  = DB::table('coupons')
						->select('coupons.*','coupons.title as coupon_name','stores.title as store_name,','categories.title as category_name', 'categories.image AS cat_image')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
				->where('coupons.is_active', '=', 1)
				->where('coupons.is_front', '=', 1)
                ->where('coupons.category_id', '=', $input['category_id'])
                ->where('coupons.city_id', '=', $input['city_id'])
				->get();

		
		//			$Data = Coupon::select('*')
        //        ->where('is_active', '=', 1)
        //        ->where('category_id', '=', $input['category_id'])
        //        ->get();

		if(count($Data)>0)	
		{
		    foreach($Data as $key => $d){
                $couponimage = DB::table('coupon_image')
                                ->where('coupon_image.coupon_id', '=', $d->id)
                                ->select('coupon_image.image as couponimage')
                                ->get()->toArray();
                
                $couponimage[count($couponimage)]['couponimage'] = $d->image;
                
                $Data[$key]->couponimage = $couponimage;
            }
            
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $Data; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus); 
			//return response()->json(['success'=>$Data], $this->successStatus); 
		}else
		{
			$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = []; 
			$data['msg'] = 'No records Found'; 
			return response()->json($data, 200); 
		}
	}
	
	
	
	public function getrecentcoupons(Request $request)
	{
	
	$validator = Validator::make($request->all(), [ 
                
            'city_id' => 'required'             
        ]);
	
		if ($validator->fails()) { 
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
		}
		
	$input = $request->all(); 	
		$Data  = DB::table('coupons')
		->select('coupons.*','coupons.title as coupon_name','categories.title as category_name','stores.title as store_name', 'categories.image AS cat_image')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
				->where('coupons.is_active', '=', 1)
				->where('coupons.city_id', '=', $input['city_id'])
				->orderBy('coupons.created_at', 'DESC')
				->skip(0)->take(5)
                ->get();

		if(count($Data)>0)	
		{
		    foreach($Data as $key => $d){
                $couponimage = DB::table('coupon_image')
                                ->where('coupon_image.coupon_id', '=', $d->id)
                                ->select('coupon_image.image as couponimage')
                                ->get()->toArray();
                
                $couponimage[count($couponimage)]['couponimage'] = $d->image;
                
                $Data[$key]->couponimage = $couponimage;
            }
            
			//	strip_tags(htmlspecialchars_decode($desc))
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $Data; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus); 	



		}else
		{
				$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = []; 
			$data['msg'] = 'No records Found'; 
			return response()->json($data, 200); 
			//return response()->json(['Failed'=>'Failed']); 
		}
	}
	
	public function getfeaturedcoupons(Request $request)
	{
		
			$validator = Validator::make($request->all(), [ 
                
            'city_id' => 'required'             
        ]);
	
		if ($validator->fails()) { 
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
		}
		
	$input = $request->all(); 
		
			$Data  = DB::table('coupons')
		->select('coupons.*','coupons.title as coupon_name','stores.title as store_name','categories.title as category_name', 'categories.image AS cat_image')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
				->where('coupons.is_featured', '=', 1)
				->where('coupons.city_id', '=', $input['city_id'])
				->orderBy('coupons.created_at', 'DESC')
				->skip(0)->take(5)
                ->get();
	
		

		if(count($Data)>0)	
		{
		    foreach($Data as $key => $d){
                $couponimage = DB::table('coupon_image')
                                ->where('coupon_image.coupon_id', '=', $d->id)
                                ->select('coupon_image.image as couponimage')
                                ->get()->toArray();
                
                $couponimage[count($couponimage)]['couponimage'] = $d->image;
                
                $Data[$key]->couponimage = $couponimage;
            }
            
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $Data; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus); 	
			//return response()->json(['success'=>$Data], $this->successStatus); 
		}else
		{
			
			$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = []; 
			$data['msg'] = 'No records Found'; 
			return response()->json($data, 200); 
				//return response()->json(['Failed'=>'Failed']); 
		}
	}
	
	public function search_coupon(Request $request)
	{
	
		$validator = Validator::make($request->all(), [ 
            'search_key' => 'required' ,            
            'city_id' => 'required'             
        ]);
	
		if ($validator->fails()) { 
			
			$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = $validator->errors(); 
			$data['msg'] = 'Invalid Perameters'; 
			
			return response()->json($data, 200); 	

		  }
		
		$input = $request->all(); 		
		//$Data = Coupon::select('*')
		
		$Data  = DB::table('coupons')
		->select('coupons.*','coupons.title as coupon_name','stores.title as store_name,','categories.title as category_name', 'categories.image AS cat_image')
						->leftjoin('stores','coupons.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
		
		
                ->where('coupons.is_active', '=', 1)
                ->where('coupons.city_id', '=', $input['city_id'])
                ->where('coupons.title', 'like',  '%' . $input['search_key'] . '%' )
                ->orwhere('coupons.code', 'like',  '%' . $input['search_key'] . '%' )
                ->orwhere('stores.title', 'like',  '%' . $input['search_key'] . '%' )
                ->get();

		if (count($Data) > 0) {
		    foreach($Data as $key => $d){
                $couponimage = DB::table('coupon_image')
                                ->where('coupon_image.coupon_id', '=', $d->id)
                                ->select('coupon_image.image as couponimage')
                                ->get()->toArray();
                
                $couponimage[count($couponimage)]['couponimage'] = $d->image;
                
                $Data[$key]->couponimage = $couponimage;
            }
		    
			$data = array();
    		$result['status'] = 'success'; 
    		$result['data'] = $Data; 
    		$result['msg'] = ''; 
    		return response()->json($result, $this->successStatus); 
			// return response()->json(['success'=>$Data], $this->successStatus); 
		} else {
			$data = array();
			$data['status'] = 'failed'; 
			$data['data'] = $validator->errors(); 
			$data['msg'] = 'No records found'; 
			return response()->json($data, 200); 
			
			// return response()->json(['Failed'=>'No records found']); 
		}
	}
	
}
