<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Chat; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB; 
use Validator;



class Chatapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	
    public function send_msg(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'message' 	=> 'required'
			
        ]);
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		$input = $request->all();
	
		$input['msg_send_by']='user'; 
		$inquiry =Chat::create($input); 
      //  $success['token'] =  $Cart->createToken('MyApp')-> accessToken; 
       
	$last20Msg = Chat::where('user_id','=',$input['user_id'])
		
		->orderBy('chat.created_at', 'DESC')
				->skip(0)->take(20)
                ->get();
		
		
		
		$data = array();
		$data['status'] = 'success'; 
		$data['data'] = $last20Msg; 
		$data['msg'] = 'Msg Sent Successfully'; 
		return response()->json($data, $this->successStatus); 
		
	}   


	public function chat_history(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required'
          
			
        ]);
	if ($validator->fails()) {
				$data = array();
				$data['status'] = 'failed'; 
				$data['data'] = $validator->errors(); 
				$data['msg'] = 'Invalid Perameters'; 
		return response()->json($data, 200); 
			          
			}
		$input = $request->all();
	       
	$last20Msg = Chat::where('user_id','=',$input['user_id'])
		
		->orderBy('chat.created_at', 'DESC')
				->skip(0)->take(200)
                ->get();
		
		
		
		$data = array();
		$data['status'] = 'success'; 
		$data['data'] = $last20Msg; 
		$data['msg'] = 'Fetch Successfully'; 
		return response()->json($data, $this->successStatus); 
		
	}  


	

	
	
}
