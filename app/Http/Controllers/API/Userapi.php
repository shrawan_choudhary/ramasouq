<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User;
use App\Settings;
use Image; 
use Illuminate\Support\Facades\Auth; 
use Validator;



class Userapi extends Controller
{
    //
	public $successStatus = 200;
	
	public function login(Request $request){ 	
			$validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'password' 	=> 'required', 
            'fcm_id' 		=> 'required' ,
            'device_id' 		=> 'required' ,
            'device_type' 		=> 'required' 
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =''; 
					$result['msg'] = 'Invalid Parameters'; 
					return response()->json($result, 200);        
			}
		
	
	
		
        if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password')])){ 
		
		$user = Auth::user(); 
			$user1 = User::findOrFail($user['id']);
			
			$input1['fcm_id'] = request('fcm_id');
			$input1['device_id'] = request('device_id');
			$input1['device_type'] = request('device_type');
			
			
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
			 $user1->update($input1);
			
			$userNew = User::where('id','=',$user['id'])->get();
			
            $user = $userNew;
			unset($user['otp']);
			unset($user['brief']);
			unset($user['is_admin']);
			unset($user['image']);
			unset($user['website']);
			unset($user['google_id']);
			unset($user['facebook_id']);
			unset($user['dob']);
		
			unset($user['confirmation_code']);
		   $success['userData'] =  $user[0]; 
		   
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		   
		   
           // return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
         //   return response()->json(['error'=>'Unauthorised'], 200); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Unauthorised'; 
				return response()->json($result, 200); 
		 
        } 
    }
	
	
	
	
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required', 
            'device_id' => 'required', 
            'fcm_id' => 'required', 
            'device_type' => 'required', 
            'image' => 'nullable|image|mimes:jpeg,png,jpg',
	//     'userid' => 'required', 
            'name' => 'required', 
            'email' => 'required|email', 
            'gender' => 'required', 
            'password' => 'required', 
            'is_terms_and_condition_accepted' => 'required', 
            'c_password' => 'required|same:password'
		   
            
        ]);
if ($validator->fails()) { 
          
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 

	
			//return response()->json(['error'=>$validator->errors()], 200);            
        }
		$input = $request->all(); 
		
		
		if ($file = $request->file('image')) {
		  $optimizeImage = Image::make($file)->resize(100, null);
      $optimizePath = public_path().'/images/user/';
      $name = time().$file->getClientOriginalName();
      $optimizeImage->save($optimizePath.$name, 72);
		  $input['image'] = $name;
		}
		
		
			$count = 	User::where('mobile','=',$input['mobile'])->get()->count();
			$emailcount = 	User::where('email','=',$input['email'])->get()->count();
		
		if($count>0)
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Mobile No already exits'; 
				return response()->json($result, 200); 
		}elseif($emailcount>0)
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Email already exits'; 
				return response()->json($result, 200); 
		}
		else{
		
		
        $input['password'] = bcrypt($input['password']); 
		$input['otp'] = rand(100000,999999);
		$rnd = rand(100000,999999);
	//	$input['email'] = "demo@gmail$rnd.com";
		$input['is_otp_verified'] = "0";
	//	$id = $input['id'];
        $user =User::create($input); 
	//	print_R($user);
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['userid'] =  $user->id;
        $success['otp'] =  $user->otp;
        $success['is_otp_verified'] =  $user->is_otp_verified;
		
			unset($user['otp']);
			unset($user['brief']);
			unset($user['is_admin']);
			unset($user['image']);
			unset($user['website']);
			unset($user['google_id']);
			unset($user['facebook_id']);
			unset($user['dob']);
			
		   $success['userData'] =  $user; 
		
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		
	//	return response()->json(['success'=>$success], $this-> successStatus); 
		}
    }  


    public function otp_verify(Request $request) 
    { //722491
        $validator = Validator::make($request->all(), [ 
            'userid' => 'required', 
            'otp' 	 => 'required'     
        ]);
		
		if ($validator->fails()) { 
		
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($validator->errors(), 200); 
		
           // return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		$input = $request->all(); 
		
		$user = User::findOrFail($input['userid']);
	//	$user->update($input);	
		if ($user->otp ==$input['otp'])
		{
			//$user = User::where($arr);
			$input['is_otp_verified']=1;
			$user->update($input);			
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			$success['is_otp_verified'] =  1;
			//$success['otp'] =  $user->otp;
			
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = 'OTP Verified Successfully'; 
			return response()->json($result, $this->successStatus);
			
			//return response()->json(['success'=>$success], $this-> successStatus); 			
		}else
		{
			return response()->json(['error'=>'Invalid OTP'], 200); 
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid OTP'; 
				return response()->json($result, 200); 
		}
		
		}  


	public function edit_profile(Request $request) 
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'userid' => 'required', 
            'name' => 'required', 
            'email' => 'required|email', 
            
        ]);
	
		if ($validator->fails()) { 
         //   return response()->json(['error'=>$validator->errors()], 200); 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($validator->errors(), 200); 
		 
        }
		
		$input = $request->all(); 
        $id = $input['userid'] ;
        $user = User::findOrFail($id);
		if($user!=null)
		{
			$user->update($input);	
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			
				unset($user['otp']);
				unset($user['brief']);
				unset($user['is_admin']);
				unset($user['image']);
				unset($user['website']);
				unset($user['google_id']);
				unset($user['facebook_id']);
				unset($user['dob']);
				
			
			$success['userData'] =  $user;
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);
			
			
			//return response()->json(['success'=>$success], $this-> successStatus); 
		}else
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Id Not Found'; 
				return response()->json($result, 200); 
		//	return response()->json(['error'=>'User Id Not Found'], 200); 
		}
	
	}
	
	
	public function resend_otp(Request $request)    
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'userid' => 'required' 
        ]);
	
		if ($validator->fails()) { 
				//return response()->json(['error'=>$validator->errors()], 200);            
        
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 
		
		
		}
		
		$input = $request->all(); 
        $id = $input['userid'] ;
		$otp = rand(100000,999999);
		$input['otp'] =  $otp ;
        $user = User::findOrFail($id);
		if($user!=null)
		{
			$user->update($input);	
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			$success['otp'] =  $otp;
			//$success['otp'] =  $user->otp;
			return response()->json(['success'=>$success], $this-> successStatus); 
		}else
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 	
			//return response()->json(['error'=>'User Id Not Found'], 200); 
		}
	
	}
	
	
	public function change_password(Request $request)    
    { 
		// $user = Auth::user(); 
			
	  $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'old_password' => 'required' ,
            'new_password' => 'required',
			'confirm_password' =>'required|same:new_password',
        ]);
	
		if ($validator->fails()) { 
          ///  return response()->json(['error'=>$validator->errors()], 200);  
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 
          
        }
		
		$input = $request->all(); 
        $id = $input['user_id'] ;
	  
	   if(Auth::Check())
	{
		
		 $current_password = Auth::User()->password;           
		if(password_verify($input['old_password'], $current_password))
		{  
		
		 $user_id = Auth::User()->id;                       
        $obj_user = User::find($user_id);
        $obj_user->password = bcrypt($input['new_password']);
        $obj_user->save();
		//return response()->json(['success'=>'Password Changed Successfully'], $this-> successStatus); 
		
		$result = array();
				$result['status'] = 'success'; 
				$result['data'] =[]; 
				$result['msg'] = 'Password Changed Successfully'; 
				return response()->json($result, $this-> successStatus); 
		
		
		}else
      {           
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Please enter correct current password'; 
				return response()->json($result, 200); 
      }
		
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Incorrect old Password'; 
				return response()->json($result, 200); 
			//return response()->json(['error'=>'Incorrect old Password'], 200); 
		}
	
	}
	
	
	//public function forgot_password(Request $request)
	//{
	//	return array(
	//	'test'=>'test'
	//	);
	//	
	//}	
	
	
	public function forgot_password(Request $request){ 
	
			
		 $validator = Validator::make($request->all(), [ 
            'mobile' => 'required',
            
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =''; 
					$result['msg'] = 'Invalid Parameters'; 
					return response()->json($result, 200);        
			}
		
	
			$input = $request->all(); 
		
      //  if(Auth::attempt(['mobile' => $input['mobile']])){ 
	  
	 // $userData = User::where('mobile',$input['mobile'])->get();
	  $count = 	User::where('mobile','=',$input['mobile'])->get()->count();
		if($count==1)
		{
			 $userData = 	User::where('mobile','=',$input['mobile'])->get();
	//	$user = Auth::user(); 
				$jsonEncode = 	json_encode($userData);
				$json_decode = json_decode($jsonEncode,true);
			$user1 = User::findOrFail($json_decode['0']['id']);
			
			$userOTP  = rand(100000,999999);
			
			$input1['otp'] = $userOTP;
			$input1['is_otp_verified'] = 0;
			
           // $success['token'] =  $user->createToken('MyApp')-> accessToken; 
			 $user1->update($input1);
			
			
		   
		   $success = array('otp'=>$userOTP,'user_id'=>$json_decode['0']['id'],'is_otp_verified'=>0);
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = 'OTP Sent Successfully'; 
			return response()->json($result, $this->successStatus);	
		   
		   
           // return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
         //   return response()->json(['error'=>'Unauthorised'], 200); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Not Found'; 
				return response()->json($result, 200); 
		 
        } 
    }
	
	
	
	
	public function reset_password(Request $request){ 
	
			
		 $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'otp' => 'required',
            'new_password' => 'required'
            
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =['error'=>$validator->errors()]; 
					$result['msg'] = 'Invalid Parameters1'; 
					return response()->json($result, 200);        
			}
		
	$input = $request->all(); 
	
		  $count = 	User::where('id','=',$input['user_id'])
			->where('otp','=',$input['otp'])				
			->get()->count();
        if($count==1){ 
		
		 
			$user1 = User::findOrFail($input['user_id']);
			
			$input1['password'] = bcrypt(request('new_password'));
			$input1['is_otp_verified'] =1;
			
			 //$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			 $user1->update($input1);
			
			
		   $success = array('user_id'=>$input['user_id'],'is_otp_verified'=>1);
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		   
		   
           // return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
         //   return response()->json(['error'=>'Unauthorised'], 200); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid OTP'; 
				return response()->json($result, 200); 
		 
        } 
    }
//	store_reset_password_otp_veriy
	
	
		public function user_reset_password_otp_veriy(Request $request){ 
	
			
		 $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'otp' => 'required',
           
            
        ]);
					if ($validator->fails()) {
							
					$result['status'] = 'failed'; 
					$result['data'] =['error'=>$validator->errors()]; 
					$result['msg'] = 'Invalid Parameters1'; 
					return response()->json($result, 200);        
			}
		
	$input = $request->all(); 
	
		  $count = 	User::where('id','=',$input['user_id'])
			->where('otp','=',$input['otp'])				
			->get()->count();
        if($count==1){ 
		
		 
			$user1 = User::findOrFail($input['user_id']);
			
		//	$input1['password'] = bcrypt(request('new_password'));
			$input1['is_otp_verified'] =1;
			
			 //$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			 $user1->update($input1);
			
			
		   $success = array('user_id'=>$input['user_id'],'is_otp_verified'=>1);
		   $result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		   
		   
           // return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
         //   return response()->json(['error'=>'Unauthorised'], 200); 
		 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid OTP'; 
				return response()->json($result, 200); 
		 
        } 
    }
	
	
	
	  public function social_login(Request $request) 
    { 
		$input = $request->all(); 
			
	//	if (empty($input['mobile'])) { 
	//	
	//			$result = array();
	//			$result['status'] = 'failed'; 
	//			$result['data'] =[]; 
	//			$result['msg'] = 'Invalid Parameters'; 
	//			return response()->json($result, 200); 
	//	
    //       // return response()->json(['error'=>$validator->errors()], 200);            
    //    }elseif(empty($input['email_id']))
	//	{
	//		$result = array();
	//			$result['status'] = 'failed'; 
	//			$result['data'] =[]; 
	//			$result['msg'] = 'Invalid Parameters'; 
	//			return response()->json($result, 200); 
	//		
	//	}
		
		if(isset($input['mobile']))
		{	
			$UserCount = User::where('mobile','=',$input['mobile'])->get()->count(); 
		}elseif(isset($input['email_id']))
		{	
			$UserCount = User::where('email','=',$input['email_id'])->get()->count(); 
		}elseif(isset($input['facebook_id']))
		{	
			$UserCount = User::where('facebook_id','=',$input['facebook_id'])->get()->count(); 
		}elseif(isset($input['google_id']))
		{	
			$UserCount = User::where('google_id','=',$input['google_id'])->get()->count(); 
		}else
		{
			$UserCount=0;
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 
		}
		
		
		
		
		//$user = User::findOrFail($input['userid']);
	//	$user->update($input);	
		if ($UserCount==1)
		{
			
		if(isset($input['mobile']))
		{	
			$userdata = User::where('mobile','=',$input['mobile'])->get(); 
		}elseif(isset($input['email_id']))
		{	
			$userdata = User::where('email','=',$input['email_id'])->get(); 
		}elseif(isset($input['google_id']))
		{	
			$userdata = User::where('google_id','=',$input['google_id'])->get(); 
		}elseif(isset($input['facebook_id']))
		{	
			$userdata = User::where('facebook_id','=',$input['facebook_id'])->get(); 
		}else
		{	
			$userdata = array();
		}
			
			$userArr = json_decode(json_encode($userdata),true);
			$user = User::findOrFail($userArr[0]['id']);
			$input['is_otp_verified']=1;
			$user->update($input);			
			$success['token'] =  $user->createToken('MyApp')-> accessToken; 
			
			//$success['otp'] =  $user->otp;
			unset($userArr[0]['otp']);
			unset($userArr[0]['brief']);
			unset($userArr[0]['is_admin']);
			unset($userArr[0]['image']);
			unset($userArr[0]['website']);
			unset($userArr[0]['dob']);
			unset($userArr[0]['confirmation_code']);
			
			$success['user_id'] =   $userArr[0]['id'];
			$success['userData'] =  $userArr[0];
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);
		}else
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'User Not Found'; 
				return response()->json($result, 200); 
		}
		
		}  

	
	
	  public function social_register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
       //     'mobile' => 'required', 
            'device_id' => 'required', 
            'fcm_id' => 'required', 
            'device_type' => 'required', 
            'image' => 'nullable|image|mimes:jpeg,png,jpg',
		//     'userid' => 'required', 
       //     'name' => 'required', 
       //     'email' => 'required|email', 
       //     'gender' => 'required', 
            'is_terms_and_condition_accepted' => 'required'

			//'facebook_id' => 'required', 
            //'google_id' => 'required', 

			
        ]);
if ($validator->fails()) { 
          
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 

	
			//return response()->json(['error'=>$validator->errors()], 200);            
        }
		$input = $request->all(); 
		
		
		
		if(isset($input['facebook_id']) && isset($input['google_id']))
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] ='facebook_id or google_id required'; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 
			
		}
		
		
		
		if ($file = $request->file('image')) {
		  $optimizeImage = Image::make($file)->resize(100, null);
      $optimizePath = public_path().'/images/user/';
      $name = time().$file->getClientOriginalName();
      $optimizeImage->save($optimizePath.$name, 72);
		  $input['image'] = $name;
		}
		
			if(isset($input['facebook_id']))
			{	
				$count = 	User::where('facebook_id','=',$input['facebook_id'])->get()->count();
			}else
			{
				$count ='';
			}
			if(isset($input['google_id']))
			{	
				$googleCount = 	User::where('google_id','=',$input['google_id'])->get()->count();
			}else
			{
				$googleCount ='';
			}
			
			if(isset($input['email']))
			{	
				$emailcount = 	User::where('email','=',$input['email'])->get()->count();
			}else
			{
				$emailcount ='';
			}
			if(isset($input['mobile']))
			{	
				$mobile_count = 	User::where('mobile','=',$input['mobile'])->get()->count();
			}else
			{
				$mobile_count ='';
			}
		if($count>0)
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'User Already Exits'; 
				return response()->json($result, 200); 
		}elseif($googleCount>0)
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'User Already exits'; 
				return response()->json($result, 200); 
		}elseif($emailcount>0)
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'User Already exits'; 
				return response()->json($result, 200); 
			
		}elseif($mobile_count>0)
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =$validator->errors(); 
				$result['msg'] = 'Mobile No Already exits'; 
				return response()->json($result, 200); 
			
		}
		else{
		
		
       
	//	$input['email'] = "demo@gmail$rnd.com";
		$input['is_otp_verified'] = "0";
		$input['otp'] = rand(100000,999999);
	//	$id = $input['id'];
        $user =User::create($input); 
	//	print_R($user);
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['userid'] =  $user->id;
      
			$success['is_otp_verified'] =  $user->is_otp_verified;
			unset($user['brief']);
			unset($user['is_admin']);
			unset($user['image']);
			unset($user['website']);
			unset($user['google_id']);
			unset($user['facebook_id']);
			unset($user['dob']);
			unset($user['confirmation_code']);
			
		   $success['userData'] =  $user; 
		
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $success; 
			$result['msg'] = ''; 
			return response()->json($result, $this->successStatus);	
		
	//	return response()->json(['success'=>$success], $this-> successStatus); 
		}
    }  

	
	function contact_us()
{
	$contact = Settings::findOrFail(1);	
	
	$data = array('phone'=>$contact['w_email'],'address'=>$contact['w_address'],'email'=>$contact['w_email'],'phone'=>$contact['w_phone']);
	
	$result = array();
				$result['status'] = 'success'; 
				$result['data'] =$data; 
				$result['msg'] = ''; 
				return response()->json($result); 	
}
	
	
}
