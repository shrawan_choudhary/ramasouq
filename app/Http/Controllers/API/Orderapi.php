<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Order; 
use App\Cart; 
use App\Order_trans; 
use App\Coupon; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB; 
use Validator;



class Orderapi extends Controller
{
    //
	public $successStatus = 200;
	
	
	
    public function add_order(Request $request) 
    { 
        
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
        ]);
        
        if ($validator->fails()) { 
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 
					
					
					//return response()->json(['error'=>$validator->errors()], 200);            
        }
        
        $input = $request->all();
        $Order = new Order();
        $Order->transaction_id = $request->transaction_id;
        $Order->user_id = $request->user_id;
        $Order->status = $request->status;
        $Order->save();
        $success['orderid'] =  $Order->id;
        
		if($request->status == 'success')
		{
		$CartList = Cart::where('user_id','=',$input['user_id'])
					->get();
		
		
		    
			foreach($CartList as $key=>$val)
			{
			
				$CouponData = Coupon::find($val->coupon_id);
				
				
			//	print_r($CouponData);	
				if($CouponData['code']!='')
				{
				$cartData['coupon_id'] = $val->coupon_id;
				$cartData['store_id'] = $val->store_id;
				$cartData['qty'] = $val->qty;
				$cartData['user_id'] = $val->user_id;
				$cartData['code'] =  $CouponData['code'];
				$cartData['price'] = $CouponData['price'];
				
			//	$CouponDataDiscount = Coupon::findOrFail($input['coupon_id']);	
					
				$discount_type = $CouponData['discount_type'];
				
				if($discount_type=='PERCENTAGE')
				{
					$quantity = $val->qty;	
					
					$price = $CouponData['price'];
					
					$totalPrice = $price;
					$discount = $CouponData['discount'];
			//		exit;
					
					
					
					if($totalPrice>0)
					{
						 $totalPrice;
						 $discountAmt = ($discount*$totalPrice)/100;
					}else
					{
						$discountAmt=0;
					}
					if($discountAmt<=$CouponData['discount_upto'])	
					{
						$cartData['discount'] = round($discountAmt,2); 
					}else
					{
						$cartData['discount'] = $CouponData['discount_upto'];
					}
				}	
				
			//	exit;
			//	$cartData['discount'] = $CouponData->discount;
				$cartData['order_id'] = $Order->id;
				$order_trans = Order_trans::create($cartData);
				}
				
				$coupon = Coupon::where('id', $val->coupon_id)->get();
				foreach($coupon as $c){
		        $c->total_number_of_coupons = $c->total_number_of_coupons - $val->qty;
		        
		        $c->update();
		        
				}
				
		
	
		}
		
	//	print_r($CartList['items:protected']);
		Cart::where('user_id','=',$input['user_id'])->delete();
		
			
		
	} 
	
	$result = array();
				$result['status'] = 'success'; 
				$result['data'] =$success; 
				$result['msg'] = ''; 
				return response()->json($result, 200); 
		//return response()->json(['success'=>$success], $this-> successStatus); 
	    
	}  


		



    public function my_purchased_coupons(Request $request) 
    {   
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
          
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 

			//return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		
		
		$cartCount = Order_trans::where('user_id' ,'=' , $input['user_id'])
					 ->count();
		
		if($cartCount>0)
		{
			$data  = DB::table('order_trans')
						->select('coupons.*','order_trans.id as order_trans_id','order_trans.qty','order_trans.code','order_trans.is_redeem',
							'coupons.image','coupons.expiry','stores.title as store_name')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->where('order_trans.is_redeem' ,'=' , 'N')
						->get();
					
					
				//return response()->json(['success'=>$data], $this-> successStatus); 	
			$result = array();
				$result['status'] = 'success'; 
				$result['data'] =$data; 
				$result['msg'] = ''; 
				return response()->json($result, 200); 

			
		}else
		{
			$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 	
			// return response()->json(['error'=>'No records Found'], 200);    
		}
		
		

	}
	   
	   
	   
	   public function my_used_coupons(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
          
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 


			//return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		
		
		$cartCount = 	Order_trans::where('user_id' ,'=' , $input['user_id'])
						->where('is_redeem' ,'=' ,'Y')
						->count();
		
		if($cartCount>0)
		{
			$data  = DB::table('order_trans')
						->select('coupons.*','order_trans.qty','order_trans.code','order_trans.is_redeem',
							'coupons.image','coupons.expiry','stores.title as store_name')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->where('is_redeem' ,'=' ,'Y')
						->get();
					
					
				$result['status'] = 'success'; 
				$result['data'] =$data; 
				$result['msg'] = ''; 
				return response()->json($result, 200); 		
					
		//	return response()->json(['success'=>$data], $this-> successStatus); 			
		}else
		{
			// return response()->json(['error'=>'No records Found'], 200); 
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 		
		}
		
		

	}
	  

	  public function my_expired_coupons(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
          
		  $result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 	
		  //  return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		
		$date = 	date('Y-m-d');
		$cartCount = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')	
						->whereDate('coupons.expiry' ,'<' , $date)
						->count();
		
		if($cartCount>0)
		{
			
			$data  = DB::table('order_trans')
						->select('coupons.*','order_trans.qty','order_trans.code','order_trans.is_redeem',
							'coupons.image','coupons.expiry','stores.title as store_name')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->whereDate('coupons.expiry' ,'<' , $date)
						->get();
					
				$result['status'] = 'success'; 
				$result['data'] =$data; 
				$result['msg'] = ''; 
				return response()->json($result, 200); 		
			//return response()->json(['success'=>$data], $this-> successStatus); 			
		}else
		{
			$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'No records Found'; 
				return response()->json($result, 200); 		
			// return response()->json(['error'=>'No records Found'], 200);    
		}
		
		

	}
	
		  public function redeem_coupon(Request $request) 
    { 
       
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'coupon_id' => 'required',
             
        ]);
		
		
		$input = $request->all(); 
		if ($validator->fails()) { 
           
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Invalid Parameters'; 
				return response()->json($result, 200); 	
		   
		   // return response()->json(['error'=>$validator->errors()], 200);            
        }
		
		
		$date = 	date('Y-m-d');
		$cartCount = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
						->where('order_trans.coupon_id' ,'=' , $input['coupon_id'])
						->count();
		
		if($cartCount>0)
		{
			$cartCount_Exp = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
				->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
				->where('order_trans.coupon_id' ,'=' , $input['coupon_id'])
				->whereDate('coupons.expiry' ,'<' , $date)
				->count();	
			
			if($cartCount_Exp>0)
			{
				
				 $result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Coupon Code Expired'; 
				return response()->json($result, 200); 	
				
				//return response()->json(['error'=>'Coupon Code Expired']);
			}else{
				
				$cartCount_redeem = 	Order_trans::where('order_trans.user_id' ,'=' , $input['user_id'])
				->where('order_trans.coupon_id' ,'=' , $input['coupon_id'])
				->where('order_trans.is_redeem' ,'=' , 'Y')
				->count();	
				
				if($cartCount_redeem>0)
				{
					 $result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Coupon Already Used'; 
				return response()->json($result, 200); 
					//return response()->json(['error'=>'Coupon Already Used']);
				}else
				{
					 $coupon_redeem = DB::table('order_trans')
					->where('order_trans.user_id' ,'=' , $input['user_id'])
					->where('order_trans.coupon_id' ,'=' , $input['coupon_id'])
					->where('order_trans.is_redeem' ,'=' , 'N')
					->update(['is_redeem'=>'Y']);
				$Coupondata  = DB::table('order_trans')
						->select('order_trans.discount')
						->where('order_trans.user_id' ,'=' , $input['user_id'])
						->get();
				$result['status'] = 'success'; 
				$result['data'] =$Coupondata; 
				$result['msg'] = 'Your coupon is successfully redeemed. Thank you for using our service'; 
				return response()->json($result, 200); 

					
				//	return response()->json(['success'=>$Coupondata], $this-> successStatus); 	
				}
			
				
				
			}
			
				
			 			
		}else
		{ 
				$result['status'] = 'failed'; 
				$result['data'] =''; 
				$result['msg'] = 'Invalid Coupon Code'; 
				return response()->json($result, 200); 
			 
		}
		
		

	}
	
	
}
