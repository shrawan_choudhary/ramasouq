<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Pages; 
use Illuminate\Support\Facades\Auth; 
use Validator;



class Pagesapi extends Controller
{
    //
	public $successStatus = 200;
	
	public function getpage(Request $request)
	{
		
		 $validator = Validator::make($request->all(), [ 
            'page_id' => 'required' 
        ]);
	
		if ($validator->fails()) { 
          //  return response()->json(['error'=>$validator->errors()], 200);            
        
		$result = array();
				$result['status'] = 'failed'; 
				$result['data'] = $validator->errors(); 
				$result['msg'] = 'Invalid Perameters'; 
		return response()->json($result, 200); 
		
		}
		
		$input = $request->all(); 
		
		$Data = pages::where('id','=',$input['page_id'])
						->get();
		if(count($Data)>0)	
		{
			
			$result = array();
			$result['status'] = 'success'; 
			$result['data'] = $Data; 
			$result['msg'] = ''; 
		return response()->json($result, $this->successStatus);
		}else
		{
				$result = array();
				$result['status'] = 'failed'; 
				$result['data'] =[]; 
				$result['msg'] = 'No records Found'; 
		return response()->json($result, 200); 
			
			//return response()->json(['Failed'=>'Failed'], $this-> successStatus); 
		}
	
	
	}
	
}
