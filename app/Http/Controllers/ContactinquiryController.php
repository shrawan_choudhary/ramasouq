<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Contactinquiry;

class ContactinquiryController extends Controller {
    
    public function index(Request $request) {

        $inquiry    = Contactinquiry::get();
        $data       = compact('inquiry');
        // dd($inquiry);
        return view('admin.contact-inquiry',$data);

    }
    public function destroy($id)
    {
        $remove = Contactinquiry::where('id',$id)->delete();

        return back()->with('deleted','Inquiry has been deleted');;
    }

    // public function removeMultiple(Request $request)
    // {
    //     $check = $request->check; // input type="checkbox" name="check[]"
    //     City::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

    //     return redirect()->back()->with('success', 'Item(s) removed.');
    // }

     public function bulk_delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'checked' => 'required',
        ]);

        if ($validator->fails()) {

            return back()->with('deleted', 'Please select one of them to delete');
        }

        foreach ($request->checked as $checked) {

            $this->destroy($checked);
            
        }

        return back()->with('deleted', 'Inquiry has been deleted');   
    }
}