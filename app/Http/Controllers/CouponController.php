<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Category;
use App\ForumCategory;
use App\Discussion;
use App\Store;
use App\User;
use App\Settings;
use Image;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB; 

class CouponController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		//$store_id = 	$request->input('store_id');
		//$category_id = 	$request->input('category_id');
			
			$category = Category::orderBy('created_at','desc')->get();
			$store = Store::orderBy('created_at','desc')->get();
			$coupon = Coupon::orderBy('created_at','desc')->get();
		//	$coupon = Coupon::where('id','=',$store_id)->orderBy('created_at','desc')->get();
		return view('admin.coupon.index', compact('coupon','store','category'));
	}
	
	public function getcoupons(Request $request)
	{
	    
			$store_id = 		$request->input('store_id');
			$category_id = 		$request->input('category_id');
			$is_expired = 		$request->input('is_expired');
			$is_redeem = 		$request->input('is_redeem');
	//	$store_id = 2;
	//	$category_id = 2;
		
			//$coupon = Coupon::orderBy('created_at','desc')->get();
			$iTotalRecords = Coupon::get()->count();
			
			$start =  $request['start'];
			$length =  $request['length'];
			if($length== -1)
				$length = $iTotalRecords;	
			
			
			if($store_id>0 && $category_id>0)
			{
				
				$coupon  = DB::table('coupons')
							->select('coupons.id','coupons.image','coupons.title','stores.title as store_name','categories.title as category_name','coupons.discount','coupons.is_featured','coupons.is_verified','coupons.is_active')
							->leftjoin('stores','coupons.store_id' ,'stores.id')
							->leftjoin('categories','coupons.category_id' ,'categories.id')
							->where('coupons.store_id','=',$store_id)
							->where('coupons.category_id','=',$category_id)
							
							->orderBy('coupons.created_at','desc')->skip($start)->take($length)->get();
				//$coupon = Coupon::where('id','=',$store_id)->orderBy('created_at','desc')->get();
			}elseif($is_redeem!='' && $store_id>0)
			{
				$date = 	date('Y-m-d');
				//$coupon  = DB::table('coupons')
				//			->select('coupons.id','coupons.image','coupons.title','stores.title as store_name','categories.title as category_name','coupons.discount','coupons.is_featured','coupons.is_verified','coupons.is_active')
				//			->leftjoin('stores','coupons.store_id' ,'stores.id')
				//			->leftjoin('categories','coupons.category_id' ,'categories.id')
				//			->whereDate('coupons.expiry' ,'<' , $date)
				//			->orderBy('coupons.created_at','desc')->get();
				
					$coupon  = DB::table('order_trans')
						->select('coupons.id','coupons.image','coupons.title','stores.title as store_name','categories.title as category_name','coupons.discount','coupons.is_featured','coupons.is_verified','coupons.is_active')
						->leftjoin('coupons','order_trans.coupon_id' ,'coupons.id')
						->leftjoin('stores','order_trans.store_id' ,'stores.id')
						->leftjoin('categories','coupons.category_id' ,'categories.id')
						->where('order_trans.store_id','=',$store_id)
						->where('order_trans.is_redeem' ,'=' ,$is_redeem)
						->skip($start)->take($length)->get();
				
				
				
				
			}elseif($store_id>0)
			{
				$coupon  = DB::table('coupons')
							->select('coupons.id','coupons.image','coupons.title','stores.title as store_name','categories.title as category_name','coupons.discount','coupons.is_featured','coupons.is_verified','coupons.is_active')
							->leftjoin('stores','coupons.store_id' ,'stores.id')
							->leftjoin('categories','coupons.category_id' ,'categories.id')
							->where('coupons.store_id','=',$store_id)
							->orderBy('coupons.created_at','desc')->skip($start)->take($length)->get();
				
				
			}elseif($category_id>0)
			{
				$coupon  = DB::table('coupons')
							->select('coupons.id','coupons.image','coupons.title','stores.title as store_name','categories.title as category_name','coupons.discount','coupons.is_featured','coupons.is_verified','coupons.is_active')
							->leftjoin('stores','coupons.store_id' ,'stores.id')
							->leftjoin('categories','coupons.category_id' ,'categories.id')
							->where('coupons.category_id','=',$category_id)
							->orderBy('coupons.created_at','desc')->skip($start)->take($length)->get();
				
				
			}elseif($is_expired=='is_expired')
			{
				$date = 	date('Y-m-d');
				$coupon  = DB::table('coupons')
							->select('coupons.id','coupons.image','coupons.title','stores.title as store_name','categories.title as category_name','coupons.discount','coupons.is_featured','coupons.is_verified','coupons.is_active')
							->leftjoin('stores','coupons.store_id' ,'stores.id')
							->leftjoin('categories','coupons.category_id' ,'categories.id')
							->whereDate('coupons.expiry' ,'<' , $date)
							->orderBy('coupons.created_at','desc')->skip($start)->take($length)->get();
				
				
			}else
			{
				
				$coupon  = DB::table('coupons')
							->select('coupons.id','coupons.image','coupons.title','stores.title as store_name','categories.title as category_name','coupons.discount','coupons.is_featured','coupons.is_verified','coupons.is_active')
							->leftjoin('stores','coupons.store_id' ,'stores.id')
							->leftjoin('categories','coupons.category_id' ,'categories.id')
							->orderBy('coupons.created_at','desc')->skip($start)->take($length)->get();
				
			}
			
			
			
		

$data = array();
if($start==0)
{
	$i=1;
}else
{
	$i=$start+1;
}
				foreach($coupon as $row)
				{
				 $sub_array = array();
				$is_active = 	$row->is_active;
				
				
			if($is_active==1)
				{
					$active = 'Active';
				}else
				{
					$active = 'InActive';
				}
			
				
			
			
				
			//$category_id = 	$row->category_id;
			//	$store_id = $row->store_id;
				$id = $row->id;
				$checkbox = $i;
                  
				$i++;
				
				 if ($row->image != null)
				 {
					 $image = $row->image;
					$url = url("/images/coupon/$image");
					$img = 	'<img src="'.$url.'" class="img-responsive" width="80" alt="image">';
                 } 
				  else
                  { $img  =  'N/A';  
				  }
				 $id =  $row->id;
				$urlAction = url("admin/coupon/$id/edit");
				$urlDelete = url("admin/coupon/$id");
				
				$action = '<div class="admin-table-action-block">
					<a href="'.$urlAction.'" data-toggle="tooltip" data-original-title="Edit" class="btn-info btn-floating"><i class="material-icons">mode_edit</i></a>
                    <!-- Delete Modal -->
                    <button type="button" class="btn-danger btn-floating" data-toggle="modal" data-target="#'.$row->id .'deleteModal"><i class="material-icons">delete</i> </button>
                    <!-- Modal -->
                    <div id="'.$row->id .'deleteModal" class="delete-modal modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="delete-icon"></div>
                          </div>
                          <div class="modal-body text-center">
                            <h4 class="modal-heading">Are You Sure ?</h4>
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                          </div>
                          <div class="modal-footer">
							<form method="POST" action="'.$urlDelete.'">
							<input name="_method" type="hidden" value="DELETE">
							<input name="_token" type="hidden" value="'.csrf_token().'">
						 <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                           </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>';
				
				
				
				
				 $sub_array[] = $checkbox;
				 $sub_array[] = $img;
				 $sub_array[] = $row->title;
				 $sub_array[] = $row->store_name;
				 $sub_array[] = $row->category_name;
				 $sub_array[] = $row->discount;
				
				 $sub_array[] = "$active";
				 $sub_array[] = "$action";
			
				 $data[] = $sub_array;
				}


		
			
			// json_decode(json_encode($coupon),true);
		$subData = $coupon;
		
		$output = array(
 "draw"       =>  intval($_GET["draw"]),
 "recordsTotal"   =>  $iTotalRecords,
 "recordsFiltered"  =>  $iTotalRecords,
 "data"       =>  $data
);

echo json_encode($output);
		
		
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{	
		$all_users = User::where('is_active','1')->pluck('name','id')->all();
		
		$cat_coupon = ForumCategory::where('is_active','1')->where('category','c')->pluck('title','id')->all();
		$cat_deal = ForumCategory::where('is_active','1')->where('category','d')->pluck('title','id')->all();	
		$all_category = Category::where('is_active','1')->pluck('title','id')->all();
        
		$locations = \App\Category::all();

		$data_gst = $locations->pluck('gst')->toArray();

		$optionAttributes = [];
		    foreach ($locations as $location) {
		        $optionAttributes[$location->id] = [
		            'data-gst' => $location->gst,
		            
		        ];
		    }

		// $all_category = Category::where('is_active','1')->pluck('gst','id')		
		$all_store = Store::where('is_active','1')->pluck('title','id')->all();
		return view('admin.coupon.create', compact('all_users','optionAttributes','all_category','data_gst','all_store','cat_coupon','cat_deal'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    
	
		$request->validate([
			'title' => 'required|min:3',
			'detail' => 'required|min:3',
			'price' => 'nullable|numeric',
			'discount' => 'nullable|numeric',
			
			'image' => 'nullable|image|mimes:jpg,png,gif,jpeg',
		]);

	
		if (isset($input['type']))
    {
      $request->validate([
				'code' => 'required'
			]);
    }
	
		$input = $request->all();

		$input['type']='c';
		$input['forum_category_id '] = 1;
	
	

		if (!isset($input['type']))
    {
      $input['type'] = 'd';
    }
    else{
    	$input['type'] = 'c';
    }

		if ($file = $request->file('image')) {
			
			$optimizeImage = Image::make($file);
      $optimizePath = public_path().'/images/coupon/';
      $name = time().$file->getClientOriginalName();
      $optimizeImage->save($optimizePath.$name, 72);

			$input['image'] = $name;

		}

		if (!isset($input['is_featured']))
    {
      $input['is_featured'] = 0;
    }
    if (!isset($input['is_exclusive']))
    {
      $input['is_exclusive'] = 0;
    }
    if (!isset($input['is_verified']))
    {
      $input['is_verified'] = 0;
    }
    if (!isset($input['is_front']))
    {
      $input['is_front'] = 0;
    }
    if (!isset($input['is_active']))
    {
      $input['is_active'] = 0;
    }
	//print_r($input);
	
		$coupon = Coupon::create($input);
    
		$coupon->slug = str_slug($input['title'],'-');
        
		$uni_col = collect();
		$uni_col->push(Coupon::pluck('uni_id'));
		$uni_col->push(Discussion::pluck('uni_id'));
		$uni_col = array($uni_col->flatten());
	
		do {
		  $random = str_random(5);
		} while (in_array($random, $uni_col));
		$gst_price = $request->gst_price;
    		$coupon->gst_price = $gst_price;
	
		$coupon->uni_id = $random;
        $store_data = Store::findOrFail($input['store_id']);
        $sendmail = $store_data->email;
        $setting = Settings::findOrFail(1);
        $w_email = $setting->w_email;
        
        $all_users = User::where('is_active','1')->get();
        
		if($coupon->save()){
                 
                 $subject = "Add New Coupon Ramasouq";
                  $data = array('w_email' => $w_email, 'email' => $sendmail, 'name' => "Ramasouq", 'subject' => $subject);

                  Mail::send('email.mail', $data, function($message) use ($data) {
                  
                     $message->to($data['email'], $data['name'])->subject($data['subject']);
                     $message->from($data['w_email'], 'Ramasouq');
                  });
                  foreach($all_users as $u){
                     
 
                   $fcm_id = $u->fcm_id;
                   $title = "Greeting Notification";
                   $message = "Have good day!";
                   $id = $u->id;
                   $type = "basic";
                 
                   $res = send_notification_FCM($fcm_id, $title, $message, $id,$type);
                   
                   
                   if($res == 1){
 
                         // success code
                        //  dd('success');
                     
                       }else{
                     
                         // fail code
                        //  dd('failed');
                       }
                  }
               
         }
	
		return back()->with('added', 'Coupon has been added');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Coupon  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Coupon  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{		
		$coupon = Coupon::findOrFail($id);		
		$all_users = User::where('is_active','1')->pluck('name','id')->all();
		$cat_coupon = ForumCategory::where('is_active','1')->where('category','c')->pluck('title','id')->all();	
		$cat_deal = ForumCategory::where('is_active','1')->where('category','d')->pluck('title','id')->all();	
		$all_category = Category::where('is_active','1')->pluck('title','id')->all();	
		// $data_gst = Category::where('is_active','1')->pluck('gst')->all();	
		$all_store = Store::where('is_active','1')->pluck('title','id')->all();
		$locations = \App\Category::all();

		$data_gst = $locations->pluck('gst')->toArray();

		$optionAttributes = [];
		    foreach ($locations as $location) {
		        $optionAttributes[$location->id] = [
		            'data-gst' => $location->gst,
		            
		        ];
		    }
		return view('admin.coupon.edit', compact('coupon','all_users','optionAttributes','data_gst','all_category','all_store','cat_coupon','cat_deal'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Coupon  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate([
			'title' => 'required|min:3',
			'detail' => 'required|min:3',
			'price' => 'nullable|numeric',
			'discount' => 'nullable|numeric',
			
			'image' => 'nullable|image|mimes:jpg,png,gif,jpeg',
		]);
//'link' => 'nullable|regex:#^https?://#',
		$coupon = Coupon::findOrFail($id);

		$input = $request->all();
		$input['type'] = 'c';
		$input['forum_category_id '] = 1;
		$input['user_id '] = 2;
		if (isset($input['type']))
    {
      $request->validate([
				'code' => 'required'
			]);
    }

		if (!isset($input['type']))
    {
      $input['type'] = 'd';
    }
    else{
    	$input['type'] = 'c';
    }

    if($coupon->type == 'c' && $input['type'] == 'd')
    {
      $input['code'] = null;
    }

		if ($file = $request->file('image')) {
			
			if ($coupon->image != null) {
				
				$image_file = @file_get_contents(public_path().'/images/coupon/'.$coupon->image);

				if($image_file){		
					unlink(public_path().'/images/coupon/'.$coupon->image);
				}

			}

			$optimizeImage = Image::make($file);
      $optimizePath = public_path().'/images/coupon/';
      $name = time().$file->getClientOriginalName();
      $optimizeImage->save($optimizePath.$name, 72);

			$input['image'] = $name;

		}
		
		if (!isset($input['is_featured']))
    {
      $input['is_featured'] = '0';
    }
    else{
      $input['is_featured'] = '1';
    }
    if (!isset($input['is_exclusive']))
    {
      $input['is_exclusive'] = '0';
    }
    else{
      $input['is_exclusive'] = '1';
    }
    if (!isset($input['is_verified']))
    {
      $input['is_verified'] = '0';
    }
    else{
      $input['is_verified'] = '1';
    }
    if (!isset($input['is_active']))
    {
      $input['is_active'] = '0';
    }
    else{
      $input['is_active'] = '1';
    }
    
		$coupon->update($input);
		$coupon->slug = str_slug($input['title'],'-');
		$gst_price = $request->gst_price;
    	$coupon->gst_price = $gst_price;
        $store_data = Store::findOrFail($input['store_id']);
        $sendmail = $store_data->email;
        $setting = Settings::findOrFail(1);
        $w_email = $setting->w_email;
        
		if($coupon->save()){
                 
                 $subject = "Edit Coupon Ramasouq";
                  $data = array('w_email' => $w_email, 'email' => $sendmail, 'name' => "Ramasouq", 'subject' => $subject);

                  Mail::send('email.mail', $data, function($message) use ($data) {
                  
                     $message->to($data['email'], $data['name'])->subject($data['subject']);
                     $message->from($data['w_email'], 'Ramasouq');
                  });
               
         }
        
		return redirect('admin/coupon')->with('updated', 'Coupon has been updated');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Coupon  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$coupon = Coupon::findOrFail($id);

		$coupon->comments()->delete();

    $coupon->likes()->delete();

		if ($coupon->image != null) {

			$image_file = @file_get_contents(public_path().'/images/coupon/'.$coupon->image);

				if($image_file){		
					unlink(public_path().'/images/coupon/'.$coupon->image);
				}
		}

		$coupon->delete();

		return back()->with('deleted', 'Coupon has been deleted');
	}

	public function bulk_delete(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->destroy($checked);
			
		}

		return back()->with('deleted', 'Coupon has been deleted');   
	}

 public function dropdown(Request $request) 
  {
    $state = $request['state'];
    if($state == 'true'){
    	$drop = ForumCategory::where('is_active','1')->where('category','c')->pluck('title','id')->all();
    }
    else{
    	$drop = ForumCategory::where('is_active','1')->where('category','d')->pluck('title','id')->all();
    }
    return response()->json($drop);
  }

}
