<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use Intervention\Image\ImageManagerStatic as Image;

class SettingsController extends Controller
{
    public function general_show()
    {
      $general_settings = Settings::first();
      return view('admin.general', compact('general_settings'));
    }

    /**
     * Update General Settings.
     *
     */
    public function general_update(Request $request, $id)
    {
        
        $general_settings = Settings::findOrFail($id);

        $request->validate([
          'logo' => 'nullable|image|mimes:jpg,jpeg,png',
          'w_email' => 'nullable|email',
          'copyright' => 'required'
        ]);

        $input = $request->all();

        
        if($request->hasFile('logo'))  
            { 
                $image        = $request->file('logo');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                // $image_resize->resize(150, 175);
                $image_resize->save(public_path('images/' .$filename));
                $input['logo']   = $filename;
            }

        
        if($request->hasFile('navbar_img'))  
            { 
                $image        = $request->file('navbar_img');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                // $image_resize->resize(150, 175);
                $image_resize->save(public_path('images/' .$filename));
                $input['navbar_img']   = $filename;
            }

        if ($file = $request->file('footer_logo')) {
          $name = $file->getClientOriginalName();
          if($general_settings->footer_logo != null) {
            $image_file = @file_get_contents(public_path().'/images/'.$general_settings->footer_logo);
            if($image_file){
              unlink(public_path() . '/images/' . $general_settings->footer_logo);
            }
          }
          $file->move('images', $name);
          $input['footer_logo'] = $name;
        }

        if($request->hasFile('preloader'))  
            { 
                $image        = $request->file('preloader');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                // $image_resize->resize(150, 175);
                $image_resize->save(public_path('images/' .$filename));
                $input['preloader']   = $filename;
            }

        
        
        if($request->hasFile('favicon'))  
            { 
                $image        = $request->file('favicon');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                $image_resize->resize(40, 40);
                $image_resize->save(public_path('images/favicon/' .$filename));
                $input['favicon']   = $filename;
            }
        
        if($request->hasFile('banner_img'))  
            { 
                $image        = $request->file('banner_img');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                // $image_resize->resize(1910, 132);
                $image_resize->save(public_path('images/favicon/' .$filename));
                $input['banner_img']   = $filename;
            }
        
        

        if (!isset($input['is_recent_deals']))
        {
          $input['is_recent_deals'] = 0;
        }
        if (!isset($input['is_store_slider']))
        {
          $input['is_store_slider'] = 0;
        }
        if (!isset($input['is_mailchimp']))
        {
          $input['is_mailchimp'] = 0;
        }
        if (!isset($input['is_app_icon']))
        {
          $input['is_app_icon'] = 0;
        }
        if (!isset($input['is_playstore']))
        {
          $input['is_playstore'] = 0;
        }
        if (!isset($input['is_category_block']))
        {
          $input['is_category_block'] = 0;
        }
        if (!isset($input['is_feat_slider']))
        {
          $input['is_feat_slider'] = 0;
        }
        if (!isset($input['footer_layout']))
        {
          $input['footer_layout'] = 0;
        } 
        if (!isset($input['is_blog']))
        {
          $input['is_blog'] = 0;
        } 
        if (!isset($input['blog_layout']))
        {
          $input['blog_layout'] = 0;
        } 
        if (!isset($input['blog_left']))
        {
          $input['blog_left'] = 0;
        }
        if (!isset($input['is_gotop']))
        {
          $input['is_gotop'] = 0;
        }
        if (!isset($input['is_preloader']))
        {
          $input['is_preloader'] = 0;
        }
        if (!isset($input['right_click']))
        {
          $input['right_click'] = 0;
        }
        if (!isset($input['inspect']))
        {
          $input['inspect'] = 0;
        }
 
        $general_settings->update($input);

        return back()->with('added','Settings has been saved successfully');
    }
}
