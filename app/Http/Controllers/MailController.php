<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\FeedbackMail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller {
   // public function basic_email() {
   //       $comment = 'Hi, This test feedback.';
   //       $toEmail = "sharwan.suncity@gmail.com";
   //       Mail::to($toEmail)->send(new FeedbackMail($comment));
   //    }
   public function html_email() {
      // echo env('MAIL_DRIVER');
      $subject = "Contact Enquiry from CBG Industries";
      $data = array('name' => "Suncity Techno", 'subject' => $subject);
      Mail::send('email.mail', $data, function($message) use ($data) {
      
         $message->to('sharwan.suncity@gmail.com', $data['name'])->subject($data['subject']);
         $message->from('ramasouq@ramasouq.com', 'Ramasouq');
      });
      echo "HTML Email Sent. Check your inbox.";
   }
   // public function attachment_email() {
   //    $data = array('name' => "Virat Gandhi");
   //    Mail::send('mail', $data, function($message) {
   //       $message->to('rams50288@gmail.com', 'Tutorials Point')->subject
   //          ('Laravel Testing Mail with Attachment');
   //       $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
   //       $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
   //       $message->from('sharwan.suncity@gmail.com','Virat Gandhi');
   //    });
   //    echo "Email Sent with attachment. Check your inbox.";
   // }
}