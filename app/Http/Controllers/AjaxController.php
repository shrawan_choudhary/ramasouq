<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\FeedbackMail;
use App\Inquery;
use App\Settings;
use App\Contactinquiry;

class AjaxController extends BaseController {
    
    public function index(Request $request) {
        
        
        
         $data = new Inquery();
         $data->company_name = $request->c_name;
         $data->states = $request->states;
         $data->city = $request->city;
         $data->business_category = $request->category;
         // $data->company_name = $request->c_name;
         $data->name = $request->name;
         $data->mobile_no = $request->mobile_no;
         $data->email = $request->email;
         $data->hear = $request->hear;
	     $data->page_name = $request->page_name;
         $data->term_condition = $request->term_condition;
         $sendmail = $data->email;
         
         $setting = Settings::findOrFail(1);
         $from = $setting->w_email;
         
         
         
         if($data->save()){
                 $sendmail = $data->email;
                 $subject = "Enquiry from Ramasouq";
                  $data = array('from' => $from, 'email' => $sendmail, 'name' => "Suncity Techno", 'subject' => $subject);

                  Mail::send('email.mail', $data, function($message) use ($data) {
                  
                     $message->to($data['email'], $data['name'])->subject($data['subject']);
                     $message->from($data['from'], 'Ramasouq');
                  });
               return response()->json(['status'=>1]);
         }
         else{
                return response()->json(['status'=>0]);
         }


    }

    

	public function contact_inquiry(Request $request) {
        
       
        
         $data = new Contactinquiry();
         $data->name      = $request->name;
         $data->email     = $request->email;
         $data->mobile_no = $request->mobile_no;
         $data->subject   = $request->subject;
         $data->i_category   = $request->i_category;
         $data->website   = $request->website;
         $data->message   = $request->message;
         $contact_mail = $data->email;
         
         $setting = Settings::findOrFail(1);
         $from = $setting->w_email;
         
         
         if($data->save()){

            $contact_mail = $data->email;
                 $subject = "Contact Enquiry from Ramasouq";
                  $data = array('from' => $from, 'email' => $contact_mail, 'name' => "Suncity Techno", 'subject' => $subject);
                  
                  Mail::send('email.contact-inquiry', $data, function($message) use ($data) {
                  
                     $message->to($data['email'], $data['name'])->subject($data['subject']);
                     $message->from($data['from'], 'Ramasouq');
                  });
               return response()->json(['status'=>1]);
         }
         else{
                return response()->json(['status'=>0]);
         }


    }
}