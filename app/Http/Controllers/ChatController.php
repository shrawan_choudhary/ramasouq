<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request; 
use App\Http\Controllers; 
use App\Chat; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB; 
use Validator;



class ChatController extends Controller
{
    //
	public $successStatus = 200;
	
	
		public function index()
	{        
		//$chat = Chat::all();
		
		
			$chat  = DB::table('chat')
						->select('chat.*','users.name as user_name')
						->leftjoin('users','chat.user_id' ,'users.id')
						->orderBy('chat.created_at', 'DESC')
						->groupBy('chat.user_id')
				->get();

		
		
		return view('admin.chat.index', compact('chat'));
	}

		public function getchat(Request $request)
	{        
		
			$chat  = DB::table('chat')
						->select('chat.*')
						->where('user_id','=',$_GET['user_id'])
				->get();

		
		
		$data = 	json_decode(json_encode($chat),true);
		if(is_array($data))
		{
			?>
				
		<?php 	foreach($data as $key=>$val)
			{ ?>
				<!--<div class='row'>
					<div style='padding-left:20px'> <?php echo $val['msg_send_by'] ?>  : <?php echo $val['message'] ?></div>
				</div>
				-->
			<?php 
			if($val['msg_send_by']=='user')
			{
			?>	
			<div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p><?php echo $val['message'] ?></p>
                  <span class="time_date"> 
				  <?php
						$date_create = date_create($val['created_at']);
				  echo date_format($date_create,"H:i A"); ?>  |  <?php echo  date_format($date_create,"M d"); ?> </span></div>
              </div>
            </div>
			<?php }else{ ?>	
				
			 <div class="outgoing_msg">
              <div class="sent_msg">
                <p><?php echo $val['message'] ?></p>
                   <span class="time_date"> 
				  <?php
						$date_create = date_create($val['created_at']);
				  echo date_format($date_create,"H:i A"); ?>  |  <?php echo  date_format($date_create,"M d"); ?> </span></div>
         
			 </div>
            </div>	
				
				
			<?php } }
		}
		
	}



	
// code start from here 



		public function makechat(Request $request)
	{
		$input = $request->all();
		$data = Chat::create($input);
			
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  \App\chat  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\chat  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
	   $chat = Chat::findOrFail($id);
		return view('admin.chat.edit', compact('chat'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\chat  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate([
			'title' => 'required|min:3|unique:categories,title,'.$id,
			'image' => 'nullable|image|mimes:jpg,png,gif,jpeg'
		]);
		
		if(!$request->hasFile('image')){
			return back()->with('deleted', 'Please add chat image');
		}
		
		$chat = Chat::findOrFail($id);

		$input = $request->all();

		if (isset($input['is_active']) && $input['is_active'] == 'on')
    {
      $input['is_active'] = 1;
    }
    else{
    	$input['is_active'] = 0;
    }

		if($file = $request->file('image')) {
			
			if ($chat->image != null) {

				$image_file = @file_get_contents(public_path().'/images/chat/'.$chat->image);

				if($image_file){

					unlink(public_path().'/images/chat/'.$chat->image);
				}

			}

			$optimizeImage = Image::make($file);
      $optimizePath = public_path().'/images/chat/';
      $name = time().$file->getClientOriginalName();
      $optimizeImage->save($optimizePath.$name, 72);

			$input['image'] = $name;

		}

		$chat->update($input);

		$slug = str_slug($input['title'],'-');

    $chat->slug = $slug;

    $chat->save();

		if($chat->is_active == 0){
			foreach($chat->coupon as $coupon){
				$coupon->update([
					'is_active' => 0
				]);
			}
		}
		else{
			foreach($chat->coupon as $coupon){
				$coupon->update([
					'is_active' => 1
				]);
			}
		}
		return redirect('admin/chat')->with('updated', 'chat has been updated');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\chat  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$chat = Chat::findOrFail($id);

		if(isset($chat->coupon)){
			foreach($chat->coupon as $coupon){
				$coupon->comments()->delete();	
				$coupon->likes()->delete();
			}
		}
		if(isset($chat->discussion)){
			foreach($chat->discussion as $discussion){
				$discussion->comments()->delete();	
				$discussion->likes()->delete();
			}
		}

		if ($chat->image != null) {
				
			$image_file = @file_get_contents(public_path().'/images/chat/'.$chat->image);

				if($image_file){
					
					unlink(public_path().'/images/chat/'.$chat->image);
				}

		}

		$chat->delete();

		return back()->with('deleted', 'chat has been deleted');
	}

	public function bulk_delete(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->destroy($checked);
			
		}

		return back()->with('deleted', 'chat has been deleted');   
	}
}

