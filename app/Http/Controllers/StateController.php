<?php
namespace App\Http\Controllers;

use App\State;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
// use App\City;

class StateController extends Controller
{
   
    // public function index(request $request){
    //     //
    //     $query = Category::withCount('')->latest();

    //     if( !empty( $request->category ) ) {
    //         $query->where('category', 'LIKE', '%'.$request->category.'%');
    //     }
    //     $lists1 = $query->paginate(20);

    //     $data = compact( 'lists1' ); // Variable to array convert
    //     return view('backend.inc.category.index', $data);
    // }   
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
        $query = State::withCount('')->latest();

        if( !empty( $request->name ) ) {
            $query->where('name', 'LIKE', '%'.$request->name.'%');
        }
        $lists1 = $query->paginate(20);

        return view('admin.state.index', compact('lists1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function add()
    // {
    //     $m_category = Category::where('parent', null)->get();
    //     $parentArr = [null => 'ROOT'];
    //         if (!$m_category->isEmpty()) {
    //         foreach ($m_category as $mcat) {
    //         $parentArr[$mcat->id] = $mcat->category;
    //         }
    //     }
    //     $data = compact('parentArr');
    //     return view('backend.inc.category.add', $data);
    // }

    public function create()
    {

        $m_category = State::where('parent', null)->get();
        $parentArr = [null => 'ROOT'];
            if (!$m_category->isEmpty()) {
            foreach ($m_category as $mcat) {
            $parentArr[$mcat->id] = $mcat->name;
            }
        }
        return view('admin.state.create', compact('parentArr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    

    public function store(Request $request)
    {
        //
        $rules = [
            'name'        => 'required'           
        ];
        $request->validate( $rules );
        $obj = new State;
        $obj->name       = $request->name;       
        $obj->slug  = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug); 
        $obj->parent         = $request->parent; 

        
        $obj->save();

        return back()->with('added','State has been added');;
    }   
   
    // public function edit(Request $request,$id)
    // {
    //     //
    //     $edit = Category::findOrFail( $id );
    //     $request->replace($edit->toArray());
    //     $request->flash();

    //     $categories = Category::get();
    //     $parentArr = [
    //         ''  => 'Select Category'
    //     ];

    //     foreach($categories as $c) {
    //         $parentArr[ $c->id ] = $c->category;
    //     }
    //     $data = compact('parentArr','edit');
    //     return view('backend.inc.category.edit', $data);
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function edit(Request $request,$id)
    {
        $edit = State::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        $states = State::get();
        $parentArr = [
            ''  => 'Select State'
        ];

        foreach($states as $c) {
            $parentArr[ $c->id ] = $c->name;
        }
        $data = compact('parentArr','edit');

        return view('admin.state.edit',compact('edit','parentArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // }
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'name'        => 'required'
            
        ];
        
        $request->validate( $rules );
        $obj = State::findOrFail( $id ); 
        $obj->name       = $request->name;       
        $obj->slug       = $request->slug;       
        $obj->slug  = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug); 
        $obj->parent         = $request->parent;

        $obj->save();
        
        return redirect('admin/state')->with('updated','State has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function destroy($id)
    {
        $remove = State::where('id',$id)->delete();

        return back()->with('deleted','State has been deleted');;
    }

    
     public function bulk_delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'checked' => 'required',
        ]);

        if ($validator->fails()) {

            return back()->with('deleted', 'Please select one of them to delete');
        }

        foreach ($request->checked as $checked) {

            $this->destroy($checked);
            
        }

        return back()->with('deleted', 'State has been deleted');   
    }

   
}
