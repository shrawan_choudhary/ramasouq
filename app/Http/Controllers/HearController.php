<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Hear;

class HearController extends Controller
{
   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hear = Hear::get();
        return view('admin.hear-record.index', compact('hear'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    

   
    // public function add()
    // {
    //     //

    //     $states = State::get();
    //     $parentArr = [
    //         ''  => 'Select State'
    //     ];

    //     foreach($states as $c) {

    //         $parentArr[ $c->id ] = $c->state;
    //     }
    //     $data = compact('parentArr');
    //     return view('backend.inc.city.add',$data);
    // }
    public function create()
    {

    	
        return view('admin.hear-record.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    
    // public function addData(Request $request)
    // {
    //     //
    //     $rules = [
    //         'title'             => 'required',            
    //             ];
    //         // $file = $request->file('image','image_hover');
        
           
    //     $request->validate( $rules );
    //     $obj = new City;
    //     $obj->title             = $request->title;
    //     $obj->slug              = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
    //     $obj->parent            = $request->parent;
        
        
    //     // $obj->image  = $request->$file->getClientOriginalName();
        
    //     $obj->save();

    //     return redirect( url('admin-control/city/') )->with('success', 'Success! New record has been added.');
    // }
    public function store(Request $request)
    {
        //
        $rules = [
            'name'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
        
           
        $request->validate( $rules );
        $obj = new Hear;
        $obj->name             = $request->name;
        $obj->short_name       = $request->short_name;
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return back()->with('added','Hear has been added');;
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function edit(Request $request,$id)
    {
        //
        $edit = Hear::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        
        $lists1 = Hear::latest()->paginate(20);
        //
        

        

        return view('admin.hear-record.edit',compact('lists1','edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    
    
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'name'          => 'required',
            
            
        ];
        $request->validate( $rules );
        

        $obj = Hear::findOrFail( $id );
        $obj->name           = $request->name;
        $obj->short_name     = $request->short_name;
        
        $obj->save();
        return redirect('admin/hear-record')->with('updated','Hear has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     

    public function destroy($id)
    {
        $remove = Hear::where('id',$id)->delete();

        return back()->with('deleted','Hear has been deleted');;
    }

    // public function removeMultiple(Request $request)
    // {
    //     $check = $request->check; // input type="checkbox" name="check[]"
    //     City::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

    //     return redirect()->back()->with('success', 'Item(s) removed.');
    // }

     public function bulk_delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'checked' => 'required',
        ]);

        if ($validator->fails()) {

            return back()->with('deleted', 'Please select one of them to delete');
        }

        foreach ($request->checked as $checked) {

            $this->destroy($checked);
            
        }

        return back()->with('deleted', 'Hear has been deleted');   
    }

   
}
