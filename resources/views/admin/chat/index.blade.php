@extends('layouts.admin')
@section('content')
<style>
.bulk_delete{
    display: block !important; /* I added this to see the modal, you don't need this */
}

/* Important part */
.bulk_delete-dialog{
    overflow-y: initial !important
}
.bulk_delete-body{
    height: 500px;
    overflow-y: auto;
}

</style>
  <div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
   
      </div>
    </div>
    <div class="content-block box-body">
	
	

<html>
<head>

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="{{asset('css/chat.css')}}" type="text/css" rel="stylesheet">

</head>
<body>
<div class="container">
<h3 class=" text-center"></h3>
<div class="messaging">
      <div class="inbox_msg">
        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>Recent</h4>
            </div>
            <div class="srch_bar">
              <div class="stylish-input-group">
                <input type="text" class="search-bar"  placeholder="Search" >
                <span class="input-group-addon">
                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                </span> </div>
            </div>
          </div>
          <div class="inbox_chat">
		  @if (isset($chat))
			   @foreach ($chat as $key => $item)
            <div class="chat_list">
              <div class="chat_people">
                <div class="chat_img"> <img src="{{asset('images/user-profile.png')}}" alt=""> </div>
                <div class="chat_ib">
                  <h5>{{$item->user_name}} <span class="chat_date" onclick='getuserid("{{$item->user_id}}")'>Open Chat</span></h5>
                  <p></p>
                </div>
              </div>
            </div>
			 @endforeach
		  @endif
           
           
          
          </div>
        </div>
        <div class="mesgs" id='myDIV'>
          <div class="msg_history" id='message_Area'>
          <!--  <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>Test which is a new approach to have all aa
                    solutions</p>
                  <span class="time_date"> 11:01 AM    |    June 9</span></div>
              </div>
            </div>
            <div class="outgoing_msg">
              <div class="sent_msg">
                <p>Test which is a new approach to have all
                  solutions</p>
                <span class="time_date"> 11:01 AM    |    June 9</span> 
			 </div>
            </div>
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>Test, which is a new approach to have</p>
                  <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
              </div>
            </div>
            <div class="outgoing_msg">
              <div class="sent_msg">
                <p>Apollo University, Delhi, India Test</p>
                <span class="time_date"> 11:01 AM    |    Today</span> </div>
            </div>
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>We work directly with our designers and suppliers,
                    and sell direct to you, which means quality, exclusive
                    products, at a price anyone can afford.</p>
                  <span class="time_date"> 11:01 AM    |    Today</span></div>
              </div>
            </div>
			-->
          </div>
          <div class="type_msg">
            <div class="input_msg_write">
              
			  <form onSubmit='return sendSMS()'>
				<input  type='hidden' name='user_id' id='user_id' value=''>
				<input  type='hidden' name='msg_send_by' id='msg_send_by' value='admin'>
				<input class='form-control' name='message' id='message'  placeholder='Type Message...'>				  
                <button type="submit" class="btn btn-danger">Send</button>
			</form>	
			  <!--<input type="text" class="write_msg" placeholder="Type a message" />
              <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
				-->
			</div>
          </div>
        </div>
      </div>
      
      
      
    </div></div>
    </body>
    </html>
    </html>
	
	<!--chat-->
	
	
	
      
    </div>
  </div>



 <script type = "text/javascript" 
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
		
      <script type = "text/javascript" language = "javascript">
	  
	   function getuserid(id)
			  {
				 $("#message").empty();
				 $("#message_Area").empty();
				  $("#user_id").val(id);
               $.get( 
                  "chat/getchat",
                  { user_id: id },
                  function(data) {
                     $('#message_Area').html(data);
                  }
               );
			   
			//  var x = document.getElementById("myDIV");
			//	  if (x.style.display === "none") {
			//		x.style.display = "block";
			//	  } else {
			//		x.style.display = "none";
			//	  }
			   
				//$('#bulk_delete').modal('show');
			  }	
	
	//function sendSMS()
	//		  {
	//		var user_id  = $("#user_id").val();
	//		var message  = $("#message").val();
    //           $.get( 
    //              "chat/getchat",
    //              { message: message ,user_id : user_id },
    //              function(data) {
    //                 $('#message').val("");
    //              }
    //           );
	//		  // $('#bulk_delete').modal('show');
	//		  }	
	//  
     

 $(function () {
        $('form').on('submit', function (e) {

          e.preventDefault();

          $.ajax({
            type: 'post',
            url: 'chat/makechat',
            data: $('form').serialize(),
            success: function () {
				$('#message').val('');
			var	id =  $('#user_id').val();
				getuserid(id);
				$('#message').val("");
            //  alert('form was submitted');
			
            }
          });

        });

      });



function myFunction() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
	 
</script>
@endsection
