@extends('layouts.admin')
@section('content')
  <div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a  data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Send Email</h4> 
    {!! Form::open(['method' => 'POST', 'action' => 'NotificationController@sendmail', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-12">
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              {!! Form::label('email', 'Select Email*') !!} - <p class="inline info">Please select email</p>
              <select name="email[]" class="form-control select2" multiple>
                <option>Select Email</option>
                @foreach($users as $u)
                <option>{{ $u->email }}</option>
                @endforeach
              </select>
              <small class="text-danger">{{ $errors->first('email') }}</small>
          </div>
          <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
              {!! Form::label('subject', 'Subject*') !!} - <p class="inline info">Please Enter Subject</p>
              {!! Form::text('subject', null, ['class' => 'form-control', 'required']) !!}
              <small class="text-danger">{{ $errors->first('subject') }}</small>
          </div>  
                         
          <div class="summernote-main form-group{{ $errors->has('message') ? ' has-error' : '' }}">
            {!! Form::label('message', 'Message*') !!} - <p class="inline info">Please Enter Message</p>
            {!! Form::textarea('message', null, ['id' => 'summernote-main','class' => 'form-control' ,'required']) !!}
            <small class="text-danger">{{ $errors->first('message') }}</small>
          </div>                                          
          
            
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Create</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@endsection