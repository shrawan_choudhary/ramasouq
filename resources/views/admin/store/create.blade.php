@extends('layouts.admin')
@section('content')
  <div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/store')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Add Store</h4> 
    {!! Form::open(['method' => 'POST', 'action' => 'StoreController@store', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
              {!! Form::label('title', 'Store Name / Title') !!} 
              {!! Form::text('title', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('title') }}</small>
          </div>  
		  
          <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
              {!! Form::label('category_id', 'Select Category*') !!} - <p class="inline info">Please select category</p>
              {!! Form::select('category_id', $all_category, null, ['class' => 'form-control select2', 'required']) !!}
              <small class="text-danger">{{ $errors->first('category_id') }}</small>
          </div> 
		  
		  
		   <div class="form-group{{ $errors->has('opening_time') ? ' has-error' : '' }}">
              {!! Form::label('opening_time', 'Opening Time') !!} 
              {!! Form::text('opening_time', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('opening_time') }}</small>
          </div> 
		  <div class="form-group{{ $errors->has('closing_time') ? ' has-error' : '' }}">
              {!! Form::label('closing_time', 'Closing Time') !!} 
              {!! Form::text('closing_time', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('closing_time') }}</small>
          </div> 
		  
          <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} input-file-block">
            {!! Form::label('image', 'Store Logo / Image') !!}
            {!! Form::file('image', ['class' => 'input-file', 'id'=>'image']) !!}
            <label for="image" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="Store Image">
              <i class="icon fa fa-check"></i>
              <span class="js-fileName">Choose a File</span>
            </label>
            <p class="info">Choose custom image</p>
            <small class="text-danger">{{ $errors->first('image') }}</small>
          </div> 
           <div class="form-group{{ $errors->has('is_featured') ? ' has-error' : '' }} switch-main-block">
            <div class="row">
              <div class="col-xs-4">
                {!! Form::label('is_featured', 'Featured') !!}
              </div>
              <div class="col-xs-5 pad-0">
                <label class="switch">                
                  {!! Form::checkbox('is_featured', 1, 1, ['class' => 'checkbox-switch']) !!}
                  <span class="slider round"></span>
                </label>
              </div>
            </div>
            <div class="col-xs-12">
              <small class="text-danger">{{ $errors->first('is_featured') }}</small>
            </div>
          </div> 
          <div class="form-group{{ $errors->has('is_active') ? ' has-error' : '' }} switch-main-block">
            <div class="row">
              <div class="col-xs-4">
                {!! Form::label('is_active', 'Status') !!}
              </div>
              <div class="col-xs-5 pad-0">
                <label class="switch">                
                  {!! Form::checkbox('is_active', 1, 1, ['class' => 'checkbox-switch']) !!}
                  <span class="slider round"></span>
                </label>
              </div>
            </div>
            <div class="col-xs-12">
              <small class="text-danger">{{ $errors->first('is_active') }}</small>
            </div>
          </div>         
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Create</button>
          </div>
          <div class="clear-both"></div>
        </div>  
		
		 <div class="col-md-6">
			
		 
		  <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
              {!! Form::label('mobile', 'Mobile No') !!} 
              {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('mobile') }}</small>
          </div> 
		 
		  <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
              {!! Form::label('address', 'Address') !!} 
              {!! Form::text('address', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('address') }}</small>
          </div> 
		   
		  
		 
		  <div class="form-group{{ $errors->has('merchant_name') ? ' has-error' : '' }}">
              {!! Form::label('merchant_name', 'Merchant Name') !!} 
              {!! Form::text('merchant_name', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('merchant_name') }}</small>
          </div> 
		
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              {!! Form::label('email', 'Email') !!} 
              {!! Form::text('email', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('email') }}</small>
          </div> 
		 
		 <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              {!! Form::label('password', 'Password') !!} 
              {!! Form::password('password', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('password') }}</small>
          </div> 
		 
		 
		 
		 </div>
		
		
		
		
      </div>
    {!! Form::close() !!}
  </div>
@endsection