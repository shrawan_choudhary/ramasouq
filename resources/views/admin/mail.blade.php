@extends('layouts.admin')
@section('content')
  <div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <a  class="btn btn-danger btn-md">Send Notification</a>
      <!-- Delete Modal -->
      <a type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#send_mail"><i class="material-icons left">send</i> Send</a>   
      <!-- Modal -->
      <div id="send_mail" class="delete-modal modal fade" role="dialog">
        <div class="modal-dialog modal-md">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            
            <div class="modal-footer">
              {!! Form::open(['method' => 'POST', 'action' => 'NotificationController@mailsend', 'id' => 'bulk_delete_form']) !!}
              <div class="row admin-form-block z-depth-1">
                <div class="col-md-12">
                  
                  <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                      {!! Form::label('subject', 'Subject*') !!} - <p class="inline info">Please Enter Subject</p>
                      {!! Form::text('subject', null, ['class' => 'form-control', 'required']) !!}
                      <small class="text-danger">{{ $errors->first('subject') }}</small>
                  </div>  
                                 
                  <div class="summernote-main form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                    {!! Form::label('message', 'Message*') !!} - <p class="inline info">Please Enter Message</p>
                    {!! Form::textarea('message', null, ['id' => 'summernote-main','class' => 'form-control' ,'required']) !!}
                    <small class="text-danger">{{ $errors->first('message') }}</small>
                  </div> 
                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="content-block box-body">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
          <tr class="table-heading-row">
            <th>
              <div class="inline">
                <input id="checkboxAll" type="checkbox" class="filled-in" name="email[]" value="all" id="checkboxAll">
                <label for="checkboxAll" class="material-checkbox"></label>
              </div>Sn.</th>
           
            <th>Name</th>
            <th>Email</th>
          </tr>
        </thead>
        @if (isset($users))
          <tbody>
            @foreach($users as $key => $item)
              <tr>
                <td>
                  <div class="inline">
                    <input type="checkbox" form="bulk_delete_form" class="filled-in material-checkbox-input" name="email[]" value="{{$item->email}}" id="checkbox{{$item->id}}">
                    <label for="checkbox{{$item->id}}" class="material-checkbox"></label>
                  </div>
                  {{$key+1}}
                </td>
                
                <td>@if($item->name != '')
                  {{$item->name }}
                @else
                -
                @endif
                </td> 
                <td>{{$item->email }}</td> 
                <td>
                  <div class="admin-table-action-block">
                    <!-- Delete Modal -->
                   <!--  <button type="button" class="btn-danger btn-floating" data-toggle="modal" data-target="#{{$item->id}}deleteModal"><i class="material-icons">delete</i> </button> -->
                    <!-- <a href="{{ url('admin/inquiry/destroy/'.$item->id) }}" class="btn-danger btn-floating" onclick="return confirm('Are you sure you want to delete this item?');"><i class="material-icons">delete</i></a> -->
                    <!-- Modal -->
                    <div id="{{$item->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="delete-icon"></div>
                          </div>
                          <div class="modal-body text-center">
                            <h4 class="modal-heading">Are You Sure ?</h4>
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                          </div>
                          <div class="modal-footer">
                            {!! Form::open(['method' => 'DELETE', 'action' => ['InquiryController@destroy', $item->id]]) !!}
                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        @endif  
      </table>
      {{-- <div class="eloquent-pagination">
        {{ $cities->links() }}
      </div> --}}
    </div>
  </div>
@endsection
