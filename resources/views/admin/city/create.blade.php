@extends('layouts.admin')
@section('content')
  <div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/city')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Add City</h4> 
    {!! Form::open(['method' => 'POST', 'action' => 'CityController@store', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-12">
          
          <div class="form-group">
              {!! Form::label('name', 'City Name*') !!} - 
              {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
              
          </div> 
          <div class="form-group">
              {!! Form::label('slug', 'City Slug*') !!} - 
              {!! Form::text('slug', null, ['class' => 'form-control']) !!}
              
              
          </div> 
          <div class="form-group">
              {{ Form::label('parent') }} - 
              {{ Form::select('parent', $parentArr, '', ['class' => 'form-control']) }}
              
              
              
          </div> 
           
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Create</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@endsection