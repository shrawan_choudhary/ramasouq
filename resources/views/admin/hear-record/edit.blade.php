@extends('layouts.admin')
@section('content')
  <div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/hear-record')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Edit Hear Record</h4> 
    {!! Form::model($edit, ['method' => 'PATCH', 'action' => ['HearController@update', $edit->id]]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-12">
          
          <div class="form-group">
              {!! Form::label('name', 'Name*') !!} - 
              {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
              
          </div> 
          <div class="form-group">
              {!! Form::label('short_name', 'Short Name*') !!} -
              {!! Form::text('short_name', null, ['class' => 'form-control', 'required']) !!}
              
              
          </div> 
          
           
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Update</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@endsection