@extends('layouts.admin')
@section('content')
  <div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <a href="{{route('coupon.create')}}" class="btn btn-danger btn-md">Add Coupon</a>
      <!-- Delete Modal -->
      <a type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#bulk_delete"><i class="material-icons left">delete</i> Delete Selected</a>   
		
	 <!-- Modal -->
      <div id="bulk_delete" class="delete-modal modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="delete-icon"></div>
            </div>
            <div class="modal-body text-center">
              <h4 class="modal-heading">Are You Sure ?</h4>
              <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
              {!! Form::open(['method' => 'POST', 'action' => 'CouponController@bulk_delete', 'id' => 'bulk_delete_form']) !!}
                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="content-block box-body table-responsive">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
	<!-- 	<tr>
			<td><select class='form-control' id='store_id'>
			<option value=''>Select Store</option>
			 @if (isset($store))	
				  @foreach ($store as $key => $sitem)
				<option value='{{ $sitem->id }}'>{{ $sitem->title }}</option>
				@endforeach
			@endif	
			</select></td>
			<td><select class='form-control' id='category_id'>
			<option value=''>Select Category</option>
			
				 @if (isset($category))	
				  @foreach ($category as $key => $Citem)
				<option value='{{ $Citem->id }}'>{{ $Citem->title }}</option>
				@endforeach
			@endif	
			
			
			</select></td>
			<td><select class='form-control' id='is_expired'>
				<option value=''>Select Is Expired</option>
				<option value='is_expired'>Expired Coupons</option>
			</select></td>
		<td><select class='form-control' id='is_redeem'>
				<option value=''>Select Is Redeem</option>
				<option value='Y'>Redeemed Coupons</option>
				<option value='N'>Not Redeemed Coupons</option>
			</select></td>
		
			<td><button type="button" name="filter" id="filter" class="btn btn-info">Filter</button>
			</td>
		</tr> -->
		
		
          <tr class="table-heading-row">
            <th>
              <!--<div class="inline">
                <input id="checkboxAll" type="checkbox" class="filled-in" name="checked[]" value="all" id="checkboxAll">
                <label for="checkboxAll" class="material-checkbox"></label>
              </div>
			  -->
			  #</th>
            <th>Image</th>
         <!--    <th>Type</th>
           <th>Forum Category</th>   -->
            <th>Title</th>
            <th>Store</th>
            <th>Category</th>
           
            <th>Discount</th>
        
         <!--   <th>Link</th> --}}  -->
          
          
            <th>Stauts</th>
            <th>Actions</th>
          </tr>
        </thead>
		<?php //print_r($coupon1); ?>
        @if (isset($coupon))
          <tbody>
      @foreach ($coupon as $key => $item)
             <tr>
                <td>
                  <div class="inline">
                    <input type="checkbox" form="bulk_delete_form" class="filled-in material-checkbox-input" name="checked[]" value="{{$item->id}}" id="checkbox{{$item->id}}">
                    <label for="checkbox{{$item->id}}" class="material-checkbox"></label>
                  </div>
                  {{$key+1}}
                </td>
                <td>
                  @if ($item->image != null)
                    <img src="{{ asset('images/coupon/'.$item->image) }}" class="img-responsive" width="80" alt="image">
                  @else
                    N/A  
                  @endif
                </td>
          
		  <td>{{ $item->title }}</td>
		   <td>{{strtok($item->store->title,'20')}}</td>
		    <td>{{ $item->category ? $item->category->title : '' }}</td>
		    <td>{{strtok($item->discount, ' ')}}   {{strtok($item->discount_type, ' ')}} </td>
		   
			   
                <td>{{$item->is_active == '1' ? 'Active' : 'Deactive'}}</td>
                <td>
                  <div class="admin-table-action-block">
                    <a href="{{route('coupon.edit', $item->id)}}" data-toggle="tooltip" data-original-title="Edit" class="btn-info btn-floating"><i class="material-icons">mode_edit</i></a>
                    <!-- Delete Modal -->
                    <button type="button" class="btn-danger btn-floating" data-toggle="modal" data-target="#{{$item->id}}deleteModal"><i class="material-icons">delete</i> </button>
                    <!-- Modal -->
                   <div id="{{$item->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="delete-icon"></div>
                          </div>
                          <div class="modal-body text-center">
                            <h4 class="modal-heading">Are You Sure ?</h4>
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                          </div>
                          <div class="modal-footer">
                            {!! Form::open(['method' => 'DELETE', 'action' => ['CouponController@destroy', $item->id]]) !!}
                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
			 
            @endforeach 
          </tbody>
		   
        @endif  
      </table>
      {{-- <div class="eloquent-pagination">
        {{ $cities->links() }}
      </div> --}}
    </div>
  </div>
 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  

  <script type="text/javascript" language="javascript" >
 $(document).ready(function(){
  
  fill_datatable();
  
  function fill_datatable(store_id = '', category_id = '' , is_expired = '' , is_redeem = '' )
  {
   var dataTable = $('#full_detail_table').DataTable({
    "processing" : true,
    "serverSide" : true,
   
	
    "searching" : false,
	'lengthMenu': [
                    [10, 25, 50, 100,-1],
                    [10, 25, 50, 100, 'All']
                ],
    "ajax" : {
     url:"{{ url('/getcoupons') }}",
     type:"GET",
     data:{
			store_id:store_id,category_id:category_id,is_expired:is_expired , is_redeem : is_redeem }
    },
		dom: 'Blfrtip'
   });
  }
  
  $('#filter').click(function(){
   var store_id = $('#store_id').val();
   var category_id = $('#category_id').val();
   var is_expired = $('#is_expired').val();
   var is_redeem = $('#is_redeem').val();
 //  var filter_country = $('#filter_country').val();
  
    $('#full_detail_table').DataTable().destroy();
    fill_datatable(store_id, category_id , is_expired , is_redeem);
   // alert('Select Both filter option');
  //  $('#full_detail_table').DataTable().destroy();
  //  fill_datatable();
   
  });
  
  
 });
 
</script> -->

@endsection
