@extends('layouts.themelogin')
@section('main-content')
	<!-- Login -->
	<section id="forum" class="coupon-page-main-block" style='height:100%;'>
		<div class="container">
			
			<!-- breadcrumb end -->
			<div class="forum-page-header" style="
  position: fixed; /* or absolute */
  top: 50%;
  left: 50%;
  /* bring your own prefixes */
  transform: translate(-50%, -50%);
}">
				<div class="row">
					<div class="offset-md-2 col-md-8">
						<div class="login-page-form">
							<div class="forum-page-heading-block">
							<center>	<h5 class="forum-page-heading">Admin Login</h5> </center>
							<center>
							<img style="height:80px;width: 100%;" src="{{asset('images/'.$settings->logo)}}" class="img-fluid" alt="Logo">
							</center>
							</div>
							<form class="login-form" method="POST" action="{{ route('login') }}">
								{{ csrf_field() }}

								<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
									{{-- <label for="email" class="control-label">E-Mail Address</label> --}}
									<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>

									@if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
								</div>

								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
									{{-- <label for="password" class="control-label">Password</label> --}}

									<input id="password" type="password" class="form-control" name="password"  placeholder="Password" required>

									@if ($errors->has('password'))
										<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											{{-- <div class="checkbox">
												<label>
													<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
												</label>
											</div> --}}
										</div>							
					      		
									</div>
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-primary">
										Login
									</button>
								</div>

								
				      		
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
