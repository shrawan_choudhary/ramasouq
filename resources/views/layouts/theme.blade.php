@php
  $category =  App\Category::latest()->paginate();
@endphp
<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 2018 .
**********************************************************************************************************  -->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]> -->
<html lang="en">
<!-- <![endif]-->
<!-- head -->

<head>
<title>{{$settings->w_title ? $settings->w_title : ''}}</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="{{$settings->desc ? $settings->desc : ''}}" />
<meta name="keywords" content="{{$settings->keywords ? $settings->keywords : ''}}">
<meta name="author" content="Media City" />
<meta name="MobileOptimized" content="320" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/icon" href="{{asset('images/favicon/'.$settings->favicon)}}"> 
<!-- favicon-icon -->
<!-- theme styles -->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/> 
<!-- bootstrap css -->
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('vendor/fontawesome/css/fontawesome-all.min.css')}}"/> 
<!-- fontawesome css -->
<link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}"/> 
<!-- fontawesome css -->
<link rel="stylesheet" type="text/css" href="{{asset('vendor/flaticon/flaticon.css')}}"/> <!-- flaticon css -->
<link rel="stylesheet" type="text/css" href="{{asset('vendor/owl/css/owl.carousel.min.css')}}"/> 
<!-- owl carousel css -->
<link rel="stylesheet" type="text/css" href="{{asset('vendor/datatables/css/responsive.datatables.min.css')}}"/> 
<!-- datatables responsive -->
<link href="{{asset('css/jquery.rateyo.css')}}" rel="stylesheet" type="text/css"/> 
<!-- rateyo css -->
<link href="{{asset('vendor/datepicker/datepicker.css')}}" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css"/> <!-- summernote css -->
<link href="{{asset('css/summernote-bs4.css')}}" rel="stylesheet" type="text/css" />
<!-- summernote css -->
<link href="{{asset('css/select2.css')}}" rel="stylesheet" type="text/css"/> 
<!-- select css -->
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/> 
<meta name="csrf-token" content="{{csrf_token()}}">
<!-- custom css -->

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<!-- jquery library js -->
<script>
  window.Laravel =  <?php echo json_encode([
      'csrfToken' => csrf_token(),
  ]); ?>
</script>
<script>
	$( document ).ready(function() {
		@if(Route::currentRouteName() != 'register' && Route::currentRouteName() != 'login' && (count($errors) > 0) && ($errors->has('email1') || $errors->has('password1'))) 
    	$('#register').modal('show');
		@elseif((Route::currentRouteName() != 'login' && Route::currentRouteName() != 'register') && (count($errors) > 0) && (!empty(Session::get('error_code')) && Session::get('error_code') == 5) || ($errors->has('email') || $errors->has('password')))
    	$('#login').modal('show');
    @endif
	});
</script>
<!-- end theme styles -->
</head>
<!-- end head -->
<!-- body start-->
<body>
	<div>
		@include('flash::message')
	</div>
	@if($settings->preloader == 1)
		<!-- preloader --> 
	  <div class="preloader">
	      <div class="status">
	          <div class="status-message">
	          </div>
	      </div>
	  </div>
	@endif
  <!-- end preloader -->
  <!-- topbar -->
	<!--<section id="top-bar" class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-4 d-none d-sm-block">
				<!--	@if(isset($social) && count($social)>0)
						<div class="social-icon">
							<ul>
								@foreach($social as $item)
									<li class="{{strtolower($item->title)}}-icon"><a href="{{$item->url}}" target="_blank" title="{{$item->title}}"><i class="{{$item->icon}}"></i></a></li>
								@endforeach
							</ul>
						</div>
					@endif -->
		<!--		</div>
				
				<div class="col-md-6 col-sm-8">
					<div class="top-nav">
						<ul>
							
							@if(isset($settings->w_email))
								<li><a href="#" class="user-acc" title="Email"><i class="far fa-user"></i>Mail us : {{ $settings->w_email }}</a></li>
							@endif	
							
							@if(isset($settings->w_phone))
								<li><a href="#" class="user-mobile" title="Contact No"><i class="fa fa-phone"></i>Contact us : {{ $settings->w_phone }}</a></li>
							@endif	
								
							
						</ul>
					</div>
				</div>
				
				
			</div>
		</div>
		
		<!-- search -->
	<!--	<div class="search">
			<div class="container clearfix">
				{!! Form::open(['method' => 'GET', 'action' => 'SearchController@homeSearch', 'class' => 'forum-search']) !!}
					<input type="search" name="search" class="search-box" placeholder="Type anything here...." />
					<a href="#" class="fa fa-times search-close"></a>
				{!! Form::close() !!}
			</div>
		</div>
		-->
		
		<!-- end search -->
	
	<!--
	</section>
	-->
	
	
	<!--Category Modal End-->
	
	<!--
	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
 <!--   <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Download App</h4>
      </div>
      <div class="modal-body">
        <p align='center'>
		
		 @if(isset($settings) && $settings->is_playstore)
			 <div class='row'>
		            <div class="app-badge play-badge col-sm-6">
		            	<a href="{{$settings->playstore_link}}" target="_blank" title="Google Play">
					<img style='height:80px' src="{{asset('images/google-play.png')}}" class="img-fluid" alt="Google Play"></a>
		            </div>
		          @endif
		          @if(isset($settings) && $settings->is_app_icon)
		            <div class="app-badge col-sm-6">
		            	<a href="{{$settings->app_link}}" target="_blank" title="Apple App Store">
						<img  style='height:80px' src="{{asset('images/app-store.png')}}" class="img-fluid" alt="Apple App Store"></a>
		            </div>
		          @endif
			</div>
		
		</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	-->
	
	
	<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Download App</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
                <p align='center'>
		
		 @if(isset($settings) && $settings->is_playstore)
			 <div class='row'>
		            <div class="app-badge play-badge col-sm-6">
		            	<a href="{{$settings->playstore_link}}" target="_blank" title="Google Play">
					<img style='height:80px' src="{{asset('images/google-play.png')}}" class="img-fluid" alt="Google Play"></a>
		            </div>
		          @endif
		          @if(isset($settings) && $settings->is_app_icon)
		            <div class="app-badge col-sm-6">
		            	<a href="{{$settings->app_link}}" target="_blank" title="Apple App Store">
						<img  style='height:80px' src="{{asset('images/app-store.png')}}" class="img-fluid" alt="Apple App Store"></a>
		            </div>
		          @endif
			</div>
		
		</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
	
	
	
	<!---Category Modal end--->
	
	
	<!-- end topbar -->
	<!-- login -->
	
	<!-- end login -->
	<!-- register -->

	<!-- end register -->
	<!-- logo -->
	<section class="logo-block">
		<div class="container">
			<div class="row">
				<div class="col-6 col-lg-6 col-md-6 pr-0">
				

				<div class="logo">
						@if($settings->logo != Null)
							<a href="{{url('/')}}" title="Home">
						<img src="{{asset('images/'.$settings->logo)}}" class="img-fluid" alt="Logo"></a>
						@else
							<h2 class="logo-title">{{$settings->w_name ? $settings->w_name : 'Logo'}}</h2>
						@endif
				<!--	<ul align='center'>
							
							@if(isset($settings->w_email))
								<li><a style='color:black;cursor:pointer' href="#" class="user-acc" title="Email"><i class="far fa-user"></i>Mail us : {{ $settings->w_email }}</a></li>
							@endif	
							
							@if(isset($settings->w_phone))
								<li><a style='color:black;cursor:pointer' class="user-mobile" title="Contact No"><i class="fa fa-phone"></i>Contact us : {{ $settings->w_phone }}</a></li>
							@endif	
								
							
						</ul>	
						-->
					</div>
					
				</div>	
				<div class="col-6 col-lg-6 col-md-6 pl-0">
				    <div class="float-right">
					<div class="top-nav display-n float-left" style="text-transform: lowercase;">
						<ul class="">						
							@if(isset($settings->w_email))
								<li><a  style='color:black;' href="#" class="user-acc" title="Email"><i class="far fa-envelope"></i><span class="display-n"></span> {{ $settings->w_email }}</a></li>
							@endif	
							
							@if(isset($settings->w_phone))
								<li><a  style='color:black;' href="#" class="user-mobile" title="Contact No"><i class="fa fa-phone"></i><span class="display-n"></span> {{ $settings->w_phone }}</a></li>
							@endif	
						</ul>
					</div>
					@if(isset($social) && count($social)>0)
						<div class="social-icon float-left">
							<ul style='float:right'>
								@foreach($social as $item)
									<li class="{{strtolower($item->title)}}-icon"><a href="{{$item->url}}" target="_blank" title="{{$item->title}}"><i class="{{$item->icon}}"></i></a></li>
								@endforeach
							</ul>
						</div>
					@endif
					</div>
				</div>	

					
				
				
			</div>
		</div>
	</section>
	<!-- end logo -->
	
<!---   height: 64px !important; --->
	</style>
	<div id="site-header" style="    position: sticky;top: 0;z-index: 99;">
	<section class="navbar mb-0 pb-0" >
		<div class="container">
			<nav class="navbar navbar-expand-lg mb-0">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="position: absolute; top: 0;font-size: 25px;-webkit-box-shadow: 0px 6px 10px -5px rgba(0,0,0,0.75);">
					<span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
				</button>
				<div class="collapse navbar-collapse pl-0 pr-0 mr-0" id="navbarSupportedContent">
					
					<ul class="navbar-nav p-0 m-0">
			      <li class="nav-item" align=''>
			        <a class="nav-link {{ Nav::isRoute('home') }}" href="{{url('/')}}">Home</a>
			      </li>
			  
				  
			      <li class="nav-item">
			        <a class="nav-link {{ Nav::isRoute('about-us') }}" href="{{url('about-us')}}">About Us</a>
			      </li>
				  
				  <li class="nav-item">
			        <a class="nav-link {{ Nav::isRoute('contact') }}" href="{{url('contact')}}">Contact Us</a>
			      </li>

				  <li class="nav-item">
			        <a class="nav-link {{ Nav::isRoute('terms-and-conditions') }}" href="{{url('terms-and-conditions')}}">Terms & Conditions</a>
			      </li>

				  <li class="nav-item">
			        <a class="nav-link {{ Nav::isRoute('faq') }}" href="{{url('faq')}}">FAQ</a>
			      </li>
			    </ul>
				
			  </div>
			</nav>
		</div>
	</section>
</div>

	
	
	
	<!-- end navbar -->	
	@yield('main-content')



	<!-- footer start -->
	
	<div id="back-what-app">
  <a title="what app" href="https://api.whatsapp.com/send?phone=+91-{{$settings->whatsapp_number}}&text={{ $settings->whatsapp_msg }}" target="_blank">
    <i class="fa fa-whatsapp" aria-hidden="true"></i>
  </a>
</div>
	
	

	<footer id="footer" class="footer-main-block" style="background: #00aeefb8">
	  <div style="height: 0px">
	  	<a id="back2Top" title="Back to top" href="#">&#10148;</a>
	  </div>
		@if($settings->footer_layout == 1)
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">{{$settings->f_title1}}</h6>
	            @if(isset($f_menu))	
								<ul>
									@foreach($f_menu as $item)														
		            		@if($item->widget == '1')
															
											<li><a style='color:black;' href="{{url($item->slug)}}" title="{{$item->title}}">{{$item->title}}</a></li>
							
							@endif
									@endforeach
						<li><a  style='color:black;' href="{{url('/contact')}}" title="contact us">Contact Us</a></li>
								</ul>
							@endif
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">{{$settings->f_title2}}</h6>
							@if(isset($f_menu))	
								<ul>
									@foreach($f_menu as $item)														
		            		@if($item->widget == '2')	
								
											<li><a  style='color:black;' href="{{url($item->slug)}}" title="{{$item->title}}">{{$item->title}}</a></li>
										
										@endif
									@endforeach
									
								</ul>
							@endif
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">ADVERTISE / SELL</h6>
							
								<ul>
								
		            						<li><a style='color:black;' href="{{ url('/sell') }}">Sell On Rama Souq</a></li>	
											<li><a  style='color:black;' href="{{ url('/grow-bussiness') }}">Growyour Business</a></li>
											@foreach($f_menu as $item)														
		            		@if($item->widget == '4')
											<li><a  style='color:black;'href="{{url($item->slug)}}">{{ $item->title }}</a></li>
											@endif
											@endforeach
								</ul>
							
						</div>
					</div>
					<div class="laptop-non text-left">
					<div class="col-12">
						
					<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">Contact :</h6>
							<ul>						
							@if(isset($settings->w_email))
								<li><a  style='color:black;' href="#" class="" title="Email"><i class="far fa-envelope"></i><span class="display-n">Mail us </span>: {{ $settings->w_email }}</a></li>
							@endif	
							
							@if(isset($settings->w_phone))
								<li><a  style='color:black;' href="#" class="" title="Contact No"><i class="fa fa-phone"></i><span class="display-n">Contact us </span>: {{ $settings->w_phone }}</a></li>
							@endif	
							</ul>
						</div>
					</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget footer-subscribe">				
							<h6 class="footer-widget-heading" >{{$settings->f_title4}}</h6>
							@if(isset($settings) && $settings->is_mailchimp)		
								<p  style='color:black;'>{{$settings->m_text}}</p>
								{!! Form::open(['method' => 'POST', 'action' => 'EmailSubscribeController@subscribe', 'id' => 'subscribe-form', 'class' => 'subscribe-form ']) !!}
	              	{{ csrf_field() }}
									<div class="row no-gutters">
										<div class="col-9 col-md-9">
											<div class="form-group">
				                <label class="sr-only">Your Email address</label>
				                <input type="email" class="form-control" id="mc-email" placeholder="Enter email address" style="border-radius: 0;">
				              </div>
										</div>
										<div class="col-3 col-md-3">
											<button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
			              	<label for="mc-email"></label>
										</div>
									</div>
	              {!! Form::close() !!}
	            @endif
	            <div class="row">
	            @if(isset($settings) && $settings->is_playstore)
		            <div class="col-6 col-lg-6 app-badge play-badge">
		            	<a href="{{$settings->playstore_link}}" target="_blank" title="Google Play"><img src="{{asset('images/google-play.png')}}" class="img-fluid" alt="Google Play" style="height:40px; width:100%;"></a>
		            </div>
		          @endif
		          @if(isset($settings) && $settings->is_app_icon)
		            <div class="col-6 col-lg-6 app-badge">
		            	<a href="{{$settings->app_link}}" target="_blank" title="Apple App Store"><img src="{{asset('images/app-store.png')}}" class="img-fluid" alt="Apple App Store" style="height:40px; width:100%;"></a>
		            </div>
		          @endif
		      	</div>
						</div>
					</div>
				</div>
				<div class="border-divider">
				</div>
				<div class="copyright">
					<div class="row">
						<div class="col-md-6">
							<div class="copyright-text">
				  			<p  style='color:black;'>&copy; <?php echo date("Y"); ?><a  style='color:black;' href="{{url('/')}}" title="{{$settings->w_name}}"> {{$settings->w_name}}</a> | {{$settings->copyright}}</p>
			        </div>
						</div>
						<div class="col-md-6">
							@if(isset($social) && count($social)>0)
							<div class="social-icon">
								<ul>
									@foreach($social as $item)
										<li class="{{strtolower($item->title)}}-icon"><a href="{{$item->url}}" target="_blank" title="{{$item->title}}"><i class="{{$item->icon}}"></i></a></li>
									@endforeach
								</ul>
							</div>
						@endif
						</div>
					</div>
		    </div>
			</div>
		@else
			<div class="container footer2 ">
				<div class="row">
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget footer-subscribe">
							<div class="logo">
								@if($settings->footer_logo != Null)
									<a href="{{url('/')}}" title="Home"><img src="{{asset('images/'.$settings->footer_logo)}}" class="img-fluid" alt="Footer Logo"></a>
								@else
									<h2 class="logo-title" style="color:#FFF;">{{$settings->w_name ? $settings->w_name : 'Logo'}}</h2>
								@endif
							</div>
							<p>{{$settings->footer_text ? $settings->footer_text : ''}}</p>
							@if(isset($settings) && $settings->is_mailchimp)
								<p>{{$settings->m_text}}</p>	
								{!! Form::open(['method' => 'POST', 'action' => 'EmailSubscribeController@subscribe', 'id' => 'subscribe-form', 'class' => 'subscribe-form']) !!}
	              	{{ csrf_field() }}
									<div class="row no-gutters">
										<div class="col-md-9">
											<div class="form-group">
				                <label class="sr-only">Your Email address</label>
				                <input type="email" class="form-control" id="mc-email" placeholder="Enter email address">
				              </div>
										</div>
										<div class="col-md-3">
											<button style='height:33px;' type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
			              	<label for="mc-email"></label>
										</div>
									</div>
	              {!! Form::close() !!}	
	            @endif
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">{{$settings->f_title2}}</h6>
							@if(isset($f_menu))	
								<ul>
									@foreach($f_menu as $item)														
		            		@if($item->widget == '2')
		            		@if($item->id != '12')
		            		@if($item->id != '13')
							@if($item->id != '11')				
											<li><a href="{{url($item->slug)}}" target="_blank" title="{{$item->title}}">{{$item->title}}</a></li>
										@endif
										@endif
										@endif
										@endif
									@endforeach
								</ul>
							@endif
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">{{$settings->f_title3}}</h6>
							@if(isset($f_menu))	
								<ul>
									@foreach($f_menu as $item)														
		            		@if($item->widget == '3')	
											<li><a href="{{url($item->slug)}}" target="_blank" title="{{$item->title}}">{{$item->title}}</a></li>
										@endif
									@endforeach
								</ul>
							@endif
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget app-widget">
							<h6 class="footer-widget-heading">{{$settings->f_title4}}</h6>
							@if($settings->w_address)
								<ul class="contact-widget-dtl">                   
								  <li><i class="fas fa-map-marker"></i></li>
								  <li>{{$settings->w_address}}</li>
								</ul>
							@endif
							@if($settings->w_address)
								<ul class="contact-widget-dtl">  
								  <li><i class="fas fa-phone"></i></li>		
								  <li><a href="tel:{{$settings->w_phone}}">{{$settings->w_phone}}</a></li>
								</ul>
							@endif
							@if($settings->w_address)
								<ul class="contact-widget-dtl">  
								  <li><i class="fas fa-envelope"></i></li>
								  <li><a href="mailto:{{$settings->w_email}}?Subject=Hello%20again" target="_top">{{$settings->w_email}}</a></li>	
								</ul>
							@endif	
							@if($settings->w_time)
								<ul class="contact-widget-dtl">  
								  <li><i class="fas fa-clock"></i></li>
								  <li>{{$settings->w_time}}</li>	
								</ul>
							@endif
						</div>
					</div>
				</div>
				<div class="border-divider">
				</div>
				<div class="copyright">
					<div class="row">
						<div class="col-md-4">
							<div class="copyright-text">
				  			<p>&copy; <?php echo date("Y"); ?><a href="{{url('/')}}" title="{{$settings->w_name}}"> {{$settings->w_name}}</a> | {{$settings->copyright}}</p>
			        </div>
						</div>
						<div class="col-md-4">
							<div class="footer2-icon text-center">
								<ul>
									<li>
										@if(isset($settings) && $settings->is_playstore)
					            <div class="app-badge play-badge">
					            	<a href="{{$settings->playstore_link}}" target="_blank" title="Google Play"><img src="{{asset('images/google-play.png')}}" class="img-fluid" alt="Google Play"></a>
					            </div>
					          @endif
					        </li>
					        <li>
					          @if(isset($settings) && $settings->is_app_icon)
					            <div class="app-badge">
					            	<a href="{{$settings->app_link}}" target="_blank" title="Apple App Store"><img src="{{asset('images/app-store.png')}}" class="img-fluid" alt="Apple App Store"></a>
					            </div>
					          @endif
					        </li>
					      </ul>
			        </div>
			      </div>
						<div class="col-md-4">
							@if(isset($social) && count($social)>0)
							<div class="social-icon">
								<ul>
									@foreach($social as $item)
										<li class="{{strtolower($item->title)}}-icon"><a href="{{$item->url}}" target="_blank" title="{{$item->title}}"><i class="{{$item->icon}}"></i></a></li>
									@endforeach
								</ul>
							</div>
						@endif
						</div>
					</div>
		    </div>
			</div>
		@endif
	</footer>
	
	
	<!-- footer end -->
<!-- jquery -->
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script> 
<!-- bootstrap js -->
<script src="{{asset('js/select2.js')}}"></script> 
<!-- select2 js --> 
<script src="{{asset('vendor/owl/js/owl.carousel.min.js')}}"></script> 
<!-- owl carousel js -->
<script src="{{asset('vendor/mailchimp/jquery.ajaxchimp.min.js')}}"></script> 
<!-- mailchimp js -->
<script src="{{asset('vendor/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- bootstrap datepicker js-->
<script src="{{asset('vendor/datatables/js/jquery.datatables.min.js')}}"></script> 
<!-- datatables bootstrap js -->		
<script src="{{asset('vendor/datatables/js/datatables.responsive.min.js')}}"></script> <!-- datatables bootstrap js -->		
<script src="{{asset('vendor/datatables/js/datatables.min.js')}}"></script> 
<!-- datatables bootstrap js -->
<script src="{{asset('vendor/summernote/js/summernote-bs4.min.js')}}"></script>
<!-- summernote js -->
<script src="{{asset('vendor/clipboard/js/clipboard.min.js')}}"></script>
<!-- clipboard js -->
<script src="{{asset('js/jquery.rateyo.js')}}"></script> 
<!-- Rateyo js --> 
<script src="{{asset('js/theme.js')}}"></script> 
<script src="{{asset('js/ajax.js')}}"></script> 
<!-- custom js -->
@yield('custom-scripts')
<script>
$(document).ready(function(){$(".grab-now").click(function(){var n=$(this).data("id");console.log(n),$.ajax({headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},type:"GET",url:"{{ url('counter') }}",data:{id:n},error:function(n,o,t){console.log(n)}})})});
</script>
@if($settings->right_click == 1)
  <script type="text/javascript" language="javascript">
   // Right click disable 
    $(function() {
	    $(this).bind("contextmenu", function(inspect) {
	    	inspect.preventDefault();
	    });
    });
      // End Right click disable 
  </script>
@endif
<!-- @if($settings->inspect == 1)
<script type="text/javascript" language="javascript">
//all controller is disable 
  $(function() {
	  var isCtrl = false;
	  document.onkeyup=function(e){
		  if(e.which == 17) isCtrl=false;
		}
		document.onkeydown=function(e){
		  if(e.which == 17) isCtrl=true;
		  if(e.which == 85 && isCtrl == true) {
			  return false;
			}
	  };
    $(document).keydown(function (event) {
      if (event.keyCode == 123) { // Prevent F12
        return false;
  		} 
      else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I
	     	return false;
	   	}
 		});  
	});
  // end all controller is disable 
 </script>
@endif -->
@if($settings->is_gotop==1)
	<script type="text/javascript">
	 //Go to top
	$(window).scroll(function() {
	  var height = $(window).scrollTop();
	  if (height > 100) {
	      $('#back2Top').fadeIn();
	  } else {
	      $('#back2Top').fadeOut();
	  }
	});
	$(document).ready(function() {
	  $("#back2Top").click(function(event) {
	      event.preventDefault();
	      $("html, body").animate({ scrollTop: 0 }, "slow");
	      return false;
	  });
	});
	// end go to top 
	
	</script>
@endif
<script>
	$(window).on("scroll", function () {
      var scroll = $(window).scrollTop();

      // if (scroll > 120) {
      //   $("#site-header").addClass("nav-fixed");
      // } else {
      //   $("#site-header").removeClass("nav-fixed");
      // }
      
    });
</script>

<script>
  $('#states').change(function(){
  var stateID = $(this).val();  
  if(stateID){
    $.ajax({
      type:"GET",
      url:"{{url('ajax/city')}}?parent="+stateID,
      success:function(res){        
      if(res){
        $("#city").empty();
        $("#city").append('<option  selected disabled>Select</option>');
        $.each(res,function(key,value){
          $("#city").append('<option  value="'+key+'">'+value+'</option>');
        });
      
      } else {
        $("#city").empty();
      }
      }
    });
  }else{
    $("#city").empty();
  }   
  });
  // $('#city').on('change',function(){
  // var cityID = $(this).val();  
  // if(cityID){
  //   $.ajax({
  //     type:"GET",
  //     url:"{{url('city')}}?city="+cityID,
  //     success:function(res){        
  //     if(res){
  //       $("#city").empty();
  //       $.each(res,function(key,value){
  //         $("#city").append('<option value="'+key+'">'+value+'</option>');
  //       });
      
  //     }else{
  //       $("#city").empty();
  //     }
  //     }
  //   });
  // }else{
  //   $("#city").empty();
  // }
    
  // });
</script>
<script type="text/javascript">
	function submitForm() {
			   // Get the first form with the name
			   // Usually the form name is not repeated
			   // but duplicate names are possible in HTML
			   // Therefore to work around the issue, enforce the correct index
			   var frm = document.getElementsByName('contact-form')[0];
			   frm.submit(); // Submit the form
			   frm.reset();  // Reset all form data
			   return false; // Prevent page refresh
			}
</script>
<!-- Add Qulink script Here -->
<!-- end jquery -->
</body>	
<!-- body end -->
</html>

