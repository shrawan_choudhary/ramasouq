@extends('layouts.theme')
@section('main-content')
<!-- contact -->
	    <div class="forum-page-header mb-5" style="background: url('{{url('images/favicon/'.$setting->banner_img)}}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
	  		<div class="container">
		        <div class="forum-page-heading-block">
		          <h2 class="forum-page-heading text-center">Contact Us</h2>
		        </div>
		    </div>
		</div>
	<section class="contact-section">
<div class="container">
<div class="row">
<div class="col-lg-6 col-12 contact-info margin-btm-mobile">

{{ Form::open(['id'=>'contact_inquiry', 'data-url'=>route('contact-route') ,'class'=>'contact-form mb-5']) }}
<input type="text" id="contact_name" placeholder="Your name">
<input type="text" id="contact_email" placeholder="Your e-mail">
<input type="text" id="contact_mobile" placeholder="Your Mobile no.">
<input type="text" id="contact_subject" placeholder="Subject">

<!--<div class="form-group">-->
    
<!--    <select id="contact_category" class="form-control" name="category" required>-->
<!--	<option value="inquiry">Inquiry</option>-->
<!--	<option value="feedback">Feedback</option>-->
<!--	<option value="suggestions">Suggestions</option>-->
<!--	<option value="coupons-issue">Coupons Issues</option>-->
<!--	<option value="deal-issue">Deal Issues</option>-->
<!--	<option value="forums-issue">Forums Issues</option>-->
<!--	<option value="report-user">Report User</option>-->
<!--	<option value="dmca">DMCA</option>-->
<!--	<option value="others">Others</option>-->
<!--</select>-->
<!--</div>-->
<div class="form-group">
<!--<label> Select Category</label>-->
<select name="category" id="contact_category" class="form-control" >
    <option value="" selected disabled>Select Category</option>
     @foreach($category as $list)
     <option  value="{{$list->title}}"> {{$list->title}}</option>
     @endforeach
 </select>
</div>
<input type="text" id="contact_website" placeholder="Website">
<textarea placeholder="Message" id="contact_message" style="padding:10px;height:60px;"></textarea>
<p id="response_message"></p>
<button type="submit" class="site-btn float-right" onclick="" >SEND NOW</button>
{{ Form::close() }}

</div>
<div class="col-lg-6 col-12">
    <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen=""></iframe></div>
</div>
</div>
</div>
</section>
	    <!-- breadcrumb -->
	    <!-- <div id="breadcrumb" class="breadcrumb-main-block">
        <div class="breadcrumb-block">
          <ol class="breadcrumb">
            <li><a href="{{url('/')}}" title="Home">Home</a></li>
            <li class="active">Contact Us</li>
          </ol>
        </div>
	    </div> -->
	    <!-- breadcrumb end -->
			<!-- <div class="coupon-page-block categories-page contact-page">
				<div class="container coupon-dtl-outer">
					<div class="row">
						<div class="col-lg-2 col-md-2">
						</div>
						<div class="col-lg-8 col-md-8">
							<div class="submit-deal-main-block">
								<form id="submit-deal" class="submit-deal-form contact-form" action="{{ action('PageController@contact_post') }}" method="POST">
									{{ csrf_field() }}
									<input type="hidden" name="w_email" value="info@lootcoupon.com">
									<div class="form-group">
										<label for="name">Name*</label>
										<input style='height:44px;' type="text" name="name" id="name" class="form-control" placeholder="Enter Your Full Name" required>
									</div>
									<div class="form-group">
										<label for="store">Choose Categories</label>
										<select style='height:44px;' class="form-control" name="category" id="store" required>
											<option value="inquiry">Inquiry</option>
											<option value="feedback">Feedback</option>
											<option value="suggestions">Suggestions</option>
											<option value="coupons-issue">Coupons Issues</option>
											<option value="deal-issue">Deal Issues</option>
											<option value="forums-issue">Forums Issues</option>
											<option value="report-user">Report User</option>
											<option value="dmca">DMCA</option>
											<option value="others">Others</option>
										</select>
									</div>
									<div class="form-group">
										<label for="subject">Subject*</label>
										<input style='height:44px;' type="text" name="subject" id="subject" class="form-control" placeholder="Enter Subject" required>
									</div>
									<div class="form-group">
										<label for="email">Email Address*</label>
										<input style='height:44px;' type="email" name="email" id="email" class="form-control" placeholder="Enter Your Email Address" required>
									</div>
									<div class="form-group">
										<label for="mobile">Mobile Number*</label>
										<input style='height:44px;' type="text" name="mobile" id="mobile" class="form-control" placeholder="Enter Mobile Number" required>
									</div>
	                <div class="form-group">
	                  <label for="website">Website</label>
	                  <input style='height:44px;' type="url" id="website" name="website" class="form-control" placeholder="Enter Your Website">
	                </div>
									<div class="form-group">
										<label for="message">Message</label>
										<textarea class="form-control" name="message" id="c_message" placeholder="Enter Your Message" required></textarea>
									</div>
									<div class="form-group">
										<div class="submit-deal-btn">
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
												<div class="col-lg-2 col-md-2">
						</div> -->

					<!--	
						<div class="col-lg-3 col-md-4">
							<div class="coupon-sidebar">
	    					@include('includes.side-bar')
							</div>
						</div>
						
					-->	
					<!-- </div>
				</div>
			</div> -->
		</div>
	</section>
<!-- end forum -->
@endsection