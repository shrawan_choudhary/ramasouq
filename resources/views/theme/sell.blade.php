@extends('layouts.theme')
@section('main-content')
	 <div class="forum-page-header mb-5" style="background: url('{{url('images/favicon/'.$setting->banner_img)}}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
	  		<div class="container">
		        <div class="forum-page-heading-block">
		          <h2 class="forum-page-heading text-center">Sell On Rama Souq</h2>
		        </div>
		    </div>
		</div>
		<section class="container my-5">
			<div class="sell-form-card">

				{{ Form::open(['id'=>'inquiry_form', 'data-url'=>route('ajax-route') ,'class'=>'contact-form mb-5']) }}
					<label>Enter Company / Business / Individual Name</label>
					{{ Form::text('company_name', '', ['class' => 'form-control validate', 'id'=>'company_name', 'placeholder'=>'Caompany name','required'=>'required'])}}
					
					<div class="form-group">
						<label>Select state</label>
				        <select id="states" name="states" class="form-control selectpicker" >
					        <option value="" selected disabled>Select</option>
					         @foreach($states as $key => $name)
					         <option value="{{$key}}"> {{$name}}</option>
					         @endforeach
				         </select>
				      </div>
				      <div class="form-group">
				        <label for="title">Select city:</label>
				        <select name=city id="city" class="form-control">
				        </select>
				        </div>
				        <div class="form-group">
				        <label> Select Category</label>
				        <select name="business_category" id="business_category" class="form-control" >
					        <option value="" selected disabled>Select</option>
					         @foreach($category as $list)
					         <option  value="{{$list->title}}"> {{$list->title}}</option>
					         @endforeach
				         </select>
				      </div>



					
					<label>Enter Your Name</label>
					{{ Form::text('name', '', ['class' => 'form-control validate', 'id'=>'name', 'placeholder'=>'name'])}}
					<label>Enter Your Mobile No. </label>
					{{ Form::text('mobile_no', '', ['class' => 'form-control validate', 'id'=>'mobile_no', 'placeholder'=>'mobile no'])}}
					<label>Enter Your Email ID  </label>
					{{ Form::text('email', '', ['class' => 'form-control validate', 'id'=>'email', 'placeholder'=>'email'])}}
					<label>How did you hear about ramasouq ?</label>
					
					<select style='height:44px;' class="form-control" name="hear" id="hear" >
					        <option value="" selected disabled>Select</option>
					         @foreach($hear as $list)
					         <option  value="{{$list->name}}"> {{$list->name}}</option>
					         @endforeach
				         </select>
					<input type="hidden" class="form-control" id="current_page" name="page_name" value="{{URL::current()}}"   required placeholder="Job title" maxlength="50">

					<div class="row">
					<input type="checkbox" name="term_condition" id="term_condition" class="m-0 p-0 ml-4" style="width: 20px;" value="1" required>
					<!-- {{ Form::checkbox('term', '', ['class' => 'form-control validate term-check m-0 p-0 ml-4', 'id'=>'term'])}} -->
					<label class="mt-4 ml-2">By creating this account i agree to <b>Ramasouq </b> <a href="{{url('terms-and-conditions')}}">Terms and Condition</a></label>
						
					</div><p class="text-success" style="position: absolute; font-weight: bold; font-size:25px;" id="ajax_message"></p>
					<button type="submit" class="site-btn float-right" onclick="" >SEND NOW</button>
				{{ Form::close() }}
			</div>
		</section>
@endsection

