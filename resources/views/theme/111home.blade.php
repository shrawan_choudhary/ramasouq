@extends('layouts.theme')
@section('main-content')



	<section  class="section">
	<div class="container">	
		
									<div class="blog-page-main-block" >
									<div class="blog-post-main border" >
										<div class='row' style='margin: 0 auto; padding-left:140px'>
				@if(isset($settings) && $settings->is_category_block)
					@if(isset($category_list) && count($category_list) > 0)
						@foreach($category_list->take(12) as $key => $item)
											<div class='col-sm-5' style='margin:5px;'>
										
												<div class="row">
												<style>
.padding-0{
    padding-right:0;
    padding-left:0;
}
												</style>		
												
													
													<div class="col-lg-6 padding-0" style='background-color:{{$item->color_code}};'>
														<div class="blog-post-dtl">
														<br><center>	<h6 class="blog-post-heading">
														<a href="" data-toggle="modal" data-target="#myModal" title="{{strtok($item->title,' ')}}" style='color:white'>{{strtok($item->title,' ')}}</a></h6>
															</center>
															<div class="blog-post-tags">
																
															</div>
															<div class="blog-post-text">
																<p></p>
															</div>
															<div class="blog-post-link">
																<br><center><a href='' data-toggle="modal" data-target="#myModal" title="View List" style='color:white'>View List</a></center>
															</div>
														</div>
													</div>
													<div class="col-lg-6 padding-0">
														<div class="blog-img">
															<a href=""  data-toggle="modal" data-target="#myModal" title="{{strtok($item->title,' ')}}"><img style='height:180px;' src="images/category/{{$item->image}}" class="img-fluid" alt="Categories"></a>
														
</div>
													</div>
													
												</div>
										
												</div>
												@endforeach
		@endif										
		@endif										
											
												
												</div>
												
												
												
												
												
											</div>
										
																			
																					
																			</div>
								</div>						
								
						
		
		
	
	
	</section>


		
		
		
		
		
		


	
	
	
	
<!-- end deal -->
<!-- categories -->



<!--
@if(isset($settings) && $settings->is_category_block)
	<section id="categories" class="categories-main-block">
		<div class="container">			
			@if(isset($category_list) && count($category_list) > 0)
				<div class="section">
					<h4 class="section-heading">Categories</h4>
				</div>
				<div class="cat-block text-center">
					<div class="row">
						@foreach($category_list->take(12) as $key => $item)
							<div class="col-lg-2 col-md-4">
								<div class="category-block">
									<a href="{{url('category-dtl/'.$item->slug)}}" title="Categories">
										<div class="cat">
											<div class="{{$item->icon ? 'cat-icon' : ''}}">
												@if($item->icon)
													<i class="fa {{$item->icon}}"></i>
												@else
													<img src="{{asset('images/category/'.$item->image)}}" class="img-fluid" alt="category">
												@endif
											</div>
											<h5 class="cat-title">{{strtok($item->title,' ')}}</h5>
										</div>
									</a>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			@endif
		</div>
	</section>
@endif

-->


<!-- end featured stores -->


@endsection
@section('custom-scripts')
<script>
$(document).ready(function(){$(".cat-nav li").click(function(e){e.preventDefault();$(this).addClass("active"),$(this).parent().children("li").not(this).removeClass("active")});var e="all",t="all",a="all",l=1;function n(l){$.ajax({headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},type:"GET",url:"{{ url('homefilter') }}?page="+l,data:{filter:t,s_filter:e,c_filter:a,main:"0"},datatype:"html",beforeSend:function(){$(".load-more-btn").hide(),$(".ajax-loading").show()},success:function(e){console.log(e)},error:function(e,t,a){console.log(e)}}).done(function(e){if(!e)return console.log("no"),$(".ajax-loading").hide(),1==l&&$(".results").html("No Results Found!"),0;$(".ajax-loading").hide(),1==l?$(".results").html(e):$(".results").append(e),$(e).find(".deal-block").length>35&&$(".load-more-btn").show()}).fail(function(e,t,a){alert("We are facing some issues currenlty. Please try again later.")})}$(".home-filter li").on("click change keyup",function(){t=$(".cat-nav li.active").attr("id"),e=$("#store-list").val(),a=$("#cat-list").val(),console.log(a),console.log(t),n(l=1)}),$(".load-more-btn").on("click",function(){n(++l)})});
</script>
@endsection
