@extends('layouts.theme')
@section('main-content')

 

<section  class="section p-0">
	
<div class="home-slider menu-part">
    <video src="{{ url('images/RAMA SOUQ.mp4') }}" loop="" autoplay="" muted=""> </video>
</div>

</section>
</div>
	<section  class="section">
		<div class="container">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
			<style>
			.padding-0{
					padding-right:0;
					padding-left:0;
				}
				.card:hover .card-content a .card-category{
					margin-top: 40px;
				}
			</style>			
			<div class="blog-page-main-block" >
			<div class="blog-post-main" >
				<div class="row  " style='margin: 5px 0px' style=''>
				@if(isset($settings) && $settings->is_category_block)
				@if(isset($category_list) && count($category_list) > 0)
				@foreach($category_list->take(12) as $key => $item)	
							
					<div class="col-sm-4 padding-0 " >
					<div class="cat_card">
							<img src="{{ url('images/category/'.$item->image) }}" height="275px" >

							<div class="cat_card_content"style="background: {{ $item->color_code }}; position: relative;z-index: 999">
								<div style="position: absolute;left: 0;right: 0;top:0;bottom:0; z-index: 50;"></div>
								<p class="card-category text-white">{{$item->title}}</p>
							</div>

							<div class="cat_card_hover" style="background: {{ $item->color_code }}55;">
								<div class='row'>
								@if(isset($settings) && $settings->is_playstore)
								    <div class="app-badge play-badge col-sm-6 pr-0 mr-0">
								    	<a href="{{$settings->playstore_link}}" target="_blank" title="Google Play">
											<img style='height: 50px' src="{{asset('images/google-play.png')}}" class="img-fluid" alt="Google Play"></a>
								    </div>
								  @endif
								  @if(isset($settings) && $settings->is_app_icon)
								    <div class="app-badge col-sm-6 ml-0 pl-0">
								    	<a href="{{$settings->app_link}}" target="_blank" title="Apple App Store">
										<img  style='height: 50px' src="{{asset('images/app-store.png')}}" class="img-fluid" alt="Apple App Store"></a>
								    </div>
								  @endif
								</div>
							</div>
						</div>

					</div>

					@endforeach
					@endif										
					@endif										
					</div>		
				</div>												
			</div>
		</div>
	</section>
@endsection
@section('custom-scripts')
<script>
$(document).ready(function(){$(".cat-nav li").click(function(e){e.preventDefault();$(this).addClass("active"),$(this).parent().children("li").not(this).removeClass("active")});var e="all",t="all",a="all",l=1;function n(l){$.ajax({headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},type:"GET",url:"{{ url('homefilter') }}?page="+l,data:{filter:t,s_filter:e,c_filter:a,main:"0"},datatype:"html",beforeSend:function(){$(".load-more-btn").hide(),$(".ajax-loading").show()},success:function(e){console.log(e)},error:function(e,t,a){console.log(e)}}).done(function(e){if(!e)return console.log("no"),$(".ajax-loading").hide(),1==l&&$(".results").html("No Results Found!"),0;$(".ajax-loading").hide(),1==l?$(".results").html(e):$(".results").append(e),$(e).find(".deal-block").length>35&&$(".load-more-btn").show()}).fail(function(e,t,a){alert("We are facing some issues currenlty. Please try again later.")})}$(".home-filter li").on("click change keyup",function(){t=$(".cat-nav li.active").attr("id"),e=$("#store-list").val(),a=$("#cat-list").val(),console.log(a),console.log(t),n(l=1)}),$(".load-more-btn").on("click",function(){n(++l)})});
</script>
@endsection
