@extends('layouts.theme')
@section('main-content')
<!-- Page -->
<div class="forum-page-header mb-5" style="background: url('{{url('images/favicon/'.$setting->banner_img)}}'); background-position: center;background-size: cover; background-repeat: no-repeat;">
	  		<div class="container">
		        <div class="forum-page-heading-block">
		          <h2 class="forum-page-heading text-center">{{$pages->title}}</h2>
		        </div>
		    </div>
		</div>
	<section id="about" class="coupon-page-main-block">
		
			<div class="container about-us-page">
				<div class="coupon-dtl-outer">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="about-us-main-block page-block">
								<div class="about-section">
									{!! $pages->body !!}
								</div>
							</div>
						</div>
					<!--	<div class="col-lg-3 col-md-4">
							<div class="coupon-sidebar">
      					@include('includes.side-bar')
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- end forum -->
@endsection