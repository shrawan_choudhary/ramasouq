@extends('layouts.theme')
@section('main-content')



	<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
  <?php 
	 $sliderCount = count($slider);
	
	
	if(isset($slider)  && count($slider) > 0)
	{
	 
	
	for( $i=0;$i >= $sliderCount; $i++)
	{
		?>	<li data-target="#demo" data-slide-to="<?php echo $i ?>" class="<?php $i==0?'active':'' ?> "></li>  <?php
	}
	}	?>
	
	
 <!--   <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
	-->
  </ul>
  <div class="carousel-inner">
  <!--- && count($slider) > 0-->
 
  @if(isset($slider)  && count($slider) > 0)
	@foreach($slider as $key => $item)
	
  
    <div class="carousel-item active">
      <img src="{{asset('images/'.$item->image)}}" alt="Los Angeles" width="100%" height="400">
      <div class="carousel-caption">
        <h3>{{$item->heading}}</h3>
        <p>{{$item->subheading}}</p>
      </div>   
    </div>
  
   @endforeach
    @endif
  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
	
<!-- end slider -->



	<section  class="section">
	<div class="container">	
		
			
												<style>
													.padding-0{
														padding-right:0;
														padding-left:0;
													}
												</style>		
												
									<div class="blog-page-main-block" >
									<div class="blog-post-main border" >
										<div class="row  " style='margin: 5px 50px' style=''>
				@if(isset($settings) && $settings->is_category_block)
					@if(isset($category_list) && count($category_list) > 0)
						@foreach($category_list->take(12) as $key => $item)
								
													
													<div class="col-sm-3 padding-0" style='background-color:{{$item->color_code}};margin-bottom:7px;'>
														<div class="blog-post-dtl">
														<br><center>	<h6 class="blog-post-heading">
														<a href="" data-toggle="modal" data-target="#myModal" title="{{strtok($item->title,' ')}}" style='color:white'>{{strtok($item->title,' ')}}</a></h6>
															</center>
															<div class="blog-post-tags">
																
															</div>
															<div class="blog-post-text">
																<p></p>
															</div>
															<div class="blog-post-link">
																<br><center><a href='' data-toggle="modal" data-target="#myModal" title="View List" style='color:white'>View List</a></center>
															</div>
														</div>

													</div>
													<div class="col-sm-3 padding-0" style='margin-bottom:7px;'>
														<div class="blog-img"  style='padding-right:7px;'>
															<a href=""  data-toggle="modal" data-target="#myModal" title="{{strtok($item->title,' ')}}"><img style='height:180px;' src="images/category/{{$item->image}}" class="img-fluid" alt="Categories"></a>
														
</div>
													</div>
													
												
										
											
												@endforeach
		@endif										
		@endif										
											</div>
												
												
												
											</div>
										
																			
																					
																			</div>
								</div>						
								
						
		
		
	
	
	</section>


		
		
		
		
		
		


	
	
	
	
<!-- end deal -->
<!-- categories -->



<!--
@if(isset($settings) && $settings->is_category_block)
	<section id="categories" class="categories-main-block">
		<div class="container">			
			@if(isset($category_list) && count($category_list) > 0)
				<div class="section">
					<h4 class="section-heading">Categories</h4>
				</div>
				<div class="cat-block text-center">
					<div class="row">
						@foreach($category_list->take(12) as $key => $item)
							<div class="col-lg-2 col-md-4">
								<div class="category-block">
									<a href="{{url('category-dtl/'.$item->slug)}}" title="Categories">
										<div class="cat">
											<div class="{{$item->icon ? 'cat-icon' : ''}}">
												@if($item->icon)
													<i class="fa {{$item->icon}}"></i>
												@else
													<img src="{{asset('images/category/'.$item->image)}}" class="img-fluid" alt="category">
												@endif
											</div>
											<h5 class="cat-title">{{strtok($item->title,' ')}}</h5>
										</div>
									</a>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			@endif
		</div>
	</section>
@endif

-->


<!-- end featured stores -->


@endsection
@section('custom-scripts')
<script>
$(document).ready(function(){$(".cat-nav li").click(function(e){e.preventDefault();$(this).addClass("active"),$(this).parent().children("li").not(this).removeClass("active")});var e="all",t="all",a="all",l=1;function n(l){$.ajax({headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},type:"GET",url:"{{ url('homefilter') }}?page="+l,data:{filter:t,s_filter:e,c_filter:a,main:"0"},datatype:"html",beforeSend:function(){$(".load-more-btn").hide(),$(".ajax-loading").show()},success:function(e){console.log(e)},error:function(e,t,a){console.log(e)}}).done(function(e){if(!e)return console.log("no"),$(".ajax-loading").hide(),1==l&&$(".results").html("No Results Found!"),0;$(".ajax-loading").hide(),1==l?$(".results").html(e):$(".results").append(e),$(e).find(".deal-block").length>35&&$(".load-more-btn").show()}).fail(function(e,t,a){alert("We are facing some issues currenlty. Please try again later.")})}$(".home-filter li").on("click change keyup",function(){t=$(".cat-nav li.active").attr("id"),e=$("#store-list").val(),a=$("#cat-list").val(),console.log(a),console.log(t),n(l=1)}),$(".load-more-btn").on("click",function(){n(++l)})});
</script>
@endsection
