
<?php $__env->startSection('content'); ?>
  <div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="<?php echo e(url('admin/store')); ?>" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Edit Store</h4> 
    <?php echo Form::model($store, ['method' => 'PATCH', 'action' => ['StoreController@update', $store->id], 'files' => true]); ?>

      <div class="row admin-form-block z-depth-1">
        <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
              <?php echo Form::label('title', 'Store Name / Title'); ?>

              <?php echo Form::text('title', null, ['class' => 'form-control']); ?>

              <small class="text-danger"><?php echo e($errors->first('title')); ?></small>
          </div>     
          
		  <div class="form-group<?php echo e($errors->has('category_id') ? ' has-error' : ''); ?>">
              <?php echo Form::label('category_id', 'Select Category*'); ?> - <p class="inline info">Please select category</p>
              <?php echo Form::select('category_id', $all_category, null, ['class' => 'form-control select2', 'required']); ?>

              <small class="text-danger"><?php echo e($errors->first('category_id')); ?></small>
          </div> 
		  
  <div class="form-group<?php echo e($errors->has('opening_time') ? ' has-error' : ''); ?>">
              <?php echo Form::label('opening_time', 'Opening Time'); ?> 
              <?php echo Form::text('opening_time', null, ['class' => 'form-control']); ?>

              <small class="text-danger"><?php echo e($errors->first('opening_time')); ?></small>
          </div> 
		  <div class="form-group<?php echo e($errors->has('closing_time') ? ' has-error' : ''); ?>">
              <?php echo Form::label('closing_time', 'Closing Time'); ?> 
              <?php echo Form::text('closing_time', null, ['class' => 'form-control']); ?>

              <small class="text-danger"><?php echo e($errors->first('closing_time')); ?></small>
          </div> 

		  
          <div class="form-group<?php echo e($errors->has('image') ? ' has-error' : ''); ?> input-file-block">
            <?php echo Form::label('image', 'Store Logo / Image'); ?>

            <?php echo Form::file('image', ['class' => 'input-file', 'id'=>'image']); ?>

            <label for="image" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="Store Image">
              <i class="icon fa fa-check"></i>
              <span class="js-fileName">Choose a File</span>
            </label>
            <p class="info">Choose custom image</p>
            <small class="text-danger"><?php echo e($errors->first('image')); ?></small>
          </div>  
          <div class="form-group<?php echo e($errors->has('is_featured') ? ' has-error' : ''); ?> switch-main-block">
            <div class="row">
              <div class="col-xs-4">
                <?php echo Form::label('is_featured', 'Featured'); ?>

              </div>
              <div class="col-xs-5 pad-0">
                <label class="switch">                
                  <?php echo Form::checkbox('is_featured', null, null, ['class' => 'checkbox-switch']); ?>

                  <span class="slider round"></span>
                </label>
              </div>
            </div>
            <div class="col-xs-12">
              <small class="text-danger"><?php echo e($errors->first('is_featured')); ?></small>
            </div>
          </div> 
          <div class="form-group<?php echo e($errors->has('is_active') ? ' has-error' : ''); ?> switch-main-block">
            <div class="row">
              <div class="col-xs-4">
                <?php echo Form::label('is_active', 'Status'); ?>

              </div>
              <div class="col-xs-5 pad-0">
                <label class="switch">                
                  <?php echo Form::checkbox('is_active', null, null, ['class' => 'checkbox-switch']); ?>

                  <span class="slider round"></span>
                </label>
              </div>
            </div>
            <div class="col-xs-12">
              <small class="text-danger"><?php echo e($errors->first('is_active')); ?></small>
            </div>
          </div>        
          <div class="btn-group pull-right">
            <button type="submit" class="btn btn-success">Update</button>
          </div>
          <div class="clear-both"></div>
        </div> 


				 <div class="col-md-6">
			
		 
		  <div class="form-group<?php echo e($errors->has('mobile') ? ' has-error' : ''); ?>">
              <?php echo Form::label('mobile', 'Mobile No'); ?> 
              <?php echo Form::text('mobile', null, ['class' => 'form-control']); ?>

              <small class="text-danger"><?php echo e($errors->first('mobile')); ?></small>
          </div> 
		 
		  <div class="form-group<?php echo e($errors->has('mobile') ? ' has-error' : ''); ?>">
              <?php echo Form::label('address', 'Address'); ?> 
              <?php echo Form::text('address', null, ['class' => 'form-control']); ?>

              <small class="text-danger"><?php echo e($errors->first('address')); ?></small>
          </div> 
		   
		  
		 
		  <div class="form-group<?php echo e($errors->has('merchant_name') ? ' has-error' : ''); ?>">
              <?php echo Form::label('merchant_name', 'Merchant Name'); ?> 
              <?php echo Form::text('merchant_name', null, ['class' => 'form-control']); ?>

              <small class="text-danger"><?php echo e($errors->first('merchant_name')); ?></small>
          </div> 
		
			<div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
              <?php echo Form::label('email', 'Email'); ?> 
              <?php echo Form::text('email', null, ['class' => 'form-control']); ?>

              <small class="text-danger"><?php echo e($errors->first('email')); ?></small>
          </div> 
		 
		 <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
              <?php echo Form::label('password', 'Password'); ?> 
              <?php echo Form::password('password', null, ['class' => 'form-control']); ?>

              <small class="text-danger"><?php echo e($errors->first('password')); ?></small>
          </div> 
		 
		 
		 
		 </div>
		

		
      </div>
    <?php echo Form::close(); ?>

  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>