<?php
  $category =  App\Category::latest()->paginate();
?>
<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 2018 .
**********************************************************************************************************  -->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]> -->
<html lang="en">
<!-- <![endif]-->
<!-- head -->

<head>
<title><?php echo e($settings->w_title ? $settings->w_title : ''); ?></title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="<?php echo e($settings->desc ? $settings->desc : ''); ?>" />
<meta name="keywords" content="<?php echo e($settings->keywords ? $settings->keywords : ''); ?>">
<meta name="author" content="Media City" />
<meta name="MobileOptimized" content="320" />
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<link rel="icon" type="image/icon" href="<?php echo e(asset('images/favicon/'.$settings->favicon)); ?>"> 
<!-- favicon-icon -->
<!-- theme styles -->
<link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css"/> 
<!-- bootstrap css -->
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/fontawesome/css/fontawesome-all.min.css')); ?>"/> 
<!-- fontawesome css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/font-awesome.min.css')); ?>"/> 
<!-- fontawesome css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/flaticon/flaticon.css')); ?>"/> <!-- flaticon css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/owl/css/owl.carousel.min.css')); ?>"/> 
<!-- owl carousel css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/datatables/css/responsive.datatables.min.css')); ?>"/> 
<!-- datatables responsive -->
<link href="<?php echo e(asset('css/jquery.rateyo.css')); ?>" rel="stylesheet" type="text/css"/> 
<!-- rateyo css -->
<link href="<?php echo e(asset('vendor/datepicker/datepicker.css')); ?>" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css"/> <!-- summernote css -->
<link href="<?php echo e(asset('css/summernote-bs4.css')); ?>" rel="stylesheet" type="text/css" />
<!-- summernote css -->
<link href="<?php echo e(asset('css/select2.css')); ?>" rel="stylesheet" type="text/css"/> 
<!-- select css -->
<link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet" type="text/css"/> 
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<!-- custom css -->

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<!-- jquery library js -->
<script>
  window.Laravel =  <?php echo json_encode([
      'csrfToken' => csrf_token(),
  ]); ?>
</script>
<script>
	$( document ).ready(function() {
		<?php if(Route::currentRouteName() != 'register' && Route::currentRouteName() != 'login' && (count($errors) > 0) && ($errors->has('email1') || $errors->has('password1'))): ?> 
    	$('#register').modal('show');
		<?php elseif((Route::currentRouteName() != 'login' && Route::currentRouteName() != 'register') && (count($errors) > 0) && (!empty(Session::get('error_code')) && Session::get('error_code') == 5) || ($errors->has('email') || $errors->has('password'))): ?>
    	$('#login').modal('show');
    <?php endif; ?>
	});
</script>
<!-- end theme styles -->
</head>
<!-- end head -->
<!-- body start-->
<body>
	<div>
		<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
	<?php if($settings->preloader == 1): ?>
		<!-- preloader --> 
	  <div class="preloader">
	      <div class="status">
	          <div class="status-message">
	          </div>
	      </div>
	  </div>
	<?php endif; ?>
  <!-- end preloader -->
  <!-- topbar -->
	<!--<section id="top-bar" class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-4 d-none d-sm-block">
				<!--	<?php if(isset($social) && count($social)>0): ?>
						<div class="social-icon">
							<ul>
								<?php $__currentLoopData = $social; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li class="<?php echo e(strtolower($item->title)); ?>-icon"><a href="<?php echo e($item->url); ?>" target="_blank" title="<?php echo e($item->title); ?>"><i class="<?php echo e($item->icon); ?>"></i></a></li>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					<?php endif; ?> -->
		<!--		</div>
				
				<div class="col-md-6 col-sm-8">
					<div class="top-nav">
						<ul>
							
							<?php if(isset($settings->w_email)): ?>
								<li><a href="#" class="user-acc" title="Email"><i class="far fa-user"></i>Mail us : <?php echo e($settings->w_email); ?></a></li>
							<?php endif; ?>	
							
							<?php if(isset($settings->w_phone)): ?>
								<li><a href="#" class="user-mobile" title="Contact No"><i class="fa fa-phone"></i>Contact us : <?php echo e($settings->w_phone); ?></a></li>
							<?php endif; ?>	
								
							
						</ul>
					</div>
				</div>
				
				
			</div>
		</div>
		
		<!-- search -->
	<!--	<div class="search">
			<div class="container clearfix">
				<?php echo Form::open(['method' => 'GET', 'action' => 'SearchController@homeSearch', 'class' => 'forum-search']); ?>

					<input type="search" name="search" class="search-box" placeholder="Type anything here...." />
					<a href="#" class="fa fa-times search-close"></a>
				<?php echo Form::close(); ?>

			</div>
		</div>
		-->
		
		<!-- end search -->
	
	<!--
	</section>
	-->
	
	
	<!--Category Modal End-->
	
	<!--
	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
 <!--   <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Download App</h4>
      </div>
      <div class="modal-body">
        <p align='center'>
		
		 <?php if(isset($settings) && $settings->is_playstore): ?>
			 <div class='row'>
		            <div class="app-badge play-badge col-sm-6">
		            	<a href="<?php echo e($settings->playstore_link); ?>" target="_blank" title="Google Play">
					<img style='height:80px' src="<?php echo e(asset('images/google-play.png')); ?>" class="img-fluid" alt="Google Play"></a>
		            </div>
		          <?php endif; ?>
		          <?php if(isset($settings) && $settings->is_app_icon): ?>
		            <div class="app-badge col-sm-6">
		            	<a href="<?php echo e($settings->app_link); ?>" target="_blank" title="Apple App Store">
						<img  style='height:80px' src="<?php echo e(asset('images/app-store.png')); ?>" class="img-fluid" alt="Apple App Store"></a>
		            </div>
		          <?php endif; ?>
			</div>
		
		</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	-->
	
	
	<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Download App</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
                <p align='center'>
		
		 <?php if(isset($settings) && $settings->is_playstore): ?>
			 <div class='row'>
		            <div class="app-badge play-badge col-sm-6">
		            	<a href="<?php echo e($settings->playstore_link); ?>" target="_blank" title="Google Play">
					<img style='height:80px' src="<?php echo e(asset('images/google-play.png')); ?>" class="img-fluid" alt="Google Play"></a>
		            </div>
		          <?php endif; ?>
		          <?php if(isset($settings) && $settings->is_app_icon): ?>
		            <div class="app-badge col-sm-6">
		            	<a href="<?php echo e($settings->app_link); ?>" target="_blank" title="Apple App Store">
						<img  style='height:80px' src="<?php echo e(asset('images/app-store.png')); ?>" class="img-fluid" alt="Apple App Store"></a>
		            </div>
		          <?php endif; ?>
			</div>
		
		</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
	
	
	
	<!---Category Modal end--->
	
	
	<!-- end topbar -->
	<!-- login -->
	
	<!-- end login -->
	<!-- register -->

	<!-- end register -->
	<!-- logo -->
	<section class="logo-block">
		<div class="container">
			<div class="row">
				<div class="col-6 col-lg-6 col-md-6 pr-0">
				

				<div class="logo">
						<?php if($settings->logo != Null): ?>
							<a href="<?php echo e(url('/')); ?>" title="Home">
						<img src="<?php echo e(asset('images/'.$settings->logo)); ?>" class="img-fluid" alt="Logo"></a>
						<?php else: ?>
							<h2 class="logo-title"><?php echo e($settings->w_name ? $settings->w_name : 'Logo'); ?></h2>
						<?php endif; ?>
				<!--	<ul align='center'>
							
							<?php if(isset($settings->w_email)): ?>
								<li><a style='color:black;cursor:pointer' href="#" class="user-acc" title="Email"><i class="far fa-user"></i>Mail us : <?php echo e($settings->w_email); ?></a></li>
							<?php endif; ?>	
							
							<?php if(isset($settings->w_phone)): ?>
								<li><a style='color:black;cursor:pointer' class="user-mobile" title="Contact No"><i class="fa fa-phone"></i>Contact us : <?php echo e($settings->w_phone); ?></a></li>
							<?php endif; ?>	
								
							
						</ul>	
						-->
					</div>
					
				</div>	
				<div class="col-6 col-lg-6 col-md-6 pl-0">
				    <div class="float-right">
					<div class="top-nav display-n float-left" style="text-transform: lowercase;">
						<ul class="">						
							<?php if(isset($settings->w_email)): ?>
								<li><a  style='color:black;' href="#" class="user-acc" title="Email"><i class="far fa-envelope"></i><span class="display-n"></span> <?php echo e($settings->w_email); ?></a></li>
							<?php endif; ?>	
							
							<?php if(isset($settings->w_phone)): ?>
								<li><a  style='color:black;' href="#" class="user-mobile" title="Contact No"><i class="fa fa-phone"></i><span class="display-n"></span> <?php echo e($settings->w_phone); ?></a></li>
							<?php endif; ?>	
						</ul>
					</div>
					<?php if(isset($social) && count($social)>0): ?>
						<div class="social-icon float-left">
							<ul style='float:right'>
								<?php $__currentLoopData = $social; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li class="<?php echo e(strtolower($item->title)); ?>-icon"><a href="<?php echo e($item->url); ?>" target="_blank" title="<?php echo e($item->title); ?>"><i class="<?php echo e($item->icon); ?>"></i></a></li>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					<?php endif; ?>
					</div>
				</div>	

					
				
				
			</div>
		</div>
	</section>
	<!-- end logo -->
	
<!---   height: 64px !important; --->
	</style>
	<div id="site-header" style="    position: sticky;top: 0;z-index: 99;">
	<section class="navbar mb-0 pb-0" >
		<div class="container">
			<nav class="navbar navbar-expand-lg mb-0">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="position: absolute; top: 0;font-size: 25px;-webkit-box-shadow: 0px 6px 10px -5px rgba(0,0,0,0.75);">
					<span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
				</button>
				<div class="collapse navbar-collapse pl-0 pr-0 mr-0" id="navbarSupportedContent">
					
					<ul class="navbar-nav p-0 m-0">
			      <li class="nav-item" align=''>
			        <a class="nav-link <?php echo e(Nav::isRoute('home')); ?>" href="<?php echo e(url('/')); ?>">Home</a>
			      </li>
			  
				  
			      <li class="nav-item">
			        <a class="nav-link <?php echo e(Nav::isRoute('about-us')); ?>" href="<?php echo e(url('about-us')); ?>">About Us</a>
			      </li>
				  
				  <li class="nav-item">
			        <a class="nav-link <?php echo e(Nav::isRoute('contact')); ?>" href="<?php echo e(url('contact')); ?>">Contact Us</a>
			      </li>

				  <li class="nav-item">
			        <a class="nav-link <?php echo e(Nav::isRoute('terms-and-conditions')); ?>" href="<?php echo e(url('terms-and-conditions')); ?>">Terms & Conditions</a>
			      </li>

				  <li class="nav-item">
			        <a class="nav-link <?php echo e(Nav::isRoute('faq')); ?>" href="<?php echo e(url('faq')); ?>">FAQ</a>
			      </li>
			    </ul>
				
			  </div>
			</nav>
		</div>
	</section>
</div>

	
	
	
	<!-- end navbar -->	
	<?php echo $__env->yieldContent('main-content'); ?>



	<!-- footer start -->
	
	<div id="back-what-app">
  <a title="what app" href="https://api.whatsapp.com/send?phone=+91-<?php echo e($settings->whatsapp_number); ?>&text=<?php echo e($settings->whatsapp_msg); ?>" target="_blank">
    <i class="fa fa-whatsapp" aria-hidden="true"></i>
  </a>
</div>
	
	

	<footer id="footer" class="footer-main-block" style="background: #00aeefb8">
	  <div style="height: 0px">
	  	<a id="back2Top" title="Back to top" href="#">&#10148;</a>
	  </div>
		<?php if($settings->footer_layout == 1): ?>
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading"><?php echo e($settings->f_title1); ?></h6>
	            <?php if(isset($f_menu)): ?>	
								<ul>
									<?php $__currentLoopData = $f_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>														
		            		<?php if($item->widget == '1'): ?>
															
											<li><a style='color:black;' href="<?php echo e(url($item->slug)); ?>" title="<?php echo e($item->title); ?>"><?php echo e($item->title); ?></a></li>
							
							<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<li><a  style='color:black;' href="<?php echo e(url('/contact')); ?>" title="contact us">Contact Us</a></li>
								</ul>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading"><?php echo e($settings->f_title2); ?></h6>
							<?php if(isset($f_menu)): ?>	
								<ul>
									<?php $__currentLoopData = $f_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>														
		            		<?php if($item->widget == '2'): ?>	
								
											<li><a  style='color:black;' href="<?php echo e(url($item->slug)); ?>" title="<?php echo e($item->title); ?>"><?php echo e($item->title); ?></a></li>
										
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									
								</ul>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">ADVERTISE / SELL</h6>
							
								<ul>
								
		            						<li><a style='color:black;' href="<?php echo e(url('/sell')); ?>">Sell On Rama Souq</a></li>	
											<li><a  style='color:black;' href="<?php echo e(url('/grow-bussiness')); ?>">Growyour Business</a></li>
											<?php $__currentLoopData = $f_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>														
		            		<?php if($item->widget == '4'): ?>
											<li><a  style='color:black;'href="<?php echo e(url($item->slug)); ?>"><?php echo e($item->title); ?></a></li>
											<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							
						</div>
					</div>
					<div class="laptop-non text-left">
					<div class="col-12">
						
					<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading">Contact :</h6>
							<ul>						
							<?php if(isset($settings->w_email)): ?>
								<li><a  style='color:black;' href="#" class="" title="Email"><i class="far fa-envelope"></i><span class="display-n">Mail us </span>: <?php echo e($settings->w_email); ?></a></li>
							<?php endif; ?>	
							
							<?php if(isset($settings->w_phone)): ?>
								<li><a  style='color:black;' href="#" class="" title="Contact No"><i class="fa fa-phone"></i><span class="display-n">Contact us </span>: <?php echo e($settings->w_phone); ?></a></li>
							<?php endif; ?>	
							</ul>
						</div>
					</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="footer-widget footer-subscribe">				
							<h6 class="footer-widget-heading" ><?php echo e($settings->f_title4); ?></h6>
							<?php if(isset($settings) && $settings->is_mailchimp): ?>		
								<p  style='color:black;'><?php echo e($settings->m_text); ?></p>
								<?php echo Form::open(['method' => 'POST', 'action' => 'EmailSubscribeController@subscribe', 'id' => 'subscribe-form', 'class' => 'subscribe-form ']); ?>

	              	<?php echo e(csrf_field()); ?>

									<div class="row no-gutters">
										<div class="col-9 col-md-9">
											<div class="form-group">
				                <label class="sr-only">Your Email address</label>
				                <input type="email" class="form-control" id="mc-email" placeholder="Enter email address" style="border-radius: 0;">
				              </div>
										</div>
										<div class="col-3 col-md-3">
											<button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
			              	<label for="mc-email"></label>
										</div>
									</div>
	              <?php echo Form::close(); ?>

	            <?php endif; ?>
	            <div class="row">
	            <?php if(isset($settings) && $settings->is_playstore): ?>
		            <div class="col-6 col-lg-6 app-badge play-badge">
		            	<a href="<?php echo e($settings->playstore_link); ?>" target="_blank" title="Google Play"><img src="<?php echo e(asset('images/google-play.png')); ?>" class="img-fluid" alt="Google Play" style="height:40px; width:100%;"></a>
		            </div>
		          <?php endif; ?>
		          <?php if(isset($settings) && $settings->is_app_icon): ?>
		            <div class="col-6 col-lg-6 app-badge">
		            	<a href="<?php echo e($settings->app_link); ?>" target="_blank" title="Apple App Store"><img src="<?php echo e(asset('images/app-store.png')); ?>" class="img-fluid" alt="Apple App Store" style="height:40px; width:100%;"></a>
		            </div>
		          <?php endif; ?>
		      	</div>
						</div>
					</div>
				</div>
				<div class="border-divider">
				</div>
				<div class="copyright">
					<div class="row">
						<div class="col-md-6">
							<div class="copyright-text">
				  			<p  style='color:black;'>&copy; <?php echo date("Y"); ?><a  style='color:black;' href="<?php echo e(url('/')); ?>" title="<?php echo e($settings->w_name); ?>"> <?php echo e($settings->w_name); ?></a> | <?php echo e($settings->copyright); ?></p>
			        </div>
						</div>
						<div class="col-md-6">
							<?php if(isset($social) && count($social)>0): ?>
							<div class="social-icon">
								<ul>
									<?php $__currentLoopData = $social; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<li class="<?php echo e(strtolower($item->title)); ?>-icon"><a href="<?php echo e($item->url); ?>" target="_blank" title="<?php echo e($item->title); ?>"><i class="<?php echo e($item->icon); ?>"></i></a></li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						<?php endif; ?>
						</div>
					</div>
		    </div>
			</div>
		<?php else: ?>
			<div class="container footer2 ">
				<div class="row">
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget footer-subscribe">
							<div class="logo">
								<?php if($settings->footer_logo != Null): ?>
									<a href="<?php echo e(url('/')); ?>" title="Home"><img src="<?php echo e(asset('images/'.$settings->footer_logo)); ?>" class="img-fluid" alt="Footer Logo"></a>
								<?php else: ?>
									<h2 class="logo-title" style="color:#FFF;"><?php echo e($settings->w_name ? $settings->w_name : 'Logo'); ?></h2>
								<?php endif; ?>
							</div>
							<p><?php echo e($settings->footer_text ? $settings->footer_text : ''); ?></p>
							<?php if(isset($settings) && $settings->is_mailchimp): ?>
								<p><?php echo e($settings->m_text); ?></p>	
								<?php echo Form::open(['method' => 'POST', 'action' => 'EmailSubscribeController@subscribe', 'id' => 'subscribe-form', 'class' => 'subscribe-form']); ?>

	              	<?php echo e(csrf_field()); ?>

									<div class="row no-gutters">
										<div class="col-md-9">
											<div class="form-group">
				                <label class="sr-only">Your Email address</label>
				                <input type="email" class="form-control" id="mc-email" placeholder="Enter email address">
				              </div>
										</div>
										<div class="col-md-3">
											<button style='height:33px;' type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
			              	<label for="mc-email"></label>
										</div>
									</div>
	              <?php echo Form::close(); ?>	
	            <?php endif; ?>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading"><?php echo e($settings->f_title2); ?></h6>
							<?php if(isset($f_menu)): ?>	
								<ul>
									<?php $__currentLoopData = $f_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>														
		            		<?php if($item->widget == '2'): ?>
		            		<?php if($item->id != '12'): ?>
		            		<?php if($item->id != '13'): ?>
							<?php if($item->id != '11'): ?>				
											<li><a href="<?php echo e(url($item->slug)); ?>" target="_blank" title="<?php echo e($item->title); ?>"><?php echo e($item->title); ?></a></li>
										<?php endif; ?>
										<?php endif; ?>
										<?php endif; ?>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget link-widget">
							<h6 class="footer-widget-heading"><?php echo e($settings->f_title3); ?></h6>
							<?php if(isset($f_menu)): ?>	
								<ul>
									<?php $__currentLoopData = $f_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>														
		            		<?php if($item->widget == '3'): ?>	
											<li><a href="<?php echo e(url($item->slug)); ?>" target="_blank" title="<?php echo e($item->title); ?>"><?php echo e($item->title); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="footer-widget app-widget">
							<h6 class="footer-widget-heading"><?php echo e($settings->f_title4); ?></h6>
							<?php if($settings->w_address): ?>
								<ul class="contact-widget-dtl">                   
								  <li><i class="fas fa-map-marker"></i></li>
								  <li><?php echo e($settings->w_address); ?></li>
								</ul>
							<?php endif; ?>
							<?php if($settings->w_address): ?>
								<ul class="contact-widget-dtl">  
								  <li><i class="fas fa-phone"></i></li>		
								  <li><a href="tel:<?php echo e($settings->w_phone); ?>"><?php echo e($settings->w_phone); ?></a></li>
								</ul>
							<?php endif; ?>
							<?php if($settings->w_address): ?>
								<ul class="contact-widget-dtl">  
								  <li><i class="fas fa-envelope"></i></li>
								  <li><a href="mailto:<?php echo e($settings->w_email); ?>?Subject=Hello%20again" target="_top"><?php echo e($settings->w_email); ?></a></li>	
								</ul>
							<?php endif; ?>	
							<?php if($settings->w_time): ?>
								<ul class="contact-widget-dtl">  
								  <li><i class="fas fa-clock"></i></li>
								  <li><?php echo e($settings->w_time); ?></li>	
								</ul>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="border-divider">
				</div>
				<div class="copyright">
					<div class="row">
						<div class="col-md-4">
							<div class="copyright-text">
				  			<p>&copy; <?php echo date("Y"); ?><a href="<?php echo e(url('/')); ?>" title="<?php echo e($settings->w_name); ?>"> <?php echo e($settings->w_name); ?></a> | <?php echo e($settings->copyright); ?></p>
			        </div>
						</div>
						<div class="col-md-4">
							<div class="footer2-icon text-center">
								<ul>
									<li>
										<?php if(isset($settings) && $settings->is_playstore): ?>
					            <div class="app-badge play-badge">
					            	<a href="<?php echo e($settings->playstore_link); ?>" target="_blank" title="Google Play"><img src="<?php echo e(asset('images/google-play.png')); ?>" class="img-fluid" alt="Google Play"></a>
					            </div>
					          <?php endif; ?>
					        </li>
					        <li>
					          <?php if(isset($settings) && $settings->is_app_icon): ?>
					            <div class="app-badge">
					            	<a href="<?php echo e($settings->app_link); ?>" target="_blank" title="Apple App Store"><img src="<?php echo e(asset('images/app-store.png')); ?>" class="img-fluid" alt="Apple App Store"></a>
					            </div>
					          <?php endif; ?>
					        </li>
					      </ul>
			        </div>
			      </div>
						<div class="col-md-4">
							<?php if(isset($social) && count($social)>0): ?>
							<div class="social-icon">
								<ul>
									<?php $__currentLoopData = $social; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<li class="<?php echo e(strtolower($item->title)); ?>-icon"><a href="<?php echo e($item->url); ?>" target="_blank" title="<?php echo e($item->title); ?>"><i class="<?php echo e($item->icon); ?>"></i></a></li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						<?php endif; ?>
						</div>
					</div>
		    </div>
			</div>
		<?php endif; ?>
	</footer>
	
	
	<!-- footer end -->
<!-- jquery -->
<script src="<?php echo e(asset('js/bootstrap.bundle.min.js')); ?>"></script> 
<!-- bootstrap js -->
<script src="<?php echo e(asset('js/select2.js')); ?>"></script> 
<!-- select2 js --> 
<script src="<?php echo e(asset('vendor/owl/js/owl.carousel.min.js')); ?>"></script> 
<!-- owl carousel js -->
<script src="<?php echo e(asset('vendor/mailchimp/jquery.ajaxchimp.min.js')); ?>"></script> 
<!-- mailchimp js -->
<script src="<?php echo e(asset('vendor/datepicker/bootstrap-datepicker.js')); ?>"></script>
<!-- bootstrap datepicker js-->
<script src="<?php echo e(asset('vendor/datatables/js/jquery.datatables.min.js')); ?>"></script> 
<!-- datatables bootstrap js -->		
<script src="<?php echo e(asset('vendor/datatables/js/datatables.responsive.min.js')); ?>"></script> <!-- datatables bootstrap js -->		
<script src="<?php echo e(asset('vendor/datatables/js/datatables.min.js')); ?>"></script> 
<!-- datatables bootstrap js -->
<script src="<?php echo e(asset('vendor/summernote/js/summernote-bs4.min.js')); ?>"></script>
<!-- summernote js -->
<script src="<?php echo e(asset('vendor/clipboard/js/clipboard.min.js')); ?>"></script>
<!-- clipboard js -->
<script src="<?php echo e(asset('js/jquery.rateyo.js')); ?>"></script> 
<!-- Rateyo js --> 
<script src="<?php echo e(asset('js/theme.js')); ?>"></script> 
<script src="<?php echo e(asset('js/ajax.js')); ?>"></script> 
<!-- custom js -->
<?php echo $__env->yieldContent('custom-scripts'); ?>
<script>
$(document).ready(function(){$(".grab-now").click(function(){var n=$(this).data("id");console.log(n),$.ajax({headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},type:"GET",url:"<?php echo e(url('counter')); ?>",data:{id:n},error:function(n,o,t){console.log(n)}})})});
</script>
<?php if($settings->right_click == 1): ?>
  <script type="text/javascript" language="javascript">
   // Right click disable 
    $(function() {
	    $(this).bind("contextmenu", function(inspect) {
	    	inspect.preventDefault();
	    });
    });
      // End Right click disable 
  </script>
<?php endif; ?>
<!-- <?php if($settings->inspect == 1): ?>
<script type="text/javascript" language="javascript">
//all controller is disable 
  $(function() {
	  var isCtrl = false;
	  document.onkeyup=function(e){
		  if(e.which == 17) isCtrl=false;
		}
		document.onkeydown=function(e){
		  if(e.which == 17) isCtrl=true;
		  if(e.which == 85 && isCtrl == true) {
			  return false;
			}
	  };
    $(document).keydown(function (event) {
      if (event.keyCode == 123) { // Prevent F12
        return false;
  		} 
      else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I
	     	return false;
	   	}
 		});  
	});
  // end all controller is disable 
 </script>
<?php endif; ?> -->
<?php if($settings->is_gotop==1): ?>
	<script type="text/javascript">
	 //Go to top
	$(window).scroll(function() {
	  var height = $(window).scrollTop();
	  if (height > 100) {
	      $('#back2Top').fadeIn();
	  } else {
	      $('#back2Top').fadeOut();
	  }
	});
	$(document).ready(function() {
	  $("#back2Top").click(function(event) {
	      event.preventDefault();
	      $("html, body").animate({ scrollTop: 0 }, "slow");
	      return false;
	  });
	});
	// end go to top 
	
	</script>
<?php endif; ?>
<script>
	$(window).on("scroll", function () {
      var scroll = $(window).scrollTop();

      // if (scroll > 120) {
      //   $("#site-header").addClass("nav-fixed");
      // } else {
      //   $("#site-header").removeClass("nav-fixed");
      // }
      
    });
</script>

<script>
  $('#states').change(function(){
  var stateID = $(this).val();  
  if(stateID){
    $.ajax({
      type:"GET",
      url:"<?php echo e(url('ajax/city')); ?>?parent="+stateID,
      success:function(res){        
      if(res){
        $("#city").empty();
        $("#city").append('<option  selected disabled>Select</option>');
        $.each(res,function(key,value){
          $("#city").append('<option  value="'+key+'">'+value+'</option>');
        });
      
      } else {
        $("#city").empty();
      }
      }
    });
  }else{
    $("#city").empty();
  }   
  });
  // $('#city').on('change',function(){
  // var cityID = $(this).val();  
  // if(cityID){
  //   $.ajax({
  //     type:"GET",
  //     url:"<?php echo e(url('city')); ?>?city="+cityID,
  //     success:function(res){        
  //     if(res){
  //       $("#city").empty();
  //       $.each(res,function(key,value){
  //         $("#city").append('<option value="'+key+'">'+value+'</option>');
  //       });
      
  //     }else{
  //       $("#city").empty();
  //     }
  //     }
  //   });
  // }else{
  //   $("#city").empty();
  // }
    
  // });
</script>
<script type="text/javascript">
	function submitForm() {
			   // Get the first form with the name
			   // Usually the form name is not repeated
			   // but duplicate names are possible in HTML
			   // Therefore to work around the issue, enforce the correct index
			   var frm = document.getElementsByName('contact-form')[0];
			   frm.submit(); // Submit the form
			   frm.reset();  // Reset all form data
			   return false; // Prevent page refresh
			}
</script>
<!-- Add Qulink script Here -->
<!-- end jquery -->
</body>	
<!-- body end -->
</html>

