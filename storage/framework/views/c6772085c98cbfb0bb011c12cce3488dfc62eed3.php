<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 2018 .
**********************************************************************************************************  -->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]> -->
<html lang="en">
<!-- <![endif]-->
<!-- head -->

<head>
<title><?php echo e($settings->w_title ? $settings->w_title : ''); ?></title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="<?php echo e($settings->desc ? $settings->desc : ''); ?>" />
<meta name="keywords" content="<?php echo e($settings->keywords ? $settings->keywords : ''); ?>">
<meta name="author" content="Media City" />
<meta name="MobileOptimized" content="320" />
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<link rel="icon" type="image/icon" href="<?php echo e(asset('images/favicon/'.$settings->favicon)); ?>"> 
<!-- favicon-icon -->
<!-- theme styles -->
<link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css"/> 
<!-- bootstrap css -->
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/fontawesome/css/fontawesome-all.min.css')); ?>"/> 
<!-- fontawesome css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/font-awesome.min.css')); ?>"/> 
<!-- fontawesome css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/flaticon/flaticon.css')); ?>"/> <!-- flaticon css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/owl/css/owl.carousel.min.css')); ?>"/> 
<!-- owl carousel css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/datatables/css/responsive.datatables.min.css')); ?>"/> 
<!-- datatables responsive -->
<link href="<?php echo e(asset('css/jquery.rateyo.css')); ?>" rel="stylesheet" type="text/css"/> 
<!-- rateyo css -->
<link href="<?php echo e(asset('vendor/datepicker/datepicker.css')); ?>" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css"/> <!-- summernote css -->
<link href="<?php echo e(asset('css/summernote-bs4.css')); ?>" rel="stylesheet" type="text/css" />
<!-- summernote css -->
<link href="<?php echo e(asset('css/select2.css')); ?>" rel="stylesheet" type="text/css"/> 
<!-- select css -->
<link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet" type="text/css"/> 
<!-- custom css -->

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<!-- jquery library js -->
<script>
  window.Laravel =  <?php echo json_encode([
      'csrfToken' => csrf_token(),
  ]); ?>
</script>
<script>
	$( document ).ready(function() {
		<?php if(Route::currentRouteName() != 'register' && Route::currentRouteName() != 'login' && (count($errors) > 0) && ($errors->has('email1') || $errors->has('password1'))): ?> 
    	$('#register').modal('show');
		<?php elseif((Route::currentRouteName() != 'login' && Route::currentRouteName() != 'register') && (count($errors) > 0) && (!empty(Session::get('error_code')) && Session::get('error_code') == 5) || ($errors->has('email') || $errors->has('password'))): ?>
    	$('#login').modal('show');
    <?php endif; ?>
	});
</script>
<!-- end theme styles -->
</head>
<!-- end head -->
<!-- body start-->
<body style="background: #aaa">
	<div>
		<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
	<?php if($settings->preloader == 1): ?>
		<!-- preloader --> 
	  <div class="preloader">
	      <div class="status">
	          <div class="status-message">
	          </div>
	      </div>
	  </div>
	<?php endif; ?>

	<style>
		.navbar {
   padding: 0 !important;
   margin: 0 !important;

}
<!---   height: 64px !important; --->
	</style>
	

	
	
	<!-- end navbar -->	
	<?php echo $__env->yieldContent('main-content'); ?>



	<!-- footer start -->
	
	
	
	
	</body>
	
	
	<!-- footer end -->
<!-- jquery -->
<script src="<?php echo e(asset('js/bootstrap.bundle.min.js')); ?>"></script> 
<!-- bootstrap js -->
<script src="<?php echo e(asset('js/select2.js')); ?>"></script> 
<!-- select2 js --> 
<script src="<?php echo e(asset('vendor/owl/js/owl.carousel.min.js')); ?>"></script> 
<!-- owl carousel js -->
<script src="<?php echo e(asset('vendor/mailchimp/jquery.ajaxchimp.min.js')); ?>"></script> 
<!-- mailchimp js -->
<script src="<?php echo e(asset('vendor/datepicker/bootstrap-datepicker.js')); ?>"></script>
<!-- bootstrap datepicker js-->
<script src="<?php echo e(asset('vendor/datatables/js/jquery.datatables.min.js')); ?>"></script> 
<!-- datatables bootstrap js -->		
<script src="<?php echo e(asset('vendor/datatables/js/datatables.responsive.min.js')); ?>"></script> <!-- datatables bootstrap js -->		
<script src="<?php echo e(asset('vendor/datatables/js/datatables.min.js')); ?>"></script> 
<!-- datatables bootstrap js -->
<script src="<?php echo e(asset('vendor/summernote/js/summernote-bs4.min.js')); ?>"></script>
<!-- summernote js -->
<script src="<?php echo e(asset('vendor/clipboard/js/clipboard.min.js')); ?>"></script>
<!-- clipboard js -->
<script src="<?php echo e(asset('js/jquery.rateyo.js')); ?>"></script> 
<!-- Rateyo js --> 
<script src="<?php echo e(asset('js/theme.js')); ?>"></script> 
<!-- custom js -->
<?php echo $__env->yieldContent('custom-scripts'); ?>
<script>
$(document).ready(function(){$(".grab-now").click(function(){var n=$(this).data("id");console.log(n),$.ajax({headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},type:"GET",url:"<?php echo e(url('counter')); ?>",data:{id:n},error:function(n,o,t){console.log(n)}})})});
</script>
<?php if($settings->right_click == 1): ?>
  <script type="text/javascript" language="javascript">
   // Right click disable 
    $(function() {
	    $(this).bind("contextmenu", function(inspect) {
	    	inspect.preventDefault();
	    });
    });
      // End Right click disable 
  </script>
<?php endif; ?>
<?php if($settings->inspect == 1): ?>
<script type="text/javascript" language="javascript">
//all controller is disable 
  $(function() {
	  var isCtrl = false;
	  document.onkeyup=function(e){
		  if(e.which == 17) isCtrl=false;
		}
		document.onkeydown=function(e){
		  if(e.which == 17) isCtrl=true;
		  if(e.which == 85 && isCtrl == true) {
			  return false;
			}
	  };
    $(document).keydown(function (event) {
      if (event.keyCode == 123) { // Prevent F12
        return false;
  		} 
      else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I
	     	return false;
	   	}
 		});  
	});
  // end all controller is disable 
 </script>
<?php endif; ?>
<?php if($settings->is_gotop==1): ?>
	<script type="text/javascript">
	 //Go to top
	$(window).scroll(function() {
	  var height = $(window).scrollTop();
	  if (height > 100) {
	      $('#back2Top').fadeIn();
	  } else {
	      $('#back2Top').fadeOut();
	  }
	});
	$(document).ready(function() {
	  $("#back2Top").click(function(event) {
	      event.preventDefault();
	      $("html, body").animate({ scrollTop: 0 }, "slow");
	      return false;
	  });
	});
	// end go to top 
	</script>
<?php endif; ?>
<!-- Add Qulink script Here -->
<!-- end jquery -->
</body>	
<!-- body end -->
</html>

