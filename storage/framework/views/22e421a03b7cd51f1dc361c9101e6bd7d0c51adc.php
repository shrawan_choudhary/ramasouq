<?php $__env->startSection('main-content'); ?>
	<!-- Login -->
	<section id="forum" class="coupon-page-main-block" style='height:100%;'>
		<div class="container">
			
			<!-- breadcrumb end -->
			<div class="forum-page-header" style="
  position: fixed; /* or absolute */
  top: 50%;
  left: 50%;
  /* bring your own prefixes */
  transform: translate(-50%, -50%);
}">
				<div class="row">
					<div class="offset-md-2 col-md-8">
						<div class="login-page-form">
							<div class="forum-page-heading-block">
							<center>	<h5 class="forum-page-heading">Admin Login</h5> </center>
							<center>
							<img style="height:80px;width: 100%;" src="<?php echo e(asset('images/'.$settings->logo)); ?>" class="img-fluid" alt="Logo">
							</center>
							</div>
							<form class="login-form" method="POST" action="<?php echo e(route('login')); ?>">
								<?php echo e(csrf_field()); ?>


								<div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
									
									<input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" placeholder="Email Address" required autofocus>

									<?php if($errors->has('email')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('email')); ?></strong>
										</span>
									<?php endif; ?>
								</div>

								<div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
									

									<input id="password" type="password" class="form-control" name="password"  placeholder="Password" required>

									<?php if($errors->has('password')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('password')); ?></strong>
										</span>
									<?php endif; ?>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											
										</div>							
					      		
									</div>
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-primary">
										Login
									</button>
								</div>

								
				      		
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.themelogin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>