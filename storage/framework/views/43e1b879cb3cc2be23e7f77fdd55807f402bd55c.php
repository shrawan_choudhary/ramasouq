
<?php $__env->startSection('main-content'); ?>
<!-- Page -->
<div class="forum-page-header mb-5" style="background: url('<?php echo e(url('images/favicon/'.$setting->banner_img)); ?>'); background-position: center;background-size: cover; background-repeat: no-repeat;">
	  		<div class="container">
		        <div class="forum-page-heading-block">
		          <h2 class="forum-page-heading text-center"><?php echo e($pages->title); ?></h2>
		        </div>
		    </div>
		</div>
	<section id="about" class="coupon-page-main-block">
		
			<div class="container about-us-page">
				<div class="coupon-dtl-outer">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="about-us-main-block page-block">
								<div class="about-section">
									<?php echo $pages->body; ?>

								</div>
							</div>
						</div>
					<!--	<div class="col-lg-3 col-md-4">
							<div class="coupon-sidebar">
      					<?php echo $__env->make('includes.side-bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- end forum -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>